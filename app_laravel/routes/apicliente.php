<?php

namespace App\Http\Controllers\CLIENTAPI;

use Illuminate\Http\Request;
use PhpParser\Node\Expr\PostDec;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: *');
header('Access-Control-Allow-Headers: *');

// Congresistas
Route::get('congresistas/totalrecords', [CongresistaController::class, 'totalrecords']);
Route::get('congresistas', [CongresistaController::class, 'index']);
Route::get('congresistas/{id}', [CongresistaController::class, 'show']);
Route::get('congresistas/getBasico/{id}', [CongresistaController::class, 'getBasico']);
Route::get('congresistas/getConflictoInteresByIdCongresista/{id}/{idCuatrienio}', [CongresistaController::class, 'getConflictoInteresByIdCongresista']);
Route::get('congresistas/totalrecordsAutoriasByIdCongresista/{id}', [CongresistaController::class, 'totalrecordsAutoriasByIdCongresista']);
Route::get('congresistas/getAutoriasByIdCongresista/{id}', [CongresistaController::class, 'getAutoriasByIdCongresista']);
Route::get('congresistas/totalrecordsPonenciasByIdCongresista/{id}', [CongresistaController::class, 'totalrecordsPonenciasByIdCongresista']);
Route::get('congresistas/getPonenciasByIdCongresista/{id}', [CongresistaController::class, 'getPonenciasByIdCongresista']);
Route::get('congresistas/totalrecordsCitantesByIdCongresista/{id}', [CongresistaController::class, 'totalrecordsCitantesByIdCongresista']);
Route::get('congresistas/getCitantesByIdCongresista/{id}', [CongresistaController::class, 'getCitantesByIdCongresista']);

//CongresoVisible
Route::get('congresoVisible',[CongresoVisibleController::class ,'index']);
Route::get('congresoVisibleEquipo',[CongresoVisibleController::class ,'indexEquipo']);
Route::get('congresoVisibleAliado',[CongresoVisibleController::class ,'indexAliado']);

//Multimedia
Route::get('informesPnud',[MultimediaController::class ,'index']);

//Elecciones
Route::get('elecciones',[EleccionesController::class ,'index']);
Route::get('elecciones/{id}',[EleccionesController::class ,'show']);

//Partido
Route::get('partido',[PartidoController::class ,'index']);
Route::get('partido/{id}',[PartidoController::class ,'show']);

//Comisiones
Route::get('comisions', [ComisionsController::class, 'index']);
Route::get('comisions/totalrecords', [ComisionsController::class, 'totalrecords']);
Route::get('comisions/getControlPoliticoFilter', [ComisionsController::class, 'getControlPoliticoFilter']);
Route::get('comisions/getProyectoLeyFilter', [ComisionsController::class, 'getProyectoLeyFilter']);
Route::get('comisions/getSecretariosFilter', [ComisionsController::class, 'getSecretariosFilter']);
Route::get('comisions/getMiembrosFilter', [ComisionsController::class, 'getMiembrosFilter']);
Route::get('comisions/getAgendaActividad', [ComisionsController::class, 'getAgenda']);
Route::get('comisions/totalrecordsAgendaActividad', [ComisionsController::class, 'totalrecordsAgenda']);
Route::get('comisions/getDataByYearAndMonth', [ComisionsController::class, 'getDataByYearAndMonth']);
Route::get('comisions/{id}', [ComisionsController::class, 'show']);

//Proyectos de ley
Route::get('proyectoley', [ProyectoLeyController::class, 'index']);
Route::get('proyectoley/totalrecords', [ProyectoLeyController::class, 'totalrecords']);
Route::get('proyectoley/getAutoresFilter', [ProyectoLeyController::class, 'getAutoresFilter']);
Route::get('proyectoley/getPonentesFilter', [ProyectoLeyController::class, 'getPonentesFilter']);
Route::get('proyectoley/getCountVotos', [ProyectoLeyController::class, 'getCountVotos']);
Route::get('proyectoley/{id}', [ProyectoLeyController::class, 'show']);

// Partidos
Route::get('partidos/{id}', [PartidosController::class, 'show']);

//Control politico
Route::get('controlpolitico', [ControlPoliticosController::class, 'index']);
Route::get('controlpolitico/getCitantesFilter', [ControlPoliticosController::class, 'getCitantesFilter']);
Route::get('controlpolitico/getCitadosFilter', [ControlPoliticosController::class, 'getCitadosFilter']);
Route::get('controlpolitico/{id}', [ControlPoliticosController::class, 'show']);

//Blog Nuestra Democracia
Route::get('blognd',[BlogNdsController::class ,'index']);
Route::get('blognd/totalrecords', [BlogNdsController::class, 'totalrecords']);
Route::get('blogndultpub',[BlogNdsController::class ,'indexUltimasPublicaciones']);
Route::get('blogndest',[BlogNdsController::class ,'destacadoBlog']);
Route::get('blognd/{id}',[BlogNdsController::class ,'detalleBlog']);
Route::get('blognd/getComboTemaBlog', [BlogNdsController::class, 'getComboTemaBlog']);
Route::get('blognd/getComboTipoPublicacion', [BlogNdsController::class, 'getComboTipoPublicacion']);

// Contenido multimedia
Route::get('contenidomultimedia/totalrecordsInformesPNUD', [ContenidoMultimediaController::class, 'totalrecordsInformesPNUD']);
Route::get('contenidomultimedia/getInformesPNUD', [ContenidoMultimediaController::class, 'getInformesPNUD']);
Route::get('contenidomultimedia/totalrecordsBalanceCuatrienio', [ContenidoMultimediaController::class, 'totalrecordsBalanceCuatrienio']);
Route::get('contenidomultimedia/getBalanceCuatrienio', [ContenidoMultimediaController::class, 'getBalanceCuatrienio']);
Route::get('contenidomultimedia/totalrecordsOpiniones', [ContenidoMultimediaController::class, 'totalrecordsOpiniones']);
Route::get('contenidomultimedia/getOpiniones', [ContenidoMultimediaController::class, 'getOpiniones']);
Route::get('contenidomultimedia/getOpinion/{id}', [ContenidoMultimediaController::class, 'getOpinioneById']);
Route::get('contenidomultimedia/getOpinionCongresista/{id}', [ContenidoMultimediaController::class, 'getOpinioneCongresistaById']);
Route::get('contenidomultimedia/totalrecordsOpinionesCongresistas', [ContenidoMultimediaController::class, 'totalrecordsOpinionesCongresistas']);
Route::get('contenidomultimedia/getOpinionesCongresistas', [ContenidoMultimediaController::class, 'getOpinionesCongresistas']);
Route::get('contenidomultimedia/totalrecordsPodcast', [ContenidoMultimediaController::class, 'totalrecordsPodcast']);
Route::get('contenidomultimedia/getPodcast', [ContenidoMultimediaController::class, 'getPodcast']);
Route::get('contenidomultimedia/totalrecordsMultimedia', [ContenidoMultimediaController::class, 'totalrecordsMultimedia']);
Route::get('contenidomultimedia/getMultimedia', [ContenidoMultimediaController::class, 'getMultimedia']);

// Actividades legislativas
Route::get('actividadeslegislativas/totalrecordsAgenda', [ActividadesLegislativasController::class, 'totalrecordsAgenda']);
Route::get('actividadeslegislativas/getAgenda', [ActividadesLegislativasController::class, 'getAgenda']);
Route::get('actividadeslegislativas/getDetalle/{id}', [ActividadesLegislativasController::class, 'getAgendaDetalle']);
Route::get('actividadeslegislativas/totalrecordsVotaciones', [ActividadesLegislativasController::class, 'totalrecordsVotaciones']);
Route::get('actividadeslegislativas/getVotaciones', [ActividadesLegislativasController::class, 'getVotaciones']);
Route::get('actividadeslegislativas/totalrecordsControlPolitico', [ActividadesLegislativasController::class, 'totalrecordsControlPolitico']);
Route::get('actividadeslegislativas/getControlPolitico', [ActividadesLegislativasController::class, 'getControlPolitico']);
Route::get('actividadeslegislativas/totalrecordsElecciones', [ActividadesLegislativasController::class, 'totalrecordsElecciones']);
Route::get('actividadeslegislativas/getElecciones', [ActividadesLegislativasController::class, 'getElecciones']);
Route::get('actividadeslegislativas/totalrecordsPartidos', [ActividadesLegislativasController::class, 'totalrecordsPartidos']);
Route::get('actividadeslegislativas/getPartidos', [ActividadesLegislativasController::class, 'getPartidos']);
Route::get('actividadeslegislativas/getAgendaActividad', [ActividadesLegislativasController::class, 'getAgendaActividad']);
Route::get('actividadeslegislativas/getDataByYearAndMonth', [ActividadesLegislativasController::class, 'getDataByYearAndMonth']);
Route::get('actividadeslegislativas/totalrecordsAgendaActividad', [ActividadesLegislativasController::class, 'totalrecordsAgendaActividad']);

// Detalle balance cuatrienio
Route::get('balancecuatrienio/{id}',[BalanceCuatrienioController::class ,'detalleBalanceCuatrienio']);
Route::get('balancecuatrienio/getInformeById/{id}',[BalanceCuatrienioController::class ,'getInformeById']);
Route::get('balancecuatrienio/getInformes/{id}',[BalanceCuatrienioController::class ,'getInformes']);
Route::get('balancecuatrienio/totalrecordsInformes/{id}',[BalanceCuatrienioController::class ,'totalrecordsInformes']);

//Informacion del sitio
Route::get('informacionSitio',[InformacionSitioController::class ,'InformacionSitioHome']);
Route::get('slideCongresoVisible',[InformacionSitioController::class ,'slideCongresoVisible']);

// Votaciones
Route::get('votaciones/{id}',[VotacionController::class ,'detalleVotacion']);

// Graficas
Route::get('datos/camara_origen_iniciativas',[DatosController::class ,'camara_origen_iniciativas']);
Route::get('datos/camara_temas_recurrentes_por_partido',[DatosController::class ,'camara_temas_recurrentes_por_partido']);
Route::get('datos/camara_partidos_mayor_numero_autorias_proyecto_ley',[DatosController::class ,'camara_partidos_mayor_numero_autorias_proyecto_ley']);
Route::get('datos/camara_representantes_mayor_numero_autorias_proyecto_ley',[DatosController::class ,'camara_representantes_mayor_numero_autorias_proyecto_ley']);
Route::get('datos/camara_asientos_por_partidos',[DatosController::class ,'camara_asientos_por_partidos']);
Route::get('datos/camara_piramide_poblacional',[DatosController::class ,'camara_piramide_poblacional']);

Route::get('datos/senado_origen_iniciativas',[DatosController::class ,'senado_origen_iniciativas']);
Route::get('datos/senado_temas_recurrentes_por_partido',[DatosController::class ,'senado_temas_recurrentes_por_partido']);
Route::get('datos/senado_partidos_mayor_numero_autorias_proyecto_ley',[DatosController::class ,'senado_partidos_mayor_numero_autorias_proyecto_ley']);
Route::get('datos/senado_representantes_mayor_numero_autorias_proyecto_ley',[DatosController::class ,'senado_representantes_mayor_numero_autorias_proyecto_ley']);
Route::get('datos/senado_asientos_por_partidos',[DatosController::class ,'senado_asientos_por_partidos']);
Route::get('datos/senado_piramide_poblacional',[DatosController::class ,'senado_piramide_poblacional']);

Route::get('datos/edad_mediana_por_partidos',[DatosController::class ,'edad_mediana_por_partidos']);
Route::get('datos/total_proyecto_de_ley_por_genero',[DatosController::class ,'total_proyecto_de_ley_por_genero']);
Route::get('datos/total_proyecto_de_ley_sancionados_por_anio',[DatosController::class ,'total_proyecto_de_ley_sancionados_por_anio']);
Route::get('datos/cantidad_temas_todos_cuatrienios',[DatosController::class ,'cantidad_temas_todos_cuatrienios']);
Route::get('datos/cantidad_temas_un_cuatrienio',[DatosController::class ,'cantidad_temas_un_cuatrienio']);
Route::get('datos/tiempo_sancion_proyectos_de_ley',[DatosController::class ,'tiempo_sancion_proyectos_de_ley']);
Route::get('datos/proporcion_de_mujeres',[DatosController::class ,'proporcion_de_mujeres']);
Route::get('datos/proporcion_de_mujeres_por_cuatrienio',[DatosController::class ,'proporcion_de_mujeres_por_cuatrienio']);
Route::get('datos/minimo_promedio_maximo_cuatrienio',[DatosController::class ,'minimo_promedio_maximo_cuatrienio']);
Route::get('datos/numero_iniciativas_gubernamentales',[DatosController::class ,'numero_iniciativas_gubernamentales']);
Route::get('datos/top_5_partidos_congresistas_mayor_edad',[DatosController::class ,'top_5_partidos_congresistas_mayor_edad']);
Route::get('datos/top_5_partidos_congresistas_mas_jovenes',[DatosController::class ,'top_5_partidos_congresistas_mas_jovenes']);
Route::get('datos/partidos_mayor_representacion_congreso_por_cuatrienio',[DatosController::class ,'partidos_mayor_representacion_congreso_por_cuatrienio']);
Route::get('datos/numero_iniciativas_gubernamentales_sancionadas_como_ley',[DatosController::class ,'numero_iniciativas_gubernamentales_sancionadas_como_ley']);
Route::get('datos/distribucion_edad_congreso_todos_cuatrienios',[DatosController::class ,'distribucion_edad_congreso_todos_cuatrienios']);
Route::get('datos/top_10_temas_proyectos_ley',[DatosController::class ,'top_10_temas_proyectos_ley']);
Route::get('datos/estado_proyectos_ley',[DatosController::class ,'estado_proyectos_ley']);
Route::get('datos/value_box_proyecto_de_ley',[DatosController::class ,'value_box_proyecto_de_ley']);
Route::get('datos/value_box_audiencias_publicas_citadas',[DatosController::class ,'value_box_audiencias_publicas_citadas']);
Route::get('datos/value_box_debates_de_control_politico',[DatosController::class ,'value_box_debates_de_control_politico']);
Route::get('datos/value_box_sentencias_emitidas',[DatosController::class ,'value_box_sentencias_emitidas']);
Route::get('datos/value_box_objeciones_emitidas',[DatosController::class ,'value_box_objeciones_emitidas']);
Route::get('datos/value_box_proyectos_radicados',[DatosController::class ,'value_box_proyectos_radicados']);
Route::get('datos/votaciones_por_tema_camara',[DatosController::class ,'votaciones_por_tema_camara']);
Route::get('datos/votaciones_por_tema_senado',[DatosController::class ,'votaciones_por_tema_senado']);
Route::get('datos/votaciones_por_iniciativa_camara',[DatosController::class ,'votaciones_por_iniciativa_camara']);
Route::get('datos/votaciones_por_iniciativa_senado',[DatosController::class ,'votaciones_por_iniciativa_senado']);
Route::get('datos/tipo_votaciones_camara',[DatosController::class ,'tipo_votaciones_camara']);
Route::get('datos/tipo_votaciones_senado',[DatosController::class ,'tipo_votaciones_senado']);
Route::get('datos/congresistas_mas_citaciones',[DatosController::class ,'congresistas_mas_citaciones']);
Route::get('datos/partidos_mas_citaciones',[DatosController::class ,'partidos_mas_citaciones']);
Route::get('datos/total_citaciones_por_cuatrienio',[DatosController::class ,'total_citaciones_por_cuatrienio']);
Route::get('datos/datos_inicio',[DatosController::class ,'datos_inicio']);

// Gráficas conflicto de interes
Route::get('datosCongresistasConflictoInteres/totalCongresistasConflictoInteresParientes',[DatosCongresistasConflictoInteresController::class ,'totalCongresistasConflictoInteresParientes']);
Route::get('datosCongresistasConflictoInteres/totalProyectoLeyConflictoInteres',[DatosCongresistasConflictoInteresController::class ,'totalProyectoLeyConflictoInteres']);
Route::get('datosCongresistasConflictoInteres/totalCongresistasConflictoInteresCamara',[DatosCongresistasConflictoInteresController::class ,'totalCongresistasConflictoInteresCamara']);
Route::get('datosCongresistasConflictoInteres/totalCongresistasConflictoInteresSenado',[DatosCongresistasConflictoInteresController::class ,'totalCongresistasConflictoInteresSenado']);
Route::get('datosCongresistasConflictoInteres/totalCongresistasReportaronCampaniasCamara',[DatosCongresistasConflictoInteresController::class ,'totalCongresistasReportaronCampaniasCamara']);
Route::get('datosCongresistasConflictoInteres/totalCongresistasReportaronCampaniasSenado',[DatosCongresistasConflictoInteresController::class ,'totalCongresistasReportaronCampaniasSenado']);
Route::get('datosCongresistasConflictoInteres/totalCongresistasParticipanJuntas',[DatosCongresistasConflictoInteresController::class ,'totalCongresistasParticipanJuntas']);
Route::get('datosCongresistasConflictoInteres/porcentajeIngresosPublicosPrivados',[DatosCongresistasConflictoInteresController::class ,'porcentajeIngresosPublicosPrivados']);