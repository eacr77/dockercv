<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\ControlPolitico;
use App\Models\ControlPoliticoTag;
use Validator;
use App\Messages;
use Carbon\Carbon;
use DB;

class ControlPoliticosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input('idFilter');
        $legislatura = $request->input('legislatura');
        $cuatrienio = $request->input('cuatrienio');
        $comision = $request->input('comision');
        $estado = $request->input('estado');
        $tema = $request->input('tema');
        $corporacion = $request->input('corporacion');
        $nombre = $request->input('search');
        $tipoCitacion = $request->input('tipoCitacion');

        $query = ControlPolitico::query();

        if (!is_null($corporacion) && $corporacion != '-1')
        {
            $query->where(
                'corporacion_id',
                $corporacion
            );
        }
        if (!is_null($legislatura) && $legislatura != '-1')
        {
            $query->where(
                'legislatura_id',
                $legislatura
            );
        }
        if (!is_null($cuatrienio) && $cuatrienio != '-1')
        {
            $query->where(
                'cuatrienio_id',
                $cuatrienio
            );
        }
        if (!is_null($estado) && $estado != '-1')
        {
            $query->where(
                'estado_control_politico_id',
                $estado
            );
        }
        if (!is_null($tema) && $tema != '-1')
        {
            $query->where(
                'tema_id_principal',
                $tema
            );
        }
        if (!is_null($comision) && $comision != '-1')
        {
            $query->where(
                'comision_id',
                $comision
            );
        }
        if (!is_null($tipoCitacion) && $tipoCitacion != '-1')
        {
            $query->where( function ($query) use ($tipoCitacion){
                $query->WhereHas('controlPoliticoCitados', function ($query) use ($tipoCitacion){
                    $query->where('tipo_citacion',  $tipoCitacion );
                });
            });
        }

        $controlPolitico = $query->select([
            'id',
            'titulo',
            'fecha',
            'comision_id',
            'legislatura_id',
            'cuatrienio_id',
            'estado_control_politico_id',
            'tema_id_principal',
            'tema_id_secundario',
            'corporacion_id',
            'detalles',
            'numero_proposicion',
            'activo',]
        )->with(
            'legislatura',
            'cuatrienio',
            'estadoControlPolitico',
            'comision',
            'temaPrincipalControlPolitico',
            'temaSecundarioControlPolitico',
            'corporacion',
            'controlPoliticoCitantes',
            'controlPoliticoCitados'
        )->where(
            'activo',
            ($filter != "-1") ? '=' : '!=',
            $filter
        )
        ->where( function ($query) use ($nombre){
            $query->where('titulo', 'LIKE', '%' . $nombre . '%' );
            $query->orWhereHas('controlPoliticoCitantes', function ($query) use ($nombre){
                $query->WhereHas('congresista', function ($query) use ($nombre) {
                    $query->WhereHas('persona', function ($query) use ($nombre) {
                        $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%".$nombre."%")
                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%".$nombre."%");
                    });
                });
            });
            $query->orWhereHas('controlPoliticoCitados', function ($query) use ($nombre){
                $query->WhereHas('persona', function ($query) use ($nombre){
                    $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%".$nombre."%")
                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%".$nombre."%");
                });
            });
        })
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->orderBy('fecha','desc')
        ->groupBy('id')
        ->get()
        ->toJson(JSON_PRETTY_PRINT);

        return response($controlPolitico);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ControlPolitico::$rules, ControlPolitico::$rulesMessages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        // DB::beginTransaction();
        // try {
            $controlPolitico = new ControlPolitico;
            $request->request->add(['usercreated' => $request->user]);
            $result = $controlPolitico->create($request->all());

            $id = $result->id;

            $tags = $request->input('control_politico_tags');
                if($tags != null)
                {
                    foreach($request->input('control_politico_tags') as $key => $value)
                    {
                        $tags[$key]['control_politico_id'] = $id;
                        $requestCPTag = new Request($tags[$key]);

                        if($requestCPTag->activo == 1)
                        {
                            $CPTag = new ControlPoliticoTag;
                            $CPTag->fill($requestCPTag->all());
                            $CPTag->usercreated = $request->user;
                            $CPTag->save();
                        }
                    }
                }

            // DB::commit();
            return response()->json(['message' => 'OK'], 201);

        // } catch (\Exception $e)
        // {
        //     DB::rollback();
        //     return response()->json(['message' => 'Error'], 422);
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $controlPolitico = ControlPolitico::with('comision','corporacion', 'controlPoliticoTags')
        ->where('id',$id)
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($controlPolitico, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), ControlPolitico::$rules, ControlPolitico::$rulesMessages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        DB::beginTransaction();
        try
        {
            $controlPolitico = ControlPolitico::find($id);
            if($controlPolitico != null){
                $request->request->add(['usermodifed' => $request->user]);
                $controlPolitico->fill($request->all());
                $controlPolitico->save();
            }

            $tags = $request->input('control_politico_tags');
            if($tags != null)
            {
                foreach($request->input('control_politico_tags') as $key => $value)
                {
                    $tags[$key]['control_politico_id'] = $id;
                    $requesttags = new Request($tags[$key]);
                    if($requesttags->id > 0)
                    {
                        if($requesttags->activo == 1)
                        {
                            $controlPoliticoTag = ControlPoliticoTag::find($requesttags->id);
                            $controlPoliticoTag->fill($requesttags->all());
                            $controlPoliticoTag->usermodifed = $request->user;
                            $controlPoliticoTag->save();
                        }
                        else
                        {
                            $controlPoliticoTag = ControlPoliticoTag::find($requesttags->id);
                            $controlPoliticoTag->activo = 0;
                            $controlPoliticoTag->usermodifed = $request->user;
                            $controlPoliticoTag->save();
                        }
                    }
                    else
                    {
                        if($requesttags->activo == 1)
                        {
                            $controlPoliticoTag = new ControlPoliticoTag;
                            $controlPoliticoTag->fill($requesttags->all());
                            $controlPoliticoTag->usercreated = $request->user;
                            $controlPoliticoTag->save();
                        }
                    }
                }
            }
            DB::commit();
            return response()->json(['message' => 'OK'], 201);
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['message' => 'Error'], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $controlPolitico = ControlPolitico::find($id);
        $controlPolitico->activo=!$controlPolitico->activo;
        $controlPolitico->save();
        return response($controlPolitico, 200);
    }

    public function totalrecords(Request $request)
    {
        $filter = $request->input('idFilter');
        $legislatura = $request->input('legislatura');
        $cuatrienio = $request->input('cuatrienio');
        $comision = $request->input('comision');
        $estado = $request->input('estado');
        $tema = $request->input('tema');
        $corporacion = $request->input('corporacion');
        $nombre = $request->input('search');
        $tipoCitacion = $request->input('tipoCitacion');

        $query = ControlPolitico::query();

        if (!is_null($corporacion) && $corporacion != '-1')
        {
            $query->where(
                'corporacion_id',
                $corporacion
            );
        }
        if (!is_null($legislatura) && $legislatura != '-1')
        {
            $query->where(
                'legislatura_id',
                $legislatura
            );
        }
        if (!is_null($cuatrienio) && $cuatrienio != '-1')
        {
            $query->where(
                'cuatrienio_id',
                $cuatrienio
            );
        }
        if (!is_null($estado) && $estado != '-1')
        {
            $query->where(
                'estado_control_politico_id',
                $estado
            );
        }
        if (!is_null($tema) && $tema != '-1')
        {
            $query->where(
                'tema_id_principal',
                $tema
            );
        }
        if (!is_null($comision) && $comision != '-1')
        {
            $query->where(
                'comision_id',
                $comision
            );
        }
        if (!is_null($tipoCitacion) && $tipoCitacion != '-1')
        {
            $query->where( function ($query) use ($tipoCitacion){
                $query->WhereHas('controlPoliticoCitados', function ($query) use ($tipoCitacion){
                    $query->where('tipo_citacion',  $tipoCitacion );
                });
            });
        }

        $count = $query->select([
                'id',
                'titulo',
                'fecha',
                'comision_id',
                'legislatura_id',
                'cuatrienio_id',
                'estado_control_politico_id',
                'tema_id_principal',
                'tema_id_secundario',
                'corporacion_id',
                'detalles',
                'numero_proposicion',
                'activo',]
        )->with(
            'legislatura',
            'cuatrienio',
            'estadoControlPolitico',
            'comision',
            'temaPrincipalControlPolitico',
            'temaSecundarioControlPolitico',
            'corporacion',
            'controlPoliticoCitantes',
            'controlPoliticoCitados'
        )->where(
            'activo',
            ($filter != "-1") ? '=' : '!=',
            $filter
        )
        ->where( function ($query) use ($nombre){
            $query->where('titulo', 'LIKE', '%' . $nombre . '%' );
            $query->orWhereHas('controlPoliticoCitantes', function ($query) use ($nombre){
                $query->WhereHas('congresista', function ($query) use ($nombre) {
                    $query->WhereHas('persona', function ($query) use ($nombre) {
                        $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%".$nombre."%")
                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%".$nombre."%");
                    });
                });
            });
            $query->orWhereHas('controlPoliticoCitados', function ($query) use ($nombre){
                $query->WhereHas('persona', function ($query) use ($nombre){
                    $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%".$nombre."%")
                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%".$nombre."%");
                });
            });
        })
            ->count();

        return response($count, 200);
    }
}
