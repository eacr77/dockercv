<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Congresista;
use App\Models\Iniciativa;
use App\Models\ProyectoLey;
use App\Models\ProyectoLeyAutor;
use App\Models\ProyectoLeyAutorLegislativo;
use App\Models\ProyectoLeyComision;
use App\Models\ProyectoLeyEstado;
use App\Models\ProyectoLeyPonente;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProyectoLeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = ProyectoLey::query();

        if ($request->input('idFilter') != "-1")
        {
            $query->where('activo', $request->idFilter);
        }

        if ($request->input('tipo_id') != "-1")
        {
            $query->where('tipo_proyecto_id', $request->tipo_id);
        }

        if ($request->input('iniciativa_id') != "-1")
        {
            $query->where('iniciativa_id', $request->iniciativa_id);
        }

        if ($request->input('tema_id') != "-1")
        {
            $tema_id = $request->input('tema_id');
            $query->where(function ($query) use ($tema_id){
                    $query->where('tema_id_principal', $tema_id)
                    ->orWhere('tema_id_secundario', $tema_id);
                }
            );
        }

        if ($request->input('estado_id') != "-1") {
            $estado_id = $request->input('estado_id');
            $query->Where(
                function ($query) use ($estado_id) {
                    $query->WhereHas('ProyectoLeyEstado', function ($query) use ($estado_id) {
                            $query->where('estado_proyecto_ley_id', $estado_id);
                        }
                    );
                }
            );
        }
        if ($request->input('cuatrienio') != "-1")
        {
            $query->where('cuatrienio_id', $request->cuatrienio);
        }
        if ($request->input('legislatura_id') != "-1")
        {
            $query->where('legislatura_id', $request->legislatura_id);
        }

        if ($request->input('search') != "") {
            $search = $request->input('search');
            $query->Where(function ($query) use ($search) {
                    $query->Where('numero_camara', 'like', '%' . $search . '%')
                        ->orWhere('numero_senado', 'like', '%' . $search . '%')
                        ->orWhere('titulo', 'like', '%' . $search . '%')
                        ->orWhere(function ($query) use ($search) {
                            $query->orWhereHas('ProyectoLeyAutorLegislativos', function ($query) use ($search) {
                                $query->WhereHas('Congresista', function ($query) use ($search) {
                                    $query->WhereHas('persona', function ($query) use ($search) {
                                        $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                            ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                    });
                                });
                            });
                        }
                        )->orWhere(function ($query) use ($search) {
                        $query->orWhereHas('ProyectoLeyAutorPersonas', function ($query) use ($search) {
                            $query->WhereHas(
                                'persona', function ($query) use ($search) {
                                    $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                });
                        });
                    });
                }
            );
        }

        $items = $query->with(['Legislatura', 'Cuatrienio', 'TipoProyectoLey'])
                ->skip(($request->input('page') - 1) * $request->input('rows'))->take($request->input('rows'))
                ->orderBy('id','desc')
                ->get()
                ->toJson(JSON_PRETTY_PRINT);
        return response($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ProyectoLey::rulesPost(), ProyectoLey::$rulesPostMessages);
        if ($validator->fails())
            return response()->json($validator->errors(), 422);

        DB::beginTransaction();
        try
        {
            $item = new ProyectoLey();
            $user = $request->user;
            $request->request->add(['usercreated' => $request->user]);
            $result = $item->create($request->all());

            // Damos de alta los estatus del proyecto de ley por estado
            if(!is_null($request->proyecto_ley_estado))
            {
                $count = count($request->proyecto_ley_estado);
                for ($i=0; $i < $count; $i++) {
                    $item_estado = $request->proyecto_ley_estado[$i];

                    if($item_estado["activo"])
                    {
                        // Damos de alta los estados del proyecto de ley
                        $item_proyecto_ley_estado = new ProyectoLeyEstado();
                        $item_proyecto_ley_estado->fill($item_estado);
                        $item_proyecto_ley_estado->proyecto_ley_id = $result->id;
                        $item_proyecto_ley_estado->usercreated = $user;
                        $item_proyecto_ley_estado->activo = 1;

                        // Damos de alta al archivo en caso que hubiera uno
                        if (is_file($item_estado["archivo"]))
                        {
                            $folder = $item_proyecto_ley_estado->proyecto_ley_id;
                            $file_name = $item_proyecto_ley_estado->gaceta_texto;
                            $file = $item_estado["archivo"];
                            $file_extension = $file->getClientOriginalExtension();
                            $path = $file->storeAs(
                                '/proyecto-ley/' . $folder, // Directorio
                                $file_name .'.'. $file_extension, // Nombre real de la imagen
                                'public' // disco
                            );
                            $item_proyecto_ley_estado->gaceta_url = $path;
                        }
                        $item_proyecto_ley_estado->save();

                        // Verificando comisiones del proyecto de ley estado
                        if(isset($item_estado["comisiones"]))
                        {
                            $count_comision = count($item_estado["comisiones"]);
                            for ($ii=0; $ii < $count_comision; $ii++) {
                                $item_comision = $item_estado["comisiones"][$ii];
                                if($item_comision["activo"])
                                {
                                    // Damos de alta a las comisiones del proyecto de ley estado
                                    $item_proyecto_ley_comision = new ProyectoLeyComision();
                                    $item_proyecto_ley_comision->fill($item_comision);
                                    $item_proyecto_ley_comision->proyecto_ley_estado_id = $item_proyecto_ley_estado->id;
                                    $item_proyecto_ley_comision->usercreated = $user;
                                    $item_proyecto_ley_comision->activo = 1;
                                    $item_proyecto_ley_comision->save();
                                }
                            }
                        }

                        // Verificando ponentes del proyecto de ley estado
                        if(isset($item_estado["ponentes"]))
                        {
                            $count_ponente = count($item_estado["ponentes"]);
                            for ($ii=0; $ii < $count_ponente; $ii++) {
                                $item_ponente = $item_estado["ponentes"][$ii];
                                if($item_ponente["activo"])
                                {
                                    // Damos de alta a los ponentes del proyecto de ley estado
                                    $item_proyecto_ley_ponente = new ProyectoLeyPonente();
                                    $item_proyecto_ley_ponente->fill($item_ponente);
                                    $item_proyecto_ley_ponente->proyecto_ley_estado_id = $item_proyecto_ley_estado->id;
                                    $item_proyecto_ley_ponente->usercreated = $user;
                                    $item_proyecto_ley_ponente->activo = 1;
                                    $item_proyecto_ley_ponente->save();
                                }
                            }
                        }
                    }
                }
            }

            // Ahora checamos si aplica a congresistas o solo personas
            $iniciativa = Iniciativa::find($result->iniciativa_id);

            // Checamos los autores legislativos
            if($request->has('proyecto_ley_autor_legislativos')){
                if($iniciativa->aplica_congresista){
                    // Lo que la vista nos trae son los ids de las personas
                    $personas_legislativos_ids = [];
                    foreach ($request->proyecto_ley_autor_legislativos as $autor_legislativo) {
                        $personas_legislativos_ids[] = $autor_legislativo['persona_id'];
                    }
                    if(count($personas_legislativos_ids) > 0){
                        // Obtenemos a todos los congresistas en relación a personas_legislativos_ids
                        $congresistas_ids = Congresista::whereIn(
                            'persona_id',
                            $personas_legislativos_ids
                        )->distinct()->pluck('id')->all();

                        // Generamos un array de los congresistas  por insertar
                        $congresistas_insert = [];
                        foreach ($congresistas_ids as $congresista_id){
                            $item_insert = [
                                'proyecto_ley_id' =>  $result->id,
                                'congresista_id' => $congresista_id,
                                'activo' => 1,
                                'usercreated' => $request->user,
                                'created_at' => Carbon::now()
                            ];
                            $congresistas_insert[] = $item_insert;
                        }

                        // Insertamos a los congresistas
                        ProyectoLeyAutorLegislativo::insert($congresistas_insert);
                    }
                }
            }

            // Checamos los autores personas
            if($request->has('proyecto_ley_autor_personas')){
                if($iniciativa->aplica_persona){
                    // Lo que la vista nos trae son los ids de las personas
                    $personas_ids = [];
                    foreach ($request->proyecto_ley_autor_personas as $autor_persona) {
                        $personas_ids[] = $autor_persona['persona_id'];
                    }
                    if(count($personas_ids) > 0){
                        // Generamos un array de las personas por insertar
                        $personas_insert = [];
                        foreach ($personas_ids as $persona_id){
                            $item_insert = [
                                'proyecto_ley_id' =>  $result->id,
                                'persona_id' => $persona_id,
                                'activo' => 1,
                                'usercreated' => $request->user,
                                'created_at' => Carbon::now()
                            ];
                            $personas_insert[] = $item_insert;
                        }

                        // Insertamos a los autores personas
                        ProyectoLeyAutor::insert($personas_insert);
                    }
                }
            }

            DB::commit();
            return response()->json(['message' => 'OK'],202);
        }
        catch (QueryException $ex)
        {
            DB::rollback();
            return response()->json(['message' => 'Error'],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = ProyectoLey::select(
            [
                'id',
                'cuatrienio_id',
                'legislatura_id',
                'corporacion_id',
                'titulo',
                'alias',
                'fecha_radicacion',
                'numero_camara',
                'numero_senado',
                'iniciativa_id',
                'tipo_proyecto_id',
                'tema_id_principal',
                'tema_id_secundario',
                'sinopsis',
                'se_acumula_a_id',
                'alcance_id',
                'iniciativa_popular',
                'activo',
            ]
        )->where(
                'id',
                $id
            )->with(
                [
                    'ProyectoLeyEstado',
                    'Acumula',
                    'ProyectoLeyAutorLegislativos',
                    'ProyectoLeyAutorPersonas'
                ]
            )->get()->first();

        return response($item, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), ProyectoLey::rulesPut(), ProyectoLey::$rulesPutMessages);
        if ($validator->fails())
        {
            return response()->json(
                $validator->errors(),
                422
            );
        }

        DB::beginTransaction();
        try
        {
            $proyecto_ley = ProyectoLey::find($id);
            $request->request->add(['usermodifed' => $request->user]);
            $proyecto_ley->fill($request->all());
            $proyecto_ley->save();

            // Checamos los estados
            if ($request->has('proyecto_ley_estado')){
                // Solamente los que se devuelvan activos desde la vista
                // Aunque si no mal recuerdo en la vista se elimina del array directamente, no se cambia el activo
                $estados = array_filter(
                    $request->proyecto_ley_estado,
                    static function ($item)  {
                        if($item["activo"]){
                            return $item;
                        }
                    }
                );

                // Ponemos todos los estados  a activo 0, en relación al proyecto de ley
                ProyectoLeyEstado::where
                (
                    [
                        ['proyecto_ley_id', $proyecto_ley->id]
                    ]
                )->update(['activo'=> 0]);

                /* Recorremos el listado de estados que se trae de la vista */
                foreach ($estados as $estado)
                {
                    // Continuamos con el estado del proyecto de ley
                    $item_estado = ProyectoLeyEstado::where
                    (
                        [
                            ['id', $estado["id"]],
                            ['proyecto_ley_id', $proyecto_ley->id]
                        ]
                    )->first();

                    // Checamos si existe en la bd
                    $borrar_archivo_estado = false;
                    if($item_estado != null){
                        // Si existe actualizamos el activo y su fecha de modificación
                        $item_estado->fill($estado);
                        $item_estado->usermodifed = $request->user;
                        if(isset($item_estado->gaceta_url)){
                            $borrar_archivo_estado = true;
                        }
                    }
                    else{
                        // Como no existe creamos un nuevo item
                        // Y lo agregamos a la bd
                        $item_estado = new ProyectoLeyEstado();
                        $item_estado->fill($estado);
                        $item_estado->usercreated = $request->user;
                    }

                    //Checamos si tiene un archivo el estado
                    if (isset($estado["archivo"]) && is_file($estado["archivo"]))
                    {
                        if($borrar_archivo_estado){
                            $url_archivo_estado = $item_estado['gaceta_url'];
                            if(Storage::disk('public')->exists($url_archivo_estado))
                            {
                                Storage::disk('public')->delete($url_archivo_estado);
                            }
                        }
                        // Subimos el nuevo
                        $folder = $proyecto_ley->id;
                        $file_name = $item_estado->gaceta_texto;
                        $file = $estado["archivo"];
                        $file_extension = $file->getClientOriginalExtension();
                        $path = $file->storeAs(
                            '/proyecto-ley/' . $folder, // Directorio
                            $file_name .'.'. $file_extension, // Nombre real de la imagen
                            'public' // disco
                        );
                        $item_estado->gaceta_url = $path;
                    }

                    $item_estado->proyecto_ley_id = $proyecto_ley->id;
                    $item_estado->activo = 1;
                    $item_estado->save();

                    // Checamos primero las comisiones, ya que están en un nivel más profundo
                    // Checamos si tiene la propiedad: Comisiones
                    if(isset($estado["comisiones"]))
                    {
                        // Ponemos todos las comisiones a activo 0, en relación al proyecto de ley estado
                        ProyectoLeyComision::where
                        (
                            [
                                ['proyecto_ley_estado_id', $item_estado->id]
                            ]
                        )->update(['activo'=> 0]);

                        foreach ($estado["comisiones"] as $comision ){

                            $item_comision = ProyectoLeyComision::where
                            (
                                [
                                    ['comision_id', $comision["comision_id"]],
                                    ['proyecto_ley_estado_id', $item_estado->id]
                                ]
                            )->first();

                            // Checamos si existe en la bd
                            if($item_comision != null){
                                // Si existe actualizamos el activo y su fecha de modificación
                                $item_comision->fill($comision);
                                $item_comision->usermodifed = $request->user;
                            }
                            else{
                                // Como no existe creamos un nuevo item
                                // Y lo agregamos a la bd
                                $item_comision = new ProyectoLeyComision();
                                $item_comision->fill($comision);
                                $item_comision->usercreated = $request->user;
                            }
                            $item_comision->proyecto_ley_estado_id = $item_estado->id;
                            $item_comision->activo = 1;
                            $item_comision->save();
                        }

                        // Por último borramos los registros de la tabla
                        ProyectoLeyComision::where
                        (
                            [
                                ['proyecto_ley_estado_id', $item_estado->id],
                                ['activo', 0]
                            ]
                        )->delete();
                    }

                    // Checamos ahora los ponentes, ya que están en un nivel más profundo
                    // Checamos si tiene la propiedad: Ponentes
                    if(isset($estado["ponentes"]))
                    {
                        // Ponemos todos las comisiones a activo 0, en relación al proyecto de ley estado
                        ProyectoLeyPonente::where([['proyecto_ley_estado_id', $item_estado->id]])->update(['activo'=> 0]);

                        foreach ($estado["ponentes"] as $ponente ){

                            $item_ponente = ProyectoLeyPonente::where(
                                [
                                    ['congresista_id', $ponente["congresista_id"]],
                                    ['proyecto_ley_estado_id', $item_estado->id]
                                ]
                            )->get()->first();

                            // Checamos si existe en la bd
                            if($item_ponente != null){
                                // Si existe actualizamos el activo y su fecha de modificación
                                $item_ponente->fill($ponente);
                                $item_ponente->usermodifed = $request->user;
                            }
                            else{
                                // Como no existe creamos un nuevo item
                                // Y lo agregamos a la bd
                                $item_ponente = new ProyectoLeyPonente();
                                $item_ponente->fill($ponente);
                                $item_ponente->usercreated = $request->user;
                            }
                            $item_ponente->proyecto_ley_estado_id = $item_estado->id;
                            $item_ponente->activo = 1;
                            $item_ponente->save();
                        }

                        // Por último borramos los registros de la tabla
                        ProyectoLeyPonente::where(
                            [
                                ['proyecto_ley_estado_id', $item_estado->id],
                                ['activo', 0]
                            ]
                        )->delete();
                    }
                }
                //Borramos los que esten activo 0
                // Primero obtemos aquellos que esten en activo 0
                $estados_desactivados = ProyectoLeyEstado::select(
                    [
                        'id',
                        'gaceta_url',
                    ]
                )->where(
                        'activo',
                        '=',
                        0
                    )->where(
                        'proyecto_ley_id',
                        '=',
                        $proyecto_ley->id
                    )->get();

                // Ahora borramos los archivos
                foreach($estados_desactivados as $estado_desactivados)
                {
                    $url_archivo_estado = $estado_desactivados['gaceta_url'];
                    if(Storage::disk('public')->exists($url_archivo_estado))
                    {
                        Storage::disk('public')->delete($url_archivo_estado);
                    }
                }

                // Por último borramos los registros de la tabla
                ProyectoLeyEstado::where(
                    [
                        ['proyecto_ley_id', $proyecto_ley->id],
                        ['activo', 0]
                    ]
                )->delete();
            }

            // Ahora checamos si aplica a congresistas o solo personas
            $iniciativa = Iniciativa::find($proyecto_ley->iniciativa_id);

            // Checamos los autores legislativos
            if($request->has('proyecto_ley_autor_legislativos')){
                if($iniciativa->aplica_congresista){
                    // Lo que la vista nos trae son los ids de las personas
                    $personas_legislativos_ids = [];
                    foreach ($request->proyecto_ley_autor_legislativos as $autor_legislativo) {
                        $personas_legislativos_ids[] = $autor_legislativo['persona_id'];
                    }
                    if(count($personas_legislativos_ids) > 0){
                        ProyectoLeyAutorLegislativo::where
                        (
                            [
                                ['proyecto_ley_id', $proyecto_ley->id]
                            ]
                        )->update(['activo'=> 0]);
                        // Obtenemos a todos los congresistas en relación a personas_legislativos_ids
                        $congresistas_ids = Congresista::whereIn(
                            'persona_id',
                            $personas_legislativos_ids
                        )->distinct()->pluck('id')->all();

                        // Actualizamos los registros que esten en la tabla autores legislativos
                        ProyectoLeyAutorLegislativo::where(
                            'proyecto_ley_id',
                            $proyecto_ley->id
                        )->whereIn(
                            'congresista_id',
                            $congresistas_ids
                        )->update(
                            [
                                'activo' => 1,
                                'usermodifed' => $request->user
                            ]
                        );

                        // Obtenemos a los congresistas que se actualizaron
                        $congresistas_ids_actualizados = ProyectoLeyAutorLegislativo::where(
                            'proyecto_ley_id',
                            $proyecto_ley->id
                        )->where(
                            'activo',
                            1
                        )->pluck('congresista_id')->all();

                        // Obtenemos a los congresistas que faltan por insertar
                        $congresistas_ids_por_insertar = array_diff(
                            $congresistas_ids,
                            $congresistas_ids_actualizados
                        );

                        // Generamos un array de los congresistas que faltan por insertar
                        $congresistas_insert = [];
                        foreach ($congresistas_ids_por_insertar as $congresista_id_por_insertar){
                            $item_insert = [
                                'proyecto_ley_id' =>  $proyecto_ley->id,
                                'congresista_id' => $congresista_id_por_insertar,
                                'activo' => 1,
                                'usercreated' => $request->user,
                                'created_at' => Carbon::now()
                            ];
                            $congresistas_insert[] = $item_insert;
                        }

                        // Insertamos el resto de los congresistas
                        ProyectoLeyAutorLegislativo::insert($congresistas_insert);
                    }
                }
            }
            else{
                ProyectoLeyAutorLegislativo::where
                (
                    [
                        ['proyecto_ley_id', $proyecto_ley->id]
                    ]
                )->update(['activo'=> 0]);
            }
            // Checamos los autores personas
            if($request->has('proyecto_ley_autor_personas')){
                if($iniciativa->aplica_persona){
                    // Lo que la vista nos trae son los ids de las personas
                    $personas_ids = [];
                    foreach ($request->proyecto_ley_autor_personas as $autor_persona) {
                        $personas_ids[] = $autor_persona['persona_id'];
                    }
                    if(count($personas_ids) > 0){
                        ProyectoLeyAutor::where
                        (
                            [
                                ['proyecto_ley_id', $proyecto_ley->id]
                            ]
                        )->update(['activo'=> 0]);

                        // Actualizamos los registros que esten en la tabla autores legislativos
                        ProyectoLeyAutor::where(
                            'proyecto_ley_id',
                            $proyecto_ley->id
                        )->whereIn(
                            'persona_id',
                            $personas_ids
                        )->update(
                            [
                                'activo' => 1,
                                'usermodifed' => $request->user
                            ]
                        );

                        // Obtenemos a las personas  que se actualizaron
                        $personas_ids_actualizados = ProyectoLeyAutor::where(
                            'proyecto_ley_id',
                            $proyecto_ley->id
                        )->where(
                            'activo',
                            1
                        )->pluck('persona_id')->all();


                        // Obtenemos a las personas  que faltan por insertar
                        $personas_ids_por_insertar = array_diff(
                            $personas_ids,
                            $personas_ids_actualizados
                        );

                        // Generamos un array de las personas que faltan por insertar
                        $personas_insert = [];
                        foreach ($personas_ids_por_insertar as $personas_id_por_insertar){
                            $item_insert = [
                                'proyecto_ley_id' =>  $proyecto_ley->id,
                                'persona_id' => $personas_id_por_insertar,
                                'activo' => 1,
                                'usercreated' => $request->user,
                                'created_at' => Carbon::now()
                            ];
                            $personas_insert[] = $item_insert;
                        }

                        // Insertamos el resto de los autores personas
                        ProyectoLeyAutor::insert($personas_insert);
                    }
                }
            }
            else{
                ProyectoLeyAutor::where
                (
                    [
                        ['proyecto_ley_id', $proyecto_ley->id]
                    ]
                )->update(['activo'=> 0]);
            }
            // Al final los autores tanto de legislativos como personas que esten en activo 0 y que sean de ese
            // proyecto de ley, se eliminan
            ProyectoLeyAutorLegislativo::where
            (
                [
                    ['proyecto_ley_id', $proyecto_ley->id],
                    ['activo', 0]
                ]
            )->delete();

            ProyectoLeyAutor::where
            (
                [
                    ['proyecto_ley_id', $proyecto_ley->id],
                    ['activo', 0]
                ]
            )->delete();

            DB::commit();

            return response()->json(
                ['message' => 'OK'],
                202
            );
        } catch (QueryException $ex)
        {
            DB::rollback();

            return response()->json(
                ['message' => 'Error'],
                422
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = ProyectoLey::find($id);
        $item->activo = !$item->activo;
        $item->save();
        return response($item,200);
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function totalrecords(Request $request)
    {
        $query = ProyectoLey::query();

        if ($request->input('idFilter') != "-1")
        {
            $query->where('activo', $request->idFilter);
        }

        if ($request->input('tipo_id') != "-1")
        {
            $query->where('tipo_proyecto_id', $request->tipo_id);
        }

        if ($request->input('iniciativa_id') != "-1")
        {
            $query->where('iniciativa_id', $request->iniciativa_id);
        }

        if ($request->input('tema_id') != "-1")
        {
            $tema_id = $request->input('tema_id');
            $query->where(function ($query) use ($tema_id){
                    $query->where('tema_id_principal', $tema_id)
                    ->orWhere('tema_id_secundario', $tema_id);
                }
            );
        }

        if ($request->input('estado_id') != "-1") {
            $estado_id = $request->input('estado_id');
            $query->Where(
                function ($query) use ($estado_id) {
                    $query->WhereHas('ProyectoLeyEstado', function ($query) use ($estado_id) {
                            $query->where('estado_proyecto_ley_id', $estado_id);
                        }
                    );
                }
            );
        }
        if ($request->input('cuatrienio') != "-1")
        {
            $query->where('cuatrienio_id', $request->cuatrienio);
        }
        if ($request->input('legislatura_id') != "-1")
        {
            $query->where('legislatura_id', $request->legislatura_id);
        }

        if ($request->input('search') != "") {
            $search = $request->input('search');
            $search = str_replace(' ', '%', $search);
            $query->Where(function ($query) use ($search) {
                    $query->Where('numero_camara', 'like', '%' . $search . '%')
                        ->orWhere('numero_senado', 'like', '%' . $search . '%')
                        ->orWhere('titulo', 'like', '%' . $search . '%')
                        ->orWhere(function ($query) use ($search) {
                            $query->orWhereHas('ProyectoLeyAutorLegislativos', function ($query) use ($search) {
                                $query->WhereHas('Congresista', function ($query) use ($search) {
                                    $query->WhereHas('persona', function ($query) use ($search) {
                                        $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                            ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                    });
                                });
                            });
                        }
                        )->orWhere(function ($query) use ($search) {
                        $query->orWhereHas('ProyectoLeyAutorPersonas', function ($query) use ($search) {
                            $query->WhereHas(
                                'persona', function ($query) use ($search) {
                                    $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                });
                        });
                    });
                }
            );
        }

        $items = $query->count();
        return response($items);
    }
}
