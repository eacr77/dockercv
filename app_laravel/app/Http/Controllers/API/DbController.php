<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ComisionMiembro;
use App\Models\Congresista;
use App\Models\ControlPoliticoCitado;
use App\Models\ControlPoliticoCitante;
use App\Models\Investigacion;
use App\Models\Persona;
use App\Models\PersonaImagen;
use App\Models\PersonaTrayectoriaPublica;
use App\Models\ProyectoLeyAutor;
use App\Models\Votacion;
use App\Models\VotacionCamara;
use App\Models\VotacionComision;
use App\Models\VotacionEstado;
use DateTime;
use Illuminate\Http\Request;

class DbController extends Controller
{
    public function control_politico_citantes()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_control_politico_citantes.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map(
                "utf8_encode",
                $data
            ); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $created_at = $import_data[6] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[6]
                );

            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[7] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[7]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "control_politico_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "congresista_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];
            ControlPoliticoCitante::insert($insertData);
        }
    }
    public function control_politico_citados()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_control_politico_citados.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map(
                "utf8_encode",
                $data
            ); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $created_at = $import_data[8] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[8]
                );

            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[9] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[9]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "control_politico_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "persona_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "asistencia" => $import_data[3] === 'NA' ? null : $import_data[3],
                "tipo_citacion" => $import_data[4] === 'NA' ? null : $import_data[4],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];
            ControlPoliticoCitado::insert($insertData);
        }
    }
    public function personas()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_personas.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $fechaNacimiento = $import_data[3] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[3]
                );

            $fechaNacimiento = $fechaNacimiento
                ? $fechaNacimiento->format('Y-m-d')
                : null;

            $created_at = $import_data[13] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[13]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[14] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[14]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "nombres"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "apellidos"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "fechaNacimiento"=>$fechaNacimiento,
                "municipio_id_nacimiento"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "profesion_id"=>$import_data[5] === 'NA' ? null : $import_data[5],
                "genero_id"=>$import_data[6] === 'NA' ? null : $import_data[6],
                "fecha_fallecimiento"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "perfil_educativo"=>$import_data[8] === 'NA' ? null : $import_data[8],
                "grado_estudio_id"=>$import_data[9] === 'NA' ? null : $import_data[9],
                "activo"=>$import_data[10] === 'NA' ? null : $import_data[10],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            Persona::insert($insertData);
        }
    }
    public function congresistas()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_congresistas.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $created_at = $import_data[11] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[11]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[12] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[12]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "persona_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "corporacion_id"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "cuatrienio_id"=>$import_data[3] === 'NA' ? null : $import_data[3],
                "partido_id"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "curul_id"=>$import_data[5] === 'NA' ? null : $import_data[5],
                "circunscripcion_id"=>$import_data[6] === 'NA' ? null : $import_data[6],
                "departamento_id_mayor_votacion"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "activo"=>$import_data[8] === 'NA' ? null : $import_data[8],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            Congresista::insert($insertData);
        }
    }
    public function comision_miembro()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_comision_miembros.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $fecha_inicio = $import_data[5] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[5]
                );

            $fecha_inicio = $fecha_inicio
                ? $fecha_inicio->format('Y-m-d')
                : null;

            $fecha_fin = $import_data[6] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[6]
                );

            $fecha_fin = $fecha_fin
                ? $fecha_fin->format('Y-m-d')
                : null;

            $created_at = $import_data[10] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[10]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[11] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[11]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "cuatrienio_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "comision_id"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "congresista_id"=>$import_data[3] === 'NA' ? null : $import_data[3],
                "cargo_legislativo_id_comision"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "fecha_inicio"=>$fecha_inicio,
                "fecha_fin"=>$fecha_fin,
                "activo"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            ComisionMiembro::insert($insertData);
        }
    }
    public function investigacion()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_investigacions.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map(
                "utf8_encode",
                $data
            ); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $created_at = $import_data[7] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $import_data[7]
                );

            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[8] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'Y-m-d H:i:s',
                    $import_data[8]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "congresista_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "tipo_investigacion_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "descripcion" => $import_data[3] === 'NA' ? null : $import_data[3],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];
            Investigacion::insert($insertData);
        }
    }
    public function persona_trayectoria_publica()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_persona_trayectoria_publicas.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $fecha = $import_data[4] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[4]
                );

            $fecha = $fecha
                ? $fecha->format('Y-m-d G:i')
                : null;

            $fecha_final = $import_data[5] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[5]
                );

            $fecha_final = $fecha_final
                ? $fecha_final->format('Y-m-d G:i')
                : null;

            $created_at = $import_data[9] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[9]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[10] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[10]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "persona_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "partido_id"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "cargo"=>$import_data[3] === 'NA' ? null : $import_data[3],
                "fecha"=>$fecha,
                "fecha_final"=>$fecha_final,
                "activo"=>$import_data[6] === 'NA' ? null : $import_data[6],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];
            PersonaTrayectoriaPublica::insert($insertData);
        }
    }
    public function proyecto_ley_autor()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_proyecto_ley_autors.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $created_at = $import_data[4] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[4]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[5] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[5]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "persona_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "proyecto_ley_id"=>$import_data[3] === 'NA' ? null : $import_data[3],
                "activo"=>$import_data[6] === 'NA' ? null : $import_data[6],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            ProyectoLeyAutor::insert($insertData);
        }
    }
    public function votacion()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_votacions.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $fecha = $import_data[1] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[1]
                );

            $fecha = $fecha
                ? $fecha->format('Y-m-d')
                : null;

            $created_at = $import_data[22] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[22]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[23] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[23]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "fecha" => $fecha,
                "legislatura_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "cuatrienio_id" => $import_data[3] === 'NA' ? null : $import_data[3],
                "proyecto_de_ley_id" => $import_data[4] === 'NA' ? null : $import_data[4],
                "urlGaceta" => $import_data[5] === 'NA' ? null : $import_data[5],
                "esPlenaria" => $import_data[6] === 'NA' ? null : $import_data[6],
                "esComision" => $import_data[7] === 'NA' ? null : $import_data[7],
                "motivo" => $import_data[8] === 'NA' ? null : $import_data[8],
                "tipo_votacion_id" => $import_data[9] === 'NA' ? null : $import_data[9],
                "votosFavor" => $import_data[10] === 'NA' ? null : $import_data[10],
                "votosContra" => $import_data[11] === 'NA' ? null : $import_data[11],
                "acta" => $import_data[12] === 'NA' ? null : $import_data[12],
                "observaciones" => $import_data[13] === 'NA' ? null : $import_data[13],
                "aprobada" => $import_data[14] === 'NA' ? null : $import_data[14],
                "votosAbstencion" => $import_data[15] === 'NA' ? null : $import_data[15],
                "numero_no_asistencias" => $import_data[16] === 'NA' ? null : $import_data[16],
                "clase_votacion_id" => $import_data[17] === 'NA' ? null : $import_data[17],
                "numero_asistencias" => $import_data[18] === 'NA' ? null : $import_data[18],
                "voto_general" => $import_data[24] === 'NA' ? null : $import_data[24],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];

            Votacion::insert($insertData);
        }
    }
    public function votacion_camaras()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_votacion_camaras.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "votacion_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "corporacion_id" => $import_data[2] === 'NA' ? null : $import_data[2],
            ];

            VotacionCamara::insert($insertData);
        }
    }
    public function votacion_comision()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_votacion_comisions.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $created_at = $import_data[8] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[8]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[9] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[9]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "votacion_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "corporacion_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "tipo_comision_id" => $import_data[3] === 'NA' ? null : $import_data[3],
                "comision_id" => $import_data[4] === 'NA' ? null : $import_data[4],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];

            VotacionComision::insert($insertData);
        }
    }
    public function votacion_estado()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_votacion_estados.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $created_at = $import_data[6] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[6]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[7] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[7]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "votacion_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "proyecto_ley_estado_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];

            VotacionEstado::insert($insertData);
        }
    }
    public function persona_imagens()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_persona_imagens.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {

            if($import_data['3'] === 'NA'){
                $prefix_url_imagen = null;
            }
            else{
                $id = $import_data['0'];
                $arr =  explode('/', $import_data['3']);
                $name_imagen = $arr[count($arr) - 1];
                $prefix_url_imagen = 'persona/'.$id.'/'.$name_imagen;
            }

            for($i = 0; $i < 3; $i++){
                $insertData = [
                    "persona_id" => $import_data[0] === 'NA' ? null : $import_data[0],
                    "imagen" => $prefix_url_imagen,
                    "activo" => 1,
                ];

                PersonaImagen::insert($insertData);
            }
        }
    }
}
