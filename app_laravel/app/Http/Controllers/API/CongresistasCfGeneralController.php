<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Congresistas_cf_general;
use App\Models\Congresistas_cf_juntas_asociaciones;
use App\Models\Persona;
use App\Models\CongresistasCfAcreencias;
use App\Models\CongresistasCfBienes;
use App\Models\CongresistasCfDocumentos;
use App\Models\CongresistasCfInfoCercanias;
use App\Models\CongresistasCfIngresos;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Facades\DB;

class CongresistasCfGeneralController extends Controller
{
    public function save(Request $request){
        $validator_generals = Validator::make($request['congresistas_cf_generals'], Congresistas_cf_general::$rulesPost,  Congresistas_cf_general::$messagesPost);

        if ($validator_generals->fails()) {
            $messages = array_merge_recursive($validator_generals->errors()->toArray());
            return response()->json($messages, 422);
        }

        DB::beginTransaction();
        try{
            $user = $request->user;
            // Checamos si es igual a 0, si es así se crea un nuevo registro

            $congresista_cv_general = Congresistas_cf_general::firstOrNew(array('id' => $request['congresistas_cf_generals']["id"]));
            if($congresista_cv_general->id == 0){
                $congresista_cv_general->usercreated = $request->user;
            }
            else{
                // Como tiene un id en la tabla: Congresistas_cf_general
                // Da a entender que tiene data en las demás tablas
                $congresista_cv_general->usermodifed = $request->user;

                // Por lo que ponemos activo 0, para luego eliminar los registros que esten "desactivados"
                Congresistas_cf_juntas_asociaciones::where
                (
                    [
                        ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id]
                    ]
                )->update(['activo'=> 0]);

                CongresistasCfInfoCercanias::where
                (
                    [
                        ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id]
                    ]
                )->update(['activo'=> 0]);

                CongresistasCfIngresos::where
                (
                    [
                        ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id]
                    ]
                )->update(['activo'=> 0]);

                CongresistasCfAcreencias::where
                (
                    [
                        ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id]
                    ]
                )->update(['activo'=> 0]);

                CongresistasCfBienes::where
                (
                    [
                        ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id]
                    ]
                )->update(['activo'=> 0]);

                CongresistasCfDocumentos::where
                (
                    [
                        ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id]
                    ]
                )->update(['activo'=> 0]);
            }

            $congresista_cv_general->congresista_detalle_id = $request['congresistas_cf_generals']["congresista_detalle_id"];
            $congresista_cv_general->conflictos_declarados = $request['congresistas_cf_generals']["conflictos_declarados"];
            $congresista_cv_general->actividad_economica = $request['congresistas_cf_generals']["actividad_economica"];

            $congresista_cv_general->save();

            // Damos de alta a las juntas asociadas
            $count_juntas_asociadas = count($request['congresistas_cf_juntas_asociaciones']);
            for ($i = 0; $i < $count_juntas_asociadas; $i++) {
                $current_item_junta_asociada = $request['congresistas_cf_juntas_asociaciones'][$i];
                if
                (
                    $current_item_junta_asociada["activo"]
                    && $current_item_junta_asociada["cf_organo_id"] > 0
                    && $current_item_junta_asociada["cf_calidad_miembro_id"] > 0
                    && $current_item_junta_asociada["pais_id"] > 0
                )
                {
                    $item_congresistas_cf_juntas_asociaciones = Congresistas_cf_juntas_asociaciones::firstOrNew(array('id' => $current_item_junta_asociada["id"]));

                    if($item_congresistas_cf_juntas_asociaciones->id == 0){
                        $item_congresistas_cf_juntas_asociaciones->fill($current_item_junta_asociada);
                        $item_congresistas_cf_juntas_asociaciones->congresista_detalle_id = $congresista_cv_general->congresista_detalle_id;
                        $item_congresistas_cf_juntas_asociaciones->usercreated = $user;
                    }
                    else{
                        $item_congresistas_cf_juntas_asociaciones->nombre_entidad = $current_item_junta_asociada['nombre_entidad'];
                        $item_congresistas_cf_juntas_asociaciones->cf_organo_id = $current_item_junta_asociada['cf_organo_id'];
                        $item_congresistas_cf_juntas_asociaciones->cf_calidad_miembro_id = $current_item_junta_asociada['cf_calidad_miembro_id'];
                        $item_congresistas_cf_juntas_asociaciones->pais_id = $current_item_junta_asociada['pais_id'];
                        $item_congresistas_cf_juntas_asociaciones->activo = 1;
                        $item_congresistas_cf_juntas_asociaciones->usermodifed = $request->user;
                    }
                    $item_congresistas_cf_juntas_asociaciones->save();
                }
            }

            // Damos de alta a las info cercanias
            $count_info_cercanias = count($request['congresistas_cf_info_cercanias']);
            for ($i = 0; $i < $count_info_cercanias; $i++) {
                $current_item_info_cercanias = $request['congresistas_cf_info_cercanias'][$i];
                if
                (
                    $current_item_info_cercanias["activo"]
                    && $current_item_info_cercanias["cf_parentesco_id"] > 0
                )
                {
                    $item_congresistas_cf_info_cercanias = CongresistasCfInfoCercanias::firstOrNew(array('id' => $current_item_info_cercanias["id"]));

                    if($item_congresistas_cf_info_cercanias->id == 0){
                        $item_congresistas_cf_info_cercanias->fill($current_item_info_cercanias);
                        $item_congresistas_cf_info_cercanias->congresista_detalle_id = $congresista_cv_general->congresista_detalle_id;
                        $item_congresistas_cf_info_cercanias->usercreated = $user;
                    }
                    else{
                        $item_congresistas_cf_info_cercanias->nombre = $current_item_info_cercanias['nombre'];
                        $item_congresistas_cf_info_cercanias->cf_parentesco_id = $current_item_info_cercanias['cf_parentesco_id'];
                        $item_congresistas_cf_info_cercanias->conflicto_interes = $current_item_info_cercanias['conflicto_interes'];
                        $item_congresistas_cf_info_cercanias->activo = 1;
                        $item_congresistas_cf_info_cercanias->usermodifed = $request->user;
                    }
                    $item_congresistas_cf_info_cercanias->save();
                }
            }

            // Damos de alta a los ingresos
            $current_item_congresistas_cf_ingresos = $request['congresistas_cf_ingresos'];
            $item_congresistas_cf_ingresos = CongresistasCfIngresos::firstOrNew(array('id' => $current_item_congresistas_cf_ingresos["id"]));
            if($item_congresistas_cf_ingresos->id == 0){
                $item_congresistas_cf_ingresos->fill($current_item_congresistas_cf_ingresos);
                $item_congresistas_cf_ingresos->congresista_detalle_id = $congresista_cv_general->congresista_detalle_id;
                $item_congresistas_cf_ingresos->usercreated = $user;
                $item_congresistas_cf_ingresos->activo = 1;
            }
            else{
                $item_congresistas_cf_ingresos->salario_anual = $current_item_congresistas_cf_ingresos['salario_anual'];
                $item_congresistas_cf_ingresos->interes_cesantias_anual = $current_item_congresistas_cf_ingresos['interes_cesantias_anual'];
                $item_congresistas_cf_ingresos->gasto_representacion_anual = $current_item_congresistas_cf_ingresos['gasto_representacion_anual'];
                $item_congresistas_cf_ingresos->arriendo_anual = $current_item_congresistas_cf_ingresos['arriendo_anual'];
                $item_congresistas_cf_ingresos->honorario_anual = $current_item_congresistas_cf_ingresos['honorario_anual'];
                $item_congresistas_cf_ingresos->otros_anual = $current_item_congresistas_cf_ingresos['otros_anual'];
                $item_congresistas_cf_ingresos->total_anual = $current_item_congresistas_cf_ingresos['total_anual'];
                $item_congresistas_cf_ingresos->activo = 1;
                $item_congresistas_cf_ingresos->usermodifed = $request->user;
            }

            $item_congresistas_cf_ingresos->save();

            // Damos de alta las acreencias
            $count_acreencias = count($request['congresistas_cf_acreencias']);
            for ($i = 0; $i < $count_acreencias; $i++) {
                $current_item_acreencias = $request['congresistas_cf_acreencias'][$i];
                if
                (
                    $current_item_acreencias["activo"]
                    && $current_item_acreencias["cf_acreencia_cpt"] > 0
                )
                {
                    $item_congresistas_cf_acreencias = CongresistasCfAcreencias::firstOrNew(array('id' => $current_item_acreencias["id"]));

                    if($item_congresistas_cf_acreencias->id == 0){
                        $item_congresistas_cf_acreencias->fill($current_item_acreencias);
                        $item_congresistas_cf_acreencias->congresista_detalle_id = $congresista_cv_general->congresista_detalle_id;
                        $item_congresistas_cf_acreencias->usercreated = $user;
                    }
                    else{
                        $item_congresistas_cf_acreencias->cf_acreencia_cpt = $current_item_acreencias['cf_acreencia_cpt'];
                        $item_congresistas_cf_acreencias->saldo = $current_item_acreencias['saldo'];
                        $item_congresistas_cf_acreencias->activo = 1;
                        $item_congresistas_cf_acreencias->usermodifed = $request->user;
                    }
                    $item_congresistas_cf_acreencias->save();
                }
            }

            // Damos de alta los bienes
            $count_bienes = count($request['congresistas_cf_bienes']);
            for ($i = 0; $i < $count_bienes; $i++) {
                $current_item_bienes = $request['congresistas_cf_bienes'][$i];
                if
                (
                    $current_item_bienes["activo"]
                    && $current_item_bienes["pais_id"] > 0
                    && $current_item_bienes["cf_tipo_bien"] > 0
                    && $current_item_bienes["cf_departamento_id"] > 0
                )
                {
                    $item_congresistas_cf_bienes = CongresistasCfBienes::firstOrNew(array('id' => $current_item_bienes["id"]));

                    if($item_congresistas_cf_bienes->id == 0){
                        $item_congresistas_cf_bienes->fill($current_item_bienes);
                        $item_congresistas_cf_bienes->congresista_detalle_id = $congresista_cv_general->congresista_detalle_id;
                        $item_congresistas_cf_bienes->usercreated = $user;
                    }
                    else{
                        $item_congresistas_cf_bienes->cf_tipo_bien = $current_item_bienes['cf_tipo_bien'];
                        $item_congresistas_cf_bienes->pais_id = $current_item_bienes['pais_id'];
                        $item_congresistas_cf_bienes->cf_departamento_id = $current_item_bienes['cf_departamento_id'];
                        $item_congresistas_cf_bienes->valor = $current_item_bienes['valor'];
                        $item_congresistas_cf_bienes->activo = 1;
                        $item_congresistas_cf_bienes->usermodifed = $request->user;
                    }
                    $item_congresistas_cf_bienes->save();
                }
            }

            // Damos de alta los documentos
            $count_documentos = count($request['congresistas_cf_documentos']);
            for ($i = 0; $i < $count_documentos; $i++) {
                $current_item_documentos = $request['congresistas_cf_documentos'][$i];
                if
                (
                    $current_item_documentos["activo"]
                    && $current_item_documentos["cf_tipo_documento_id"] > 0
                )
                {
                    $item_congresistas_cf_documentos = CongresistasCfDocumentos::firstOrNew(array('id' => $current_item_documentos["id"]));

                    if($item_congresistas_cf_documentos->id == 0){
                        $item_congresistas_cf_documentos->fill($current_item_documentos);
                        $item_congresistas_cf_documentos->congresista_detalle_id = $congresista_cv_general->congresista_detalle_id;
                        $item_congresistas_cf_documentos->usercreated = $user;
                    }
                    else{
                        $item_congresistas_cf_documentos->cf_tipo_documento_id = $current_item_documentos['cf_tipo_documento_id'];
                        $item_congresistas_cf_documentos->year = $current_item_documentos['year'];
                        $item_congresistas_cf_documentos->url_documento = $current_item_documentos['url_documento'];
                        $item_congresistas_cf_documentos->nombre = $current_item_documentos['nombre'];
                        $item_congresistas_cf_documentos->activo = 1;
                        $item_congresistas_cf_documentos->usermodifed = $request->user;
                    }

                    if (is_file($current_item_documentos["documento"]))
                    {
                        // Como tiene un nuevo documento, verificamos si tiene un archivo en la bd, si es así
                        // eliminamos el archivo anterior
                        if($item_congresistas_cf_documentos["url_documento"]){
                            $url_archivo_estado = $item_congresistas_cf_documentos['url_documento'];
                            if(Storage::disk('public')->exists($url_archivo_estado))
                            {
                                Storage::disk('public')->delete($url_archivo_estado);
                            }
                        }

                        $folder = $congresista_cv_general->congresista_detalle_id;
                        $file_name =  $current_item_documentos["year"] . "_" . $current_item_documentos["nombre"];
                        $file = $current_item_documentos["documento"];
                        $path = $file->storeAs(
                            '/cf-congresistas/' . $folder, // Directorio
                            $file_name, // Nombre real de la imagen
                            'public' // disco
                        );
                        $item_congresistas_cf_documentos->url_documento = $path;
                    }

                    $item_congresistas_cf_documentos->save();
                }
            }

            // Ahora borramos los activos 0 que esten en las tablas, para no acumular basura
            // Por último borramos los registros de la tabla
            Congresistas_cf_juntas_asociaciones::where
            (
                [
                    ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id],
                    ['activo', 0]
                ]
            )->delete();

            CongresistasCfInfoCercanias::where
            (
                [
                    ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id],
                    ['activo', 0]
                ]
            )->delete();

            CongresistasCfIngresos::where
            (
                [
                    ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id],
                    ['activo', 0]
                ]
            )->delete();

            CongresistasCfAcreencias::where
            (
                [
                    ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id],
                    ['activo', 0]
                ]
            )->delete();

            CongresistasCfBienes::where
            (
                [
                    ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id],
                    ['activo', 0]
                ]
            )->delete();

            // Bueno en documentos es necesario obtener los "desactivados", para así poder eliminar los archivos
            $documentos_desactivados = CongresistasCfDocumentos::select(
                [
                    'id',
                    'congresista_detalle_id',
                    'url_documento',
                ]
            )->where(
                'activo',
                '=',
                0
            )->where(
                'congresista_detalle_id',
                '=',
                $congresista_cv_general->congresista_detalle_id
            )->get();

            // Ahora iteramos los documentos desactivados del congresista para luego borrar el archivo
            foreach($documentos_desactivados as $documento_desactivado)
            {
                $url_archivo_estado = $documento_desactivado['url_documento'];
                if(Storage::disk('public')->exists($url_archivo_estado))
                {
                    Storage::disk('public')->delete($url_archivo_estado);
                }
            }

            // Ahora borramos los registros desactivados del congresista documento.
            CongresistasCfDocumentos::where
            (
                [
                    ['congresista_detalle_id', $congresista_cv_general->congresista_detalle_id],
                    ['activo', 0]
                ]
            )->delete();

            DB::commit();
            return response()->json(['message' => 'OK'], 201);
        }
        catch (QueryException $ex)
        {
            DB::rollback();
            //return response()->json(['message' => 'Error'],422);
            return response()->json(['message' => $ex],422);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($congresista_detalle_id)
    {
        $item = Congresistas_cf_general::select(
            [
                'id',
                'congresista_detalle_id',
                'conflictos_declarados',
                'actividad_economica',
            ]
        )->where(
            'congresista_detalle_id',
            $congresista_detalle_id
        )->with(['Juntas', 'Cercanias', 'Ingresos', 'Acreencias', 'Bienes', 'Documentos'])->get()->first();

        return response($item, 200);
    }

    public function importacion(Request $request){
        try {
            $user = $request->user;
            $congresistas_importar = $request['congresistas_importar'];
            $file = $_FILES['file'];
            $inputFileName = $file['tmp_name'];
            $sheetname = 'REPORTE_PNATURAL';

            $spreadsheet = IOFactory::load($inputFileName);
            $worksheet = $spreadsheet->getSheetByName($sheetname);
            $rows = $worksheet->toArray();

            $firstRow = true;

            foreach($rows as $row) {
                if ($firstRow) {
                    $firstRow = false;
                    continue;
                }
                $nombre_congresista = '';

                if ($row[2]) {
                    $nombre_congresista = strtoupper(Str::ascii($row[2])) . " ";
                }
                if ($row[3]) {
                    $nombre_congresista .= strtoupper(Str::ascii($row[3])) . " ";
                }
                if ($row[4]) {
                    $nombre_congresista .= strtoupper(Str::ascii($row[4])) . " ";
                }
                if ($row[5]) {
                    $nombre_congresista .= strtoupper(Str::ascii($row[5])) . " ";
                }

                $nombre_congresista = rtrim($nombre_congresista, " ");

                $count_congresistas_importar = count($congresistas_importar);
                if($count_congresistas_importar == 0){
                    break;
                }

                $i = 0;
                while ($i < $count_congresistas_importar) {
                    if ($nombre_congresista == $congresistas_importar[$i]['nombre_completo']) {
                        $congresista_detalle_id = $congresistas_importar[$i]['id'];

                        Congresistas_cf_general::where
                        (
                            [
                                ['congresista_detalle_id', $congresista_detalle_id],
                            ]
                        )->delete();

                        $congresista_cv_general = Congresistas_cf_general::firstOrNew(
                            array(
                                'congresista_detalle_id' => $congresista_detalle_id,
                            )
                        );

                        $congresista_cv_general->conflictos_declarados = $row[69];
                        $congresista_cv_general->actividad_economica = $row[48];
                        $congresista_cv_general->activo = 1;

                        if($congresista_cv_general->id == 0){
                            $congresista_cv_general->congresista_detalle_id = $congresista_detalle_id;
                            $congresista_cv_general->usercreated = $user;
                        }
                        else{
                            $congresista_cv_general->usermodifed = $user;
                        }

                        $congresista_cv_general->save();

                        // Importamos las juntas
                        $items_congresistasCfJuntasAsociaciones = $this->getCongresistasCfJuntasAsociacionesFromRow($row, $congresista_detalle_id, $user);

                        // Importamos las cercanias
                        $items_congresistaCfInfoCercanias = $this->getCongresistasCfInfoCercaniasFromRow($row, $congresista_detalle_id, $user);

                        // Importamos los ingresos
                        $items_congresistaCfIngresos = $this->getCongresistasCfIngresosFromRow($row, $congresista_detalle_id, $user);

                        // Importamos las acreencias
                        $items_congresistaCfAcreencias = $this->getCongresistasCfAcreenciasFromRow($row, $congresista_detalle_id, $user);

                        // Importamos los bienes
                        $items_congresistaCfBienes = $this->getCongresistasCfBienesFromRow($row, $congresista_detalle_id, $user);

                        $count_juntas = count($items_congresistasCfJuntasAsociaciones);
                        $count_cercanias = count($items_congresistaCfInfoCercanias);
                        $count_ingresos = count($items_congresistaCfIngresos);
                        $count_acreencias = count($items_congresistaCfAcreencias);
                        $count_bienes = count($items_congresistaCfBienes);

                        for($i = 0 ; $i < $count_juntas ; $i++){
                            $items_congresistasCfJuntasAsociaciones[$i]->save();
                        }

                        for($i = 0 ; $i < $count_cercanias ; $i++){
                            $items_congresistaCfInfoCercanias[$i]->save();
                        }

                        for($i = 0 ; $i < $count_ingresos ; $i++){
                            $items_congresistaCfIngresos[$i]->save();
                        }

                        for($i = 0 ; $i < $count_acreencias ; $i++){
                            $items_congresistaCfAcreencias[$i]->save();
                        }

                        for($i = 0 ; $i < $count_bienes ; $i++){
                            $items_congresistaCfBienes[$i]->save();
                        }

                        /* Eliminando  */
                        unset($congresistas_importar[$i]);
                        $congresistas_importar = array_values($congresistas_importar);
                        $count_congresistas_importar = count($congresistas_importar);
                    } else {
                        $i++;
                    }
                }

            }
        }
        catch (QueryException $ex)
        {
            //return response()->json(['message' => 'Error'],422);
            return response()->json(['message' => $ex],422);
        }
    }

    public function excel(Request $request)
    {
        try{
            $cuatrienio_id = $request['idCuatrienio'];
            $corporacion_id = $request['idCorporacion'];
            $file = $_FILES['file'];

            $inputFileName = $file['tmp_name'];
            $sheetname = 'REPORTE_PNATURAL';

            /** Load $inputFileName to a Spreadsheet Object  **/
            $spreadsheet = IOFactory::load($inputFileName);
            $worksheet = $spreadsheet->getSheetByName($sheetname);
            $rows = $worksheet->toArray();

            $congresistas_encontrados = [];
            $congresistas_no_encontrados = [];
            $index_to_delete = [];

            $firstRow = true;
            foreach($rows as $row){
                if($firstRow){
                    $firstRow = false;
                    continue;
                }
                $nombre_congresista = '';

                if($row[2]){
                    $nombre_congresista = strtoupper(Str::ascii($row[2])) . " ";
                }
                if($row[3]){
                    $nombre_congresista .= strtoupper(Str::ascii($row[3])) . " ";
                }

                if($row[4]){
                    $nombre_congresista .= strtoupper(Str::ascii($row[4])) . " ";
                }

                if($row[5]){
                    $nombre_congresista .= strtoupper(Str::ascii($row[5])) . " ";
                }

                $nombre_congresista = rtrim($nombre_congresista, " ");
                $congresistas_no_encontrados[] = ["id" => 0, "nombre_completo" => $nombre_congresista];
            }
            $congresistas_db = $this->getNombreCompletoCongresistas($cuatrienio_id, $corporacion_id);

            $count_congresistas_no_encontrados = count($congresistas_no_encontrados);
            $count_congresistas_db = count($congresistas_db);

            for($i = 0;  $i < $count_congresistas_no_encontrados; $i++){
                for($ii = 0;  $ii < $count_congresistas_db; $ii++) {
                    if($congresistas_no_encontrados[$i]['nombre_completo'] == $congresistas_db[$ii]->nombre_completo){
                        $congresistas_encontrados[] = ["id" => $congresistas_db[$ii]->id, "nombre_completo" => $congresistas_no_encontrados[$i]['nombre_completo']];
                        $index_to_delete[] = $i;
                    }
                }
            }

            $count_index_to_delete = count($index_to_delete);
            for($i = 0;  $i < $count_index_to_delete; $i++){
                unset($congresistas_no_encontrados[$index_to_delete[$i]]);
            }

            $congresistas_no_encontrados = array_values($congresistas_no_encontrados);

            sort($congresistas_encontrados);
            sort($congresistas_no_encontrados);

            $result = [
                'congresistas_encontrados' => $congresistas_encontrados,
                'congresistas_no_encontrados' => $congresistas_no_encontrados,
            ];

            return ($result);

        }
        catch (QueryException $ex)
        {
            //return response()->json(['message' => 'Error'],422);
            return response()->json(['message' => $ex],422);
        }
    }

    private function getNombreCompletoCongresistas($cuatrienio_id, $corporacion_id): array
    {
        $result = DB::select("
           select cd.id, upper(concat(p.nombres, ' ', p.apellidos)) as nombre_completo
            from congresistas as c
                     join personas p on c.persona_id = p.id
                     join congresista_detalles cd on c.id = cd.congresista_id
            where c.activo = 1
            and cd.cuatrienio_id = ".$cuatrienio_id."
            and cd.corporacion_id = ".$corporacion_id."
            group by cd.id
            order by nombre_completo;
            ");

        foreach ($result as $item){
            $item->nombre_completo = Str::ascii($item->nombre_completo);
        }

        return $result;
    }

    public function getAllPersonasEnCorpCuatrienio(Request $request){
        $filter = $request->input('idFilterActive');
        $cuatrienio = $request->input('cuatrienio');
        $corporacion = $request->input('corporacion');
        $search = $request->input('search');

        /* Buscamos a los congresistas que SI están en el cuatrienio, y almacenamos las id's de las personas en un arreglo */
        $queryCongresistasEnCuatrienio = DB::select('
        select cc2.persona_id from congresistas cc2
        left join congresista_detalles cd on cd.congresista_id = cc2.id
        where cd.cuatrienio_id = '.$cuatrienio.'
        and cd.corporacion_id = '.$corporacion.'
        and cc2.activo = 1
        ');
        $IDCongresistasEnCuatrienio = array();
        foreach ($queryCongresistasEnCuatrienio as $key => $congresista) {
            $IDCongresistasEnCuatrienio[$key] = $congresista->persona_id;
        }
        // dd($IDCongresistasEnCuatrienio);
        $items = Persona::select([
            'personas.id',
            'personas.nombres',
            'personas.apellidos',
            'personas.municipio_id_nacimiento',
            'congresista_detalles.id as congresista_detalle_id'
        ])
        ->with("LugarNacimiento", "Profesion", "Imagenes")
        ->leftJoin('congresistas', 'congresistas.persona_id', 'personas.id')
        ->leftJoin('congresista_detalles', 'congresista_detalles.congresista_id', 'congresistas.id')
        ->whereIn('personas.id', $IDCongresistasEnCuatrienio)
        ->where('personas.activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->where(function ($query) use ($search){
            $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%".$search."%")
                ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%".$search."%");
        })
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->orderBy('personas.id','desc')
        ->get()
        ->toJson(JSON_PRETTY_PRINT);

        return response($items, 200);
    }

    public function totalrecordsPersonasEnCorpCuatrienio(Request $request){
        $filter = $request->input('idFilterActive');
        $cuatrienio = $request->input('cuatrienio');
        $corporacion = $request->input('corporacion');
        $search = $request->input('search');

        /* Buscamos a los congresistas que SI están en el cuatrienio, y almacenamos las id's de las personas en un arreglo */
        $queryCongresistasEnCuatrienio = DB::select('
        select cc2.persona_id from congresistas cc2
        left join congresista_detalles cd on cd.congresista_id = cc2.id
        where cd.cuatrienio_id = '.$cuatrienio.'
        and cd.corporacion_id = '.$corporacion.'
        and cc2.activo = 1
        ');
        $IDCongresistasEnCuatrienio = array();
        foreach ($queryCongresistasEnCuatrienio as $key => $congresista) {
            $IDCongresistasEnCuatrienio[$key] = $congresista->persona_id;
        }

        $count = Persona::with("LugarNacimiento", "Profesion", "Imagenes")
        ->leftJoin('congresistas', 'congresistas.persona_id', 'personas.id')
        ->leftJoin('congresista_detalles', 'congresista_detalles.congresista_id', 'congresistas.id')
        ->whereIn('personas.id', $IDCongresistasEnCuatrienio)
        ->where('personas.activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->where(function ($query) use ($search){
            $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%".$search."%")
                ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%".$search."%");
        })
        ->count();

        return response($count, 200);
    }

    private function getCfOrganos(){
        $result = DB::select("
            select id, upper(Nombre) as nombre
            from cf_organos
            where activo = 1;
        ");

        return collect($result);
    }

    private function getCfCalidadMiembro(){
        $result = DB::select("
            select id, upper(Nombre) as nombre
            from cf_calidad_miembros
            where activo = 1;
        ");

        return collect($result);
    }

    private function getPaises(){
        $result = DB::select("
            select id, upper(Nombre) as nombre
            from pais
            where activo = 1;
        ");

        return collect($result);
    }

    private function getCfParentescos(){
        $result = DB::select("
            select id, upper(Nombre) as nombre
            from cf_parentescos
            where activo = 1;
        ");

        return collect($result);
    }

    private function getCfDepartamentos(){
        $result = DB::select("
            select id, pais_id, upper(Nombre) as nombre
            from cf_departamentos
            where activo = 1;
        ");

        return collect($result);
    }

    private function getCongresistasCfJuntasAsociacionesFromRow($row, $congresista_detalle_id, $user): array
    {
        $items_congresistas_cf_juntas_asociaciones = [];
        if(isset($row[46])){
            $cf_organos = $this->getCfOrganos();
            $cf_calidad_miembros = $this->getCfCalidadMiembro();
            $paises = $this->getPaises();

            Congresistas_cf_juntas_asociaciones::where
            (
                [
                    ['congresista_detalle_id', $congresista_detalle_id],
                ]
            )->delete();

            $array_particip_corp_socied_asoc_info = explode('|', $row[46]);

            foreach($array_particip_corp_socied_asoc_info as $row_junta){
                $array_row_junta = explode(';', $row_junta);

                $item_congresistas_cf_juntas_asociaciones = new Congresistas_cf_juntas_asociaciones();
                $item_congresistas_cf_juntas_asociaciones->nombre_entidad = $array_row_junta[0];

                $nombre_cf_organo = strtoupper($array_row_junta[1]);
                $item_cf_organo = $cf_organos->first(function ($item) use ($nombre_cf_organo) {
                    return $item->nombre == $nombre_cf_organo;
                });
                if(isset($item_cf_organo)){
                    $item_congresistas_cf_juntas_asociaciones->cf_organo_id = $item_cf_organo->id;
                }

                $nombre_cf_calidad_miembro = strtoupper($array_row_junta[2]);
                $item_cf_calidad_miembro = $cf_calidad_miembros->first(function ($item) use ($nombre_cf_calidad_miembro) {
                    return $item->nombre == $nombre_cf_calidad_miembro;
                });
                if(isset($item_cf_calidad_miembro)){
                    $item_congresistas_cf_juntas_asociaciones->cf_calidad_miembro_id = $item_cf_calidad_miembro->id;
                }

                $nombre_pais = strtoupper($array_row_junta[3]);
                $item_pais= $paises->first(function ($item) use ($nombre_pais) {
                    return $item->nombre == $nombre_pais;
                });
                if(isset($item_pais)){
                    $item_congresistas_cf_juntas_asociaciones->pais_id = $item_pais->id;
                }

                $item_congresistas_cf_juntas_asociaciones->congresista_detalle_id = $congresista_detalle_id;
                $item_congresistas_cf_juntas_asociaciones->activo = 1;
                $item_congresistas_cf_juntas_asociaciones->usercreated = $user;

                $items_congresistas_cf_juntas_asociaciones[] = $item_congresistas_cf_juntas_asociaciones;
            }
        }

        return $items_congresistas_cf_juntas_asociaciones;
    }

    private function getCongresistasCfInfoCercaniasFromRow($row, $congresista_detalle_id, $user): array
    {
        $hayConyuge = false;
        $hayParientes = false;
        $cf_parentescos = [];
        $items_congresistaCfInfoCercanias = [];

        if((isset($row[51]) || isset($row[52]) || isset($row[53]) || isset($row[54])) || isset($row[59])){

            $hayConyuge = true;
            $hayParientes = true;
            $cf_parentescos = $this->getCfParentescos();
            CongresistasCfInfoCercanias::where
            (
                [
                    ['congresista_detalle_id', $congresista_detalle_id],
                ]
            )->delete();
        }

        if($hayConyuge)
        {
            $nombre_conyuge = '';

            if (isset($row[51])) {
                $nombre_conyuge = $row[51] . " ";
            }
            if (isset($row[52])) {
                $nombre_conyuge .= $row[52] . " ";
            }

            if (isset($row[53])) {
                $nombre_conyuge .= $row[53] . " ";
            }

            if (isset($row[54])) {
                $nombre_conyuge .= $row[54] . " ";
            }

            $nombre_conyuge = rtrim($nombre_conyuge, " ");

            $item_congresistaCfInfoCercanias = new CongresistasCfInfoCercanias();
            $item_congresistaCfInfoCercanias->congresista_detalle_id = $congresista_detalle_id;
            $item_congresistaCfInfoCercanias->nombre = $nombre_conyuge;

            $nombre_parentesco = 'CÓNYUGE';
            $item_parentesco= $cf_parentescos->first(function ($item) use ($nombre_parentesco) {
                return $item->nombre == $nombre_parentesco;
            });
            if(isset($item_parentesco)){
                $item_congresistaCfInfoCercanias->cf_parentesco_id = $item_parentesco->id;
            }

            $item_congresistaCfInfoCercanias->conflicto_interes = $row[57];
            $item_congresistaCfInfoCercanias->usercreated = $user;
            $item_congresistaCfInfoCercanias->activo = 1;
            $items_congresistaCfInfoCercanias[] = $item_congresistaCfInfoCercanias;
        }

        if($hayParientes){
            /* Como tiene una cercanía eliminamos los registros anteriores  */
            CongresistasCfInfoCercanias::where
            (
                [
                    ['congresista_detalle_id', $congresista_detalle_id],
                ]
            )->delete();
            $cell_cercania = $row[59];
            $array_slash_cell_cercania = explode('|', $cell_cercania);
            foreach($array_slash_cell_cercania as $item_slash_cell_cercania){
                $array_semicolon_cell_cercania = explode(';', $item_slash_cell_cercania);
                $nombre_pariente = $array_semicolon_cell_cercania[1];

                $item_congresistaCfInfoCercanias = new CongresistasCfInfoCercanias();
                $item_congresistaCfInfoCercanias->congresista_detalle_id = $congresista_detalle_id;
                $item_congresistaCfInfoCercanias->nombre = $nombre_pariente;

                $nombre_parentesco = strtoupper($array_semicolon_cell_cercania[0]);
                $item_parentesco= $cf_parentescos->first(function ($item) use ($nombre_parentesco) {
                    return $item->nombre == $nombre_parentesco;
                });
                if(isset($item_parentesco)){
                    $item_congresistaCfInfoCercanias->cf_parentesco_id = $item_parentesco->id;
                }

                $item_congresistaCfInfoCercanias->conflicto_interes = $array_semicolon_cell_cercania[4];
                $item_congresistaCfInfoCercanias->usercreated = $user;
                $item_congresistaCfInfoCercanias->activo = 1;
                $items_congresistaCfInfoCercanias[] = $item_congresistaCfInfoCercanias;
            }
        }

        return $items_congresistaCfInfoCercanias;
    }

    private function getCongresistasCfIngresosFromRow($row, $congresista_detalle_id, $user): array
    {
        $items_congresistaCfIngresos = [];

        CongresistasCfIngresos::where
        (
            [
                ['congresista_detalle_id', $congresista_detalle_id],
            ]
        )->delete();

        $item_congresistaCfIngresos = new CongresistasCfIngresos();
        $item_congresistaCfIngresos->congresista_detalle_id = $congresista_detalle_id;
        $item_congresistaCfIngresos->usercreated = $user;
        $item_congresistaCfIngresos->activo = 1;

        $item_congresistaCfIngresos->salario_anual = (float)$row[33];
        $item_congresistaCfIngresos->interes_cesantias_anual =(float) $row[34];
        $item_congresistaCfIngresos->gasto_representacion_anual = (float)$row[35];
        $item_congresistaCfIngresos->arriendo_anual = (float)$row[36];
        $item_congresistaCfIngresos->honorario_anual = (float)$row[37];
        $item_congresistaCfIngresos->otros_anual = (float)$row[38];

        $item_congresistaCfIngresos->total_anual =  $item_congresistaCfIngresos->salario_anual
            + $item_congresistaCfIngresos->interes_cesantias_anual
            + $item_congresistaCfIngresos->gasto_representacion_anual
            + $item_congresistaCfIngresos->arriendo_anual
            + $item_congresistaCfIngresos->honorario_anual
            + $item_congresistaCfIngresos->otros_anual;

        $items_congresistaCfIngresos[] = $item_congresistaCfIngresos;

        return $items_congresistaCfIngresos;
    }

    private function getCongresistasCfAcreenciasFromRow($row, $congresista_detalle_id, $user): array
    {
        $items_congresistaCfAcreencias = [];
        if (isset($row[42])) {

            CongresistasCfAcreencias::where
            (
                [
                    ['congresista_detalle_id', $congresista_detalle_id],
                ]
            )->delete();

            $cell_acreencias = $row[42];
            $array_slash_cell_acreencias = explode('|', $cell_acreencias);

            foreach ($array_slash_cell_acreencias as $item_slash_cell_acreencia) {
                $array_semicolon_cell_acreencia = explode(';', $item_slash_cell_acreencia);
                $item_congresistaCfAcreencia = new CongresistasCfAcreencias();
                $item_congresistaCfAcreencia->congresista_detalle_id = $congresista_detalle_id;
                $item_congresistaCfAcreencia->cf_acreencia_cpt = $array_semicolon_cell_acreencia[0];
                $item_congresistaCfAcreencia->saldo = (float)$array_semicolon_cell_acreencia[1];
                $item_congresistaCfAcreencia->usercreated = $user;
                $item_congresistaCfAcreencia->activo = 1;
                $items_congresistaCfAcreencias[] = $item_congresistaCfAcreencia;
            }
        }
        return $items_congresistaCfAcreencias;
    }

    private function getCongresistasCfBienesFromRow($row, $congresista_detalle_id, $user): array
    {
        $items_congresistaCfBienes = [];
        $paises = [];

        if(isset($row[40]) || isset($row[41])){
            $paises = $this->getPaises();
            CongresistasCfBienes::where
            (
                [
                    ['congresista_detalle_id', $congresista_detalle_id],
                ]
            )->delete();
        }

        if (isset($row[40])) {
            $cell_bienes = $row[40];
            $array_slash_cell_bienes = explode('|', $cell_bienes);

            foreach ($array_slash_cell_bienes as $item_slash_cell_bienes) {
                $array_semicolon_cell_bienes = explode(';', $item_slash_cell_bienes);

                $item_congresistaCfBien = new CongresistasCfBienes();
                $item_congresistaCfBien->congresista_detalle_id = $congresista_detalle_id;
                $item_congresistaCfBien->cf_tipo_bien = $array_semicolon_cell_bienes[0];

                $nombre_pais = strtoupper($array_semicolon_cell_bienes[1]);
                $item_pais= $paises->first(function ($item) use ($nombre_pais) {
                    return $item->nombre == $nombre_pais;
                });
                if(isset($item_pais)){
                    $item_congresistaCfBien->pais_id = $item_pais->id;
                }

                $item_congresistaCfBien->valor = (float)$array_semicolon_cell_bienes[2];
                $item_congresistaCfBien->usercreated = $user;
                $item_congresistaCfBien->activo = 1;
                $items_congresistaCfBienes[] = $item_congresistaCfBien;
            }
        }

        if (isset($row[41])) {
            $cfDepartamentos = $this->getCfDepartamentos();

            $cell_bienes = $row[41];
            $array_slash_cell_bienes = explode('|', $cell_bienes);

            foreach ($array_slash_cell_bienes as $item_slash_cell_bienes) {
                $array_semicolon_cell_bienes = explode(';', $item_slash_cell_bienes);

                $item_congresistaCfBien = new CongresistasCfBienes();
                $item_congresistaCfBien->congresista_detalle_id = $congresista_detalle_id;
                $item_congresistaCfBien->cf_tipo_bien = $array_semicolon_cell_bienes[0];

                $nombre_pais = strtoupper($array_semicolon_cell_bienes[1]);
                $item_pais= $paises->first(function ($item) use ($nombre_pais) {
                    return $item->nombre == $nombre_pais;
                });

                if(isset($item_pais)){
                    $item_congresistaCfBien->pais_id = $item_pais->id;

                    $nombre_departamento = strtoupper($array_semicolon_cell_bienes[2]);
                    $item_departamento= $cfDepartamentos->first(function ($item) use ($nombre_departamento, $item_pais) {
                        return $item->pais_id == $item_pais->id && $item->nombre == $nombre_departamento;
                    });

                    if(isset($item_departamento)){
                        $item_congresistaCfBien->cf_departamento_id = $item_departamento->id;
                    }
                }

                $item_congresistaCfBien->valor = (float)$array_semicolon_cell_bienes[4];
                $item_congresistaCfBien->usercreated = $user;
                $item_congresistaCfBien->activo = 1;
                $items_congresistaCfBienes[] = $item_congresistaCfBien;
            }
        }
        return $items_congresistaCfBienes;
    }
}
