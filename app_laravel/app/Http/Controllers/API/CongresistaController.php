<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Congresista;
use App\Models\CongresistaDetalle;
use App\Models\Investigacion;
use App\Models\CargoCamara;
use App\Models\CongresistaReemplazo;
use Validator;
use App\Messages;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Storage;
use EloquentBuilder;

class CongresistaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->input('idFilter');
        $corporacion = $request->input('corporacion');
        $cuatrienio = $request->input('cuatrienio');
        $partido = $request->input('partido');

        DB::enableQueryLog(); // Enable query log
        $query = Congresista::query();

         if ($request->has('search') && !is_null($request["search"])) {
             $search = $request->input('search');
             $query->Where(
                 function ($query) use ($search) {
                     $query->WhereHas('persona', function ($query) use ( $search ) {
                         $query->whereRaw("MATCH(nombres, apellidos) AGAINST (?)", $search);
                     });
                 }
             );
         }

        $items = $query->select('congresistas.id', 'persona_id', 'congresistas.activo', 'urlHojaVida')
        ->leftJoin('personas', 'congresistas.persona_id', '=', 'personas.id')
        ->with([
            'persona',
            'detalle' => function($q) use ($corporacion, $cuatrienio){
                $q->where('corporacion_id', ($corporacion != "0") ? '=' : '!=', $corporacion)
                ->where('cuatrienio_id', ($cuatrienio != "0") ? '=' : '!=', $cuatrienio);
            }
        ])->where('congresistas.activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->whereHas('detalle', function($q) use ($corporacion, $cuatrienio, $partido){
            $q->where('corporacion_id', ($corporacion != "0") ? '=' : '!=', $corporacion)
            ->where('cuatrienio_id', ($cuatrienio != "0") ? '=' : '!=', $cuatrienio)
            ->where('partido_id', ($partido != "-1") ? '=' : '!=', $partido)
            ->orWhere('partido_id', null);
        })
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->whereNotNull('personas.id')
        ->orderBy('personas.apellidos', 'asc')
        ->get()
        ->toJson(JSON_PRETTY_PRINT);

        return response($items, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), Congresista::$rulesPost,  Congresista::$messagesPost);
        if ($validator->fails()) {
            $messages = array_merge_recursive($validator->errors()->toArray());
            return response()->json($messages, 422);
        }
        $detalleRequest = new Request($request->get('detalle'));
        $validator2 = Validator::make($detalleRequest->all(), CongresistaDetalle::$rulesPost,  CongresistaDetalle::$messagesPost);
        if ($validator2->fails())
            return response()->json($validator2->errors(),422);

        DB::beginTransaction();
        try{
            $request->merge([
                'reemplazado' => $request->reemplazado == "true" ? 1 : 0
            ]);

            // $message = "";

            /* Buscamos si la persona que se envía ya está dada de alta. Si lo está, se le creará solo detalle, sino, se crea desde padre, hasta detalle */
            $busquedaPersona = Congresista::where('persona_id', $request->input('persona_id'))->get()->first();
            $detalle = $request->input('detalle');
            $id = 0;
            $persona_id = 0;
            if($busquedaPersona != null){
                $id = $busquedaPersona->id;
                $persona_id = $busquedaPersona->persona_id;
                // Detalle
                if($detalle != null){
                    $detalle["congresista_id"] = $id;
                    $CongresistaDetalle = new CongresistaDetalle;
                    $CongresistaDetalle->fill($detalle);
                    $CongresistaDetalle->usercreated = $request->user;
                    $CongresistaDetalle->save();
                    // $message.= "Persona ya registrada. Se creó su detalle \n";
                }
            }else{
                $item = new Congresista();
                $request->request->add(['usercreated' => $request->user]);
                $result = $item->create($request->all()); // Congresista creado
                // $message.= "Persona NO registrada SE HA CREADO. \n";
                if($result != null){
                    $id = $result->id;
                    $persona_id = $result->persona_id;

                    if($detalle != null){
                        $detalle["congresista_id"] = $id;
                        $CongresistaDetalle = new CongresistaDetalle;
                        $CongresistaDetalle->fill($detalle);
                        $CongresistaDetalle->usercreated = $request->user;
                        $CongresistaDetalle->save();
                    }

                    // $message.= "Se ha creado el detale de la persona NO registrada. \n";
                }
            }

            // Insertar hoja vida
            $hoja = $request->file('hoja');

            if($hoja != null){
                $path = $hoja->storeAs(
                    '/congresista/'.$id, // Directorio
                    $hoja->getClientOriginalName(), // Nombre real de la imagen
                    'public' // disco
                );
                $item = Congresista::find($id);
                $item->urlHojaVida = $path;
                $item->save();

                // $message.= "Se ha registrado hoja. \n";
            }

            // Investigaciones
            $investigaciones = $request->input('investigaciones');
            if($investigaciones != null)
            {
                foreach($request->input('investigaciones') as $key => $value)
                {
                    $investigaciones[$key]['congresista_id'] = $id;
                    $requestInvestigaciones = new Request($investigaciones[$key]);
                    if($requestInvestigaciones->activo == 1)
                    {
                        $CongresistaInvestigaciones = new Investigacion;
                        $CongresistaInvestigaciones->fill($requestInvestigaciones->all());
                        $CongresistaInvestigaciones->usercreated = $request->user;
                        $CongresistaInvestigaciones->save();

                        // $message.= "Se ha registrado investigación ".$key.". \n";
                    }
                }
            }

            // Cargo
            $cargo = $request->input('cargo');
            if($cargo != null){
                $cargo["congresista_id"] = $id;
                $CongresistaCargo = new CargoCamara;
                $CongresistaCargo->fill($cargo);
                $CongresistaCargo->usercreated = $request->user;
                $CongresistaCargo->save();

                // $message.= "Se ha registrado la información de cargo. \n";
            }

            // Reemplazo
            $reemplazo = $request->input('reemplazo');
            if($reemplazo != null && $reemplazo["persona_id_reemplaza"] != 0){
                $reemplazo["persona_id_reemplazado"] = $persona_id;
                $CongresistaReemplazo = new CongresistaReemplazo;
                $CongresistaReemplazo->fill($reemplazo);
                $CongresistaReemplazo->usercreated = $request->user;
                $CongresistaReemplazo->save();

                // $message.= "Se ha creado el registro de reemplazo. \n";

                // Dar de alta nuevo congresista. Primero volvemos a buscar a la persona, si existe, solo se añade detalle

                $personaParaReemplazo = Congresista::where('persona_id', $reemplazo["persona_id_reemplaza"])->get()->first();
                if($personaParaReemplazo != null){
                    // Buscamos si existe alguna relación con el cuatrienio en el que se intenta registrar, si no hay, se añade, y si existe, no se inserta nada, quedando solo los registros de reemplazo
                    // $message.= "Persona que reemplaza ya ha sido dada de alta como congresista, por lo que se validará si se inserta detalle. \n";
                    $personaParaReemplazoEnCorpYCuatrienio = Congresista::with('detalle')
                    ->where('persona_id', $reemplazo["persona_id_reemplaza"])
                    ->whereHas('detalle', function($q) use ($detalle){
                        $q->where('corporacion_id', $detalle["corporacion_id"])
                        ->where('cuatrienio_id', $detalle["cuatrienio_id"]);
                    })->get()->first();

                    if($personaParaReemplazoEnCorpYCuatrienio == null){
                        $newCongresistaDetalle = new CongresistaDetalle();
                        $newCongresistaDetalle->congresista_id = $personaParaReemplazo->id;
                        $newCongresistaDetalle->corporacion_id = $detalle["corporacion_id"];
                        $newCongresistaDetalle->cuatrienio_id = $detalle["cuatrienio_id"];
                        $newCongresistaDetalle->partido_id = $reemplazo["partido_id"];
                        $newCongresistaDetalle->curul_id = $detalle["curul_id"];
                        $newCongresistaDetalle->usercreated = $request->user;
                        $newCongresistaDetalle->save();
                        // $message.= "Se ha creado detalle de reemplazo, debido a que no se encontró que haya sido dado de alta en este cuatrienio y corporación. \n";
                    }else{
                        // $message.= "No fue necesario insertar detalle de reemplazo. \n";
                    }
                }else{
                    $newCongresista = new Congresista();
                    $newCongresista->persona_id = $reemplazo["persona_id_reemplaza"];
                    $newCongresista->usercreated = $request->user;
                    $newCongresista->save();

                    // $message.= "No se encontró a la persona que reemplaza dada de alta como congresista (tabla padre). Se ha creado nuevo congresista a base de reemplazo. \n";
                    $newCongresistaDetalle = new CongresistaDetalle();
                    $newCongresistaDetalle->congresista_id = $newCongresista->id;
                    $newCongresistaDetalle->corporacion_id = $detalle["corporacion_id"];
                    $newCongresistaDetalle->cuatrienio_id = $detalle["cuatrienio_id"];
                    $newCongresistaDetalle->partido_id = $reemplazo["partido_id"];
                    $newCongresistaDetalle->curul_id = $detalle["curul_id"];
                    $newCongresistaDetalle->usercreated = $request->user;
                    $newCongresistaDetalle->save();

                    // $message.= "Se añadió detalle a congresista que reemplaza que se acaba de crear. \n";
                }
            }

            // dd($message);
            DB::commit();
            return response()->json(['message' => 'OK'], 201);
        }catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['message' => $e], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $Congresista = Congresista::where('id', $id)
        ->with("persona", "investigaciones", "cargo", 'reemplazo', "detalle")
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($Congresista, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), Congresista::$rulesPut,  Congresista::$messagesPut);
        // $validator2 = Validator::make($cPerfilRequest->all(), Congresista::$rulesPutPerfil,  Congresista::$messagesPutPerfil);
        if ($validator->fails()) {
            $messages = array_merge_recursive($validator->errors()->toArray());
            return response()->json($messages, 422);
        }

        $detalleRequest = new Request($request->get('detalle'));
        $validator2 = Validator::make($detalleRequest->all(), CongresistaDetalle::$rulesPut,  CongresistaDetalle::$messagesPut);
        if ($validator2->fails())
            return response()->json($validator2->errors(),422);

        DB::beginTransaction();
        try {
            $request->merge([
                'reemplazado' => $request->reemplazado == "true" || $request->reemplazado == "1" ? 1 : 0
            ]);
            $congresista = Congresista::find($id);
            $request->request->add(['usermodifed' => $request->user]);
            $congresista->fill($request->all());


            $file = $request->file('hoja');
            if($file){
                if(Storage::disk('public')->exists($congresista->urlHojaVida))
                    Storage::disk('public')->delete($congresista->urlHojaVida);

                $path = $file->storeAs(
                    '/congresista/'.$congresista->id, // Directorio
                    $file->getClientOriginalName(), // Nombre real de la imagen
                    'public' // disco
                );
                $congresista->urlHojaVida = $path;
            }
            $congresista->save();

            // Investigaciones

            $investigaciones = $request->input('investigaciones');
            if($investigaciones != null)
            {
                foreach($request->input('investigaciones') as $key => $value)
                {
                    $investigaciones[$key]['congresista_id'] = $id;
                    $requestInvestigaciones = new Request($investigaciones[$key]);
                    if($requestInvestigaciones->id > 0)
                    {
                        if($requestInvestigaciones->activo == 1)
                        {
                            $CongresistaInvestigaciones = Investigacion::find($requestInvestigaciones->id);
                            $CongresistaInvestigaciones->fill($requestInvestigaciones->all());
                            $CongresistaInvestigaciones->usermodifed = $request->user;
                            $CongresistaInvestigaciones->save();
                        }
                        else
                        {
                            $CongresistaInvestigaciones = Investigacion::find($requestInvestigaciones->id);
                            $CongresistaInvestigaciones->activo = 0;
                            $CongresistaInvestigaciones->usermodifed = $request->user;
                            $CongresistaInvestigaciones->save();
                        }
                    }
                    else
                    {
                        if($requestInvestigaciones->activo == 1)
                        {
                            $CongresistaInvestigaciones = new Investigacion;
                            $CongresistaInvestigaciones->fill($requestInvestigaciones->all());
                            $CongresistaInvestigaciones->usercreated = $request->user;
                            $CongresistaInvestigaciones->save();
                        }
                    }
                }
            }

            // Detalle
            $detalle = $request->input('detalle');
            if($detalle != null){
                $CongresistaDetalle = CongresistaDetalle::find($detalle["id"]);
                if($CongresistaDetalle == null){
                    $detalle["congresista_id"] = $id;
                    $CongresistaDetalle = new CongresistaDetalle;
                    $CongresistaDetalle->fill($detalle);
                    $CongresistaDetalle->usercreated = $request->user;
                    $CongresistaDetalle->save();
                }else{
                    $CongresistaDetalle->fill($detalle);
                    $CongresistaDetalle->usermodifed = $request->user;
                    $CongresistaDetalle->save();
                }
            }

            // Cargo
            $cargo = $request->input('cargo');
            if($cargo != null){
                $CongresistaCargo = CargoCamara::find($cargo["id"]);
                if($cargo["cargo_legislativo_id"] != 0){
                    if($CongresistaCargo == null){
                        $cargo["congresista_id"] = $id;
                        $CongresistaCargo = new CargoCamara;
                        $CongresistaCargo->fill($cargo);
                        $CongresistaCargo->usercreated = $request->user;
                        $CongresistaCargo->save();
                    }else{
                        $CongresistaCargo->fill($cargo);
                        $CongresistaCargo->usermodifed = $request->user;
                        $CongresistaCargo->save();
                    }
                }
            }

            $message = "";
            // Reemplazo
            $reemplazo = $request->input('reemplazo');
            if($reemplazo != null){
                if($reemplazo["persona_id_reemplaza"] != 0){
                    if($reemplazo["id"] != 0){ // Si ya se tiene un registro en existencia, buscará por la id de registro
                        $message .= "Se tiene un registro en existencia";
                        $CongresistaReemplazo = CongresistaReemplazo::find($reemplazo["id"]);
                        if($reemplazo["persona_id_reemplaza"] != $CongresistaReemplazo->persona_id_reemplaza){
                            // DB::table('congresista_reemplazos')
                            // ->where('persona_id_reemplazado', $reemplazo["persona_id_reemplazado"])
                            // ->update(['activo' => 0]);
                            $message .= "La persona que reemplaza enviada es diferente a la persona que reemplaza anterior";
                            $reemplazo["persona_id_reemplazado"] = $congresista->persona_id;
                            $CongresistaReemplazo = new CongresistaReemplazo;
                            $CongresistaReemplazo->fill($reemplazo);
                            $CongresistaReemplazo->usercreated = $request->user;
                            $CongresistaReemplazo->save();
                            // dd("se insertó reemplazo");

                            $message .= "Se insertó reemplazo";
                            $personaParaReemplazo = Congresista::where('persona_id', $reemplazo["persona_id_reemplaza"])->get()->first();
                            if($personaParaReemplazo != null){
                                $message .= "Se encontró persona ya dada de alta como congresista";
                                $personaParaReemplazoEnCorpYCuatrienio = Congresista::with('detalle')
                                ->where('persona_id', $reemplazo["persona_id_reemplaza"])
                                ->whereHas('detalle', function($q) use ($detalle){
                                    $q->where('corporacion_id', $detalle["corporacion_id"])
                                    ->where('cuatrienio_id', $detalle["cuatrienio_id"]);
                                })->get()->first();

                                if($personaParaReemplazoEnCorpYCuatrienio == null){
                                    $message .= "No se encontró detalle de persona. Creando";
                                    $newCongresistaDetalle = new CongresistaDetalle();
                                    $newCongresistaDetalle->congresista_id = $personaParaReemplazo->id;
                                    $newCongresistaDetalle->corporacion_id = $detalle["corporacion_id"];
                                    $newCongresistaDetalle->cuatrienio_id = $detalle["cuatrienio_id"];
                                    $newCongresistaDetalle->partido_id = $reemplazo["partido_id"];
                                    $newCongresistaDetalle->curul_id = $detalle["curul_id"];
                                    $newCongresistaDetalle->usercreated = $request->user;
                                    $newCongresistaDetalle->save();
                                    $message .= "Se ha creado detalle de persona";
                                }else{
                                    $message .= "Se encontró detalle, por lo que no es necesario crear más registros";
                                }
                            }else{
                                $message .= "No se encontró a la persona dada de alta como congresista. Creando registro padre";
                                $newCongresista = new Congresista();
                                $newCongresista->persona_id = $reemplazo["persona_id_reemplaza"];
                                $newCongresista->usercreated = $request->user;
                                $newCongresista->save();

                                $message .= "Creando registro hijo";

                                // $message.= "No se encontró a la persona que reemplaza dada de alta como congresista (tabla padre). Se ha creado nuevo congresista a base de reemplazo. \n";
                                $newCongresistaDetalle = new CongresistaDetalle();
                                $newCongresistaDetalle->congresista_id = $newCongresista->id;
                                $newCongresistaDetalle->corporacion_id = $detalle["corporacion_id"];
                                $newCongresistaDetalle->cuatrienio_id = $detalle["cuatrienio_id"];
                                $newCongresistaDetalle->partido_id = $reemplazo["partido_id"];
                                $newCongresistaDetalle->curul_id = $detalle["curul_id"];
                                $newCongresistaDetalle->usercreated = $request->user;
                                $newCongresistaDetalle->save();
                                $message .= "Padre e hijo creado";
                            }
                        }else{
                            $message .= "No hubo cambios en las personas enviadas. Se actualizan datos de fecha y partido";
                            $CongresistaReemplazo->fill($reemplazo);
                            $CongresistaReemplazo->usermodifed = $request->user;
                            $CongresistaReemplazo->save();
                        }
                    }else{
                        // creamos nuevo reemplazo asignando la id de persona
                        $message .= "No se encontró un registro de reemplazo. Creando";
                        $reemplazo["persona_id_reemplazado"] = $congresista->persona_id;
                        $CongresistaReemplazo = new CongresistaReemplazo;
                        $CongresistaReemplazo->fill($reemplazo);
                        $CongresistaReemplazo->usercreated = $request->user;
                        $CongresistaReemplazo->save();

                        // Buscamos si nuestro reemplazo ya ha sido dado de alta como congresista
                        $personaParaReemplazo = Congresista::where('persona_id', $reemplazo["persona_id_reemplaza"])->get()->first();
                        if($personaParaReemplazo != null){ // si existe
                            $message .= "Se encontró la persona ya dada de alta como congresista. Buscando detalle";
                            $personaParaReemplazoEnCorpYCuatrienio = Congresista::with('detalle')
                            ->where('persona_id', $reemplazo["persona_id_reemplaza"])
                            ->whereHas('detalle', function($q) use ($detalle){
                                $q->where('corporacion_id', $detalle["corporacion_id"])
                                ->where('cuatrienio_id', $detalle["cuatrienio_id"]);
                            })->get()->first();

                            if($personaParaReemplazoEnCorpYCuatrienio == null){
                                $message .= "No se encontró detalle. Creando";
                                $newCongresistaDetalle = new CongresistaDetalle();
                                $newCongresistaDetalle->congresista_id = $personaParaReemplazo->id;
                                $newCongresistaDetalle->corporacion_id = $detalle["corporacion_id"];
                                $newCongresistaDetalle->cuatrienio_id = $detalle["cuatrienio_id"];
                                $newCongresistaDetalle->partido_id = $reemplazo["partido_id"];
                                $newCongresistaDetalle->curul_id = $detalle["curul_id"];
                                $newCongresistaDetalle->usercreated = $request->user;
                                $newCongresistaDetalle->save();
                            }else{
                                $message .= "Se encontró detalle, por lo que no es necesario crear más registros";
                            }
                        }
                    }
                }
            }

            // dd($message);
            DB::commit();
            return response()->json(['message' => 'OK'], 202);
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return response()->json(['message' => $e], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Congresista = Congresista::find($id);
        $Congresista->activo=!$Congresista->activo;
        $Congresista->save();

        return response($Congresista, 200);
    }

    public function totalrecords(Request $request)
    {
        $filter = $request->input('idFilter');
        $corporacion = $request->input('corporacion');
        $cuatrienio = $request->input('cuatrienio');
        $partido = $request->input('partido');
        $count = Congresista::select('congresistas.id', 'persona_id', 'congresistas.activo', 'urlHojaVida')
            ->leftJoin('personas', 'congresistas.persona_id', '=', 'personas.id')
            ->with([
                'persona',
                'detalle' => function($q) use ($corporacion, $cuatrienio){
                    $q->where('corporacion_id', ($corporacion != "0") ? '=' : '!=', $corporacion)
                    ->where('cuatrienio_id', ($cuatrienio != "0") ? '=' : '!=', $cuatrienio);
                }
            ])
            ->where('congresistas.activo', ($filter != "-1") ? '=' : '!=', $filter)
            ->whereHas('detalle', function($q) use ($corporacion, $cuatrienio, $partido){
                $q->where('corporacion_id', ($corporacion != "0") ? '=' : '!=', $corporacion)
                ->where('cuatrienio_id', ($cuatrienio != "0") ? '=' : '!=', $cuatrienio)
                ->where('partido_id', ($partido != "-1") ? '=' : '!=', $partido)
                ->orWhere('partido_id', null);
            })
            ->whereHas('persona', function($q) use ($request){
                $q->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%".$request->input('search')."%")
                ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%".$request->input('search')."%");
            })
            ->whereNotNull('personas.id')
            ->count();

        return response($count, 200);
    }

    public function getCongresistas(Request $request){

        $query = Congresista::query();

        if ($request->has('id')) {
            $query->where('id' , $request->id);
        }

        // if ($request->has('nombres')) {
        //     $query->where('nombres', $request->nombres);
        // }

        if ($request->has('corporacion_id')) {
            $query->whereHas('detalle', function($q) use ($request){
                $q->where('corporacion_id', $request->corporacion_id);
            });
        }

        if ($request->has('cuatrienio_id')) {
            $query->whereHas('detalle', function($q) use ($request){
                $q->where('cuatrienio_id', $request->cuatrienio_id);
            });
        }

        if ($request->has('partido_id')) {
            $query->whereHas('detalle', function($q) use ($request){
                $q->where('partido_id', $request->partido_id);
            });
        }

        if ($request->has('curul_id')) {
            $query->whereHas('detalle', function($q) use ($request){
                $q->where('curul_id', $request->curul_id);
            });
        }

        // if ($request->has('fechaNacimiento')) {
        //     $query->where('fechaNacimiento', $request->fechaNacimiento);
        // }

        // if ($request->has('lugarNacimiento')) {
        //     $query->where('lugarNacimiento', $request->lugarNacimiento);
        // }

        if ($request->has('activo')) {
            $query->where('activo', $request->activo);
        }

        $result = $query->get();

        return response($result, 200);
    }

}
