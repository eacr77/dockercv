<?php

namespace App\Http\Controllers\CLIENTAPI;

use App\Http\Controllers\Controller;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DatosCongresistasConflictoInteresController extends Controller
{
    public function totalCongresistasConflictoInteresParientes(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_corporacion = $corporacion_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
                select ifnull(sum(case when tblAux.genero_id = 1 then 1 else 0 end), 0) as hombres,
                    ifnull(sum(case when tblAux.genero_id = 2 then 1 else 0 end), 0) as mujeres
                from (
                         select p.genero_id
                         from personas as p
                                  join congresistas as c on p.id = c.persona_id
                                  join congresista_detalles cd on c.id = cd.congresista_id
                         where cd.id in (
                             select congresista_detalle_id
                             from congresistas_cf_info_cercanias as ccic
                             group by congresista_detalle_id
                         )
                           and p.activo = 1
                           and c.activo = 1
                           and cd.corporacion_id ".$operador_corporacion." " . $corporacion_id  . "
                           and cd.cuatrienio_id " . $operador_cuatrienio . " " . $cuatrienio_id . "
                         group by p.id
                     ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }

    public function totalProyectoLeyConflictoInteres(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_corporacion = $corporacion_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
                select ifnull(count(*),0) as total
                from (
                         select plal.proyecto_ley_id
                         from proyecto_ley_autor_legislativos as plal
                         where congresista_id in (
                             select c.id
                             from congresistas as c
                                      join congresista_detalles cd on c.id = cd.congresista_id
                             where cd.id in (
                                 select congresista_detalle_id
                                 from congresistas_cf_generals as ccg
                                 group by congresista_id
                             )
                               and c.activo = 1
                               and cd.corporacion_id ".$operador_corporacion." " . $corporacion_id  . "
                               and cd.cuatrienio_id " . $operador_cuatrienio . " " . $cuatrienio_id . "
                             group by c.id
                         )
                           and plal.activo = 1
                         group by plal.proyecto_ley_id
                     ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }

    public function totalCongresistasConflictoInteresCamara(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
                select ifnull(count(*), 0) as total
                from (
                         select c.id
                         from congresistas as c
                                  join congresista_detalles cd on c.id = cd.congresista_id
                         where cd.id in (
                             select congresista_detalle_id
                             from congresistas_cf_documentos as cfdoc
                             where cfdoc.cf_tipo_documento_id = 3
                             group by congresista_detalle_id
                         )
                           and c.activo = 1
                           and cd.corporacion_id = 1
                           and cd.cuatrienio_id " . $operador_cuatrienio . " " . $cuatrienio_id . "
                         group by c.id
                     ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }

    public function totalCongresistasConflictoInteresSenado(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
                select ifnull(count(*), 0) as total
                from (
                         select c.id
                         from congresistas as c
                                  join congresista_detalles cd on c.id = cd.congresista_id
                         where cd.id in (
                             select congresista_detalle_id
                             from congresistas_cf_documentos as cfdoc
                             where cfdoc.cf_tipo_documento_id = 3
                             group by congresista_detalle_id
                         )
                           and c.activo = 1
                           and cd.corporacion_id = 2
                           and cd.cuatrienio_id " . $operador_cuatrienio . " " . $cuatrienio_id . "
                         group by c.id
                     ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }

    public function totalCongresistasReportaronCampaniasCamara(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
                select ifnull(count(*), 0) as total
                from (
                         select c.id
                         from congresistas as c
                                  join congresista_detalles cd on c.id = cd.congresista_id
                         where cd.id in (
                             select congresista_detalle_id
                             from congresistas_cf_documentos as cfdoc
                             where cfdoc.cf_tipo_documento_id = 1
                             group by congresista_detalle_id
                         )
                           and c.activo = 1
                           and cd.corporacion_id = 1
                           and cd.cuatrienio_id " . $operador_cuatrienio . " " . $cuatrienio_id . "
                         group by c.id
                     ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }

    function totalCongresistasReportaronCampaniasSenado(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
                select ifnull(count(*), 0) as total
                from (
                         select c.id
                         from congresistas as c
                                  join congresista_detalles cd on c.id = cd.congresista_id
                         where cd.id in (
                             select congresista_detalle_id
                             from congresistas_cf_documentos as cfdoc
                             where cfdoc.cf_tipo_documento_id = 1
                             group by congresista_detalle_id
                         )
                           and c.activo = 1
                           and cd.corporacion_id = 2
                           and cd.cuatrienio_id " . $operador_cuatrienio . " " . $cuatrienio_id . "
                         group by c.id
                     ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }

    public function totalCongresistasParticipanJuntas(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_corporacion = $corporacion_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            select ifnull(count(*), 0) as total
            from (
                     select c.id
                     from congresistas as c
                              join congresista_detalles cd on c.id = cd.congresista_id
                     where cd.id in (
                         select congresista_detalle_id
                         from congresistas_cf_juntas_asociaciones as cjuntas
                         group by congresista_detalle_id
                     )
                       and c.activo = 1
                       and cd.corporacion_id ".$operador_corporacion." " . $corporacion_id  . "
                       and cd.cuatrienio_id " . $operador_cuatrienio . " " . $cuatrienio_id . "
                     group by c.id
                 ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }

    public function porcentajeIngresosPublicosPrivados(Request $request)
    {
        try {

            $cuatrienio_id = $request->input('cuatrienio_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_corporacion = $corporacion_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
                select ifnull(100 * SUM(tblAux.salario_anual) / SUM(tblAux.ingresos_publicos), 0)                                as salario_anual,
                       ifnull(100 * SUM(tblAux.interes_cesantias_anual) / SUM(tblAux.ingresos_publicos),
                              0)                                                                                                 as interes_cesantias_anual,
                       ifnull(100 * SUM(tblAux.gasto_representacion_anual) / SUM(tblAux.ingresos_publicos),
                              0)                                                                                                 as gasto_representacion_anual,
                       ifnull(100 * SUM(tblAux.honorario_anual) / SUM(tblAux.ingresos_publicos),
                              0)                                                                                                 as honorario_anual,
                       ifnull(100 * SUM(tblAux.arriendo_anual) / SUM(tblAux.ingresos_privados),
                              0)                                                                                                 as arriendo_anual,
                       ifnull(100 * SUM(tblAux.otros_anual) / SUM(tblAux.ingresos_privados),
                              0)                                                                                                 as otros_anual,
                       ifnull(100 * SUM(tblAux.ingresos_publicos) / SUM(tblAux.ingresos_publicos + tblAux.ingresos_privados),
                              0)                                                                                                 as ingresos_publicos,
                       ifnull(100 * SUM(tblAux.ingresos_privados) / SUM(tblAux.ingresos_publicos + tblAux.ingresos_privados),
                              0)                                                                                                 as ingresos_privados
                from (
                         select cfi.salario_anual,
                                cfi.interes_cesantias_anual,
                                cfi.gasto_representacion_anual,
                                cfi.honorario_anual,
                                cfi.arriendo_anual,
                                cfi.otros_anual,
                                SUM(cfi.salario_anual + cfi.interes_cesantias_anual + cfi.gasto_representacion_anual +
                                    cfi.honorario_anual)                  as ingresos_publicos,
                                SUM(cfi.arriendo_anual + cfi.otros_anual) as ingresos_privados
                         from congresistas as c
                                  join congresista_detalles cd on c.id = cd.congresista_id
                                  join congresistas_cf_ingresos cfi on cd.id = cfi.congresista_detalle_id
                         where c.activo = 1
                            and cd.corporacion_id ".$operador_corporacion." ".$corporacion_id."
                            and cd.cuatrienio_id  ".$operador_cuatrienio." ".$cuatrienio_id."
                         group by c.id
                     ) as tblAux;
            ");

            return response($result);
        } catch (QueryException $e) {
            return response()->json(['message' => 'Error'], 422);
        }
    }
}
