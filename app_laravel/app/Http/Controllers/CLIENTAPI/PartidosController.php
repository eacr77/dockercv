<?php

namespace App\Http\Controllers\CLIENTAPI;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Models\Partido;
use App\Models\PartidoImagen;
use App\Models\PartidoDatosContacto;
use Validator;
use DB;

class PartidosController extends Controller
{
    public function show($id)
    {
        $partido = Partido::with('partidoDatosContacto')
        ->with('partidoImagen')
        ->where('id', $id)
        ->orderBy('fechaDeCreacion', 'desc')
        ->get()->first()
        ->toJson(JSON_PRETTY_PRINT);
        return response($partido, 200);
    }
}
