<?php

namespace App\Http\Controllers\CLIENTAPI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\AgendaLegislativa;
use App\Models\AgendaLegislativaActividad;
use App\Models\AgendaLegislativaComision;
use App\Models\Votacion;
use App\Models\ControlPolitico;
use App\Models\Eleccion;
use App\Models\Partido;

class ActividadesLegislativasController extends Controller
{
    public function getAgenda(Request $request){
        $fecha=$request->input('fecha');
        $tactividad=$request->input('idtactividad');
        $corporacion=$request->input('idcorporacion');
        $comision=$request->input('idcomision');
        $AgendaLegislativaComision = AgendaLegislativaActividad::select('id','agenda_legislativa_id','titulo','destacado','tipo_actividad_id','proyecto_ley_id','control_politico_id','activo')
        ->with('agenda','tipoActividad','selected','citacion')
        ->whereHas('agenda', function($q) use ($fecha,$corporacion,$comision){
                $q->whereDate('fecha',$fecha)
                ->whereHas('agendaComision',function($u) use ($corporacion,$comision){
                    $u->where('corporacion_id', ($corporacion != "-1") ? '=' : '!=', $corporacion)
                    ->where('comision_id', ($comision != "-1") ? '=' : '!=', $comision);
                });
        })
        ->where('activo', $request->input('idFilter'))
        ->where('destacado', 1)
        ->where('tipo_actividad_id', ($tactividad != "-1") ? '=' : '!=', $tactividad)
        ->where('titulo', 'LIKE', '%' . $request->input('search') . '%')
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->orderBy('orden','asc')
        ->orderBy('id','asc')
        ->get()
        ->toJson(JSON_PRETTY_PRINT);

        return response($AgendaLegislativaComision, 200);
     }
    public function totalrecordsAgenda(Request $request){
        $fecha=$request->input('fecha');
        $tactividad=$request->input('idtactividad');
        $corporacion=$request->input('idcorporacion');
        $comision=$request->input('idcomision');
        $AgendaLegislativaActividad = AgendaLegislativaActividad::select('id','agenda_legislativa_id')
        ->with('agenda','tipoActividad')
        ->whereHas('agenda', function($q) use ($fecha,$corporacion,$comision){
                $q->whereDate('fecha', $fecha)
                ->whereHas('agendaComision',function($u) use ($corporacion,$comision){
                    $u->where('corporacion_id', ($corporacion != "-1") ? '=' : '!=', $corporacion)
                    ->where('comision_id', ($comision != "-1") ? '=' : '!=', $comision);
                });
        })
        ->where('activo', $request->input('idFilter'))
        ->where('destacado', 1)
        ->where('tipo_actividad_id', ($tactividad != "-1") ? '=' : '!=', $tactividad)
        ->where('titulo', 'LIKE', '%' . $request->input('search') . '%')
        ->count();
        return response($AgendaLegislativaActividad, 200);
    }
    public function totalrecordsVotaciones(Request $request){
        $filter = $request->input('idFilter');
        $corporacion = $request->input('corporacion');
        $legislatura = $request->input('legislatura');
        $cuatrienio = $request->input('cuatrienio');
        $proyectoLey = $request->input('search');
        $comision = $request->input('comision');
        $tipoVotacion = $request->input('tipoVotacion');

        $query = Votacion::query();
        

        $query->with("legislatura", "cuatrienio", "votacionCongresista", "proyectoDeLey", "votacionPlenaria", "votacionComision")
        ->where('activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->where('legislatura_id', ($legislatura != "-1") ? '=' : '!=', $legislatura)
        ->where('cuatrienio_id', ($cuatrienio != "-1") ? '=' : '!=', $cuatrienio);
        if($tipoVotacion == "1"){
            $query->where('esComision', true);
        }
        else if ($tipoVotacion == "2"){
            $query->where('esPlenaria', true);
        }
        $query->whereHas('votacionPlenaria', function($q) use ($corporacion){
            if($corporacion != "-1"){
                $q->where('corporacion_id', $corporacion);
            }
        });
        $query->orWhereHas('votacionComision', function($q) use ($corporacion, $comision){
            if($corporacion != "-1"){
                $q->where('corporacion_id', $corporacion);
            }
            if($comision != "-1"){
                $q->where('comision_id', $comision);
            }
        });
        $query->whereHas('proyectoDeLey', function($q) use ($proyectoLey){
            $q->where('titulo', 'LIKE', '%' . $proyectoLey . '%');
        });

        if($tipoVotacion == "1"){
            $query->where('esComision', '=', 1);
        }
        else if ($tipoVotacion == "2"){
            $query->where('esPlenaria', '=', 1);
        }
        $count =  $query->count();

        return response($count, 200);
    }
    public function getVotaciones(Request $request){
        $filter = $request->input('idFilter');
        $corporacion = $request->input('corporacion');
        $legislatura = $request->input('legislatura');
        $cuatrienio = $request->input('cuatrienio');
        $proyectoLey = $request->input('search');
        $comision = $request->input('comision');
        $tipoVotacion = $request->input('tipoVotacion');

        $query = Votacion::query();

        

        $query->with("legislatura", "cuatrienio", "votacionCongresista", "proyectoDeLey", "votacionPlenaria", "votacionComision")
        ->where('activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->where('legislatura_id', ($legislatura != "-1") ? '=' : '!=', $legislatura)
        ->where('cuatrienio_id', ($cuatrienio != "-1") ? '=' : '!=', $cuatrienio);
        if($tipoVotacion == "1"){
            $query->where('esComision', '=', 1);
        }
        else if ($tipoVotacion == "2"){
            $query->where('esPlenaria', '=', 1);
        }
        $query->whereHas('votacionPlenaria', function($q) use ($corporacion){
            if($corporacion != "-1"){
                $q->where('corporacion_id', $corporacion);
            }
        });
        $query->orWhereHas('votacionComision', function($q) use ($corporacion, $comision){
            if($corporacion != "-1"){
                $q->where('corporacion_id', $corporacion);
            }
            if($comision != "-1"){
                $q->where('comision_id', $comision);
            }
        });
        $query->whereHas('proyectoDeLey', function($q) use ($proyectoLey){
            $q->where('titulo', 'LIKE', '%' . $proyectoLey . '%');
        });
        $query->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->orderBy('id','desc');
        if($tipoVotacion == "1"){
            $query->where('esComision', '=', 1);
        }
        else if ($tipoVotacion == "2"){
            $query->where('esPlenaria', '=', 1);
        }
        $items = $query->get()
        ->toJson(JSON_PRETTY_PRINT);

        return response($items, 200);
    }
    public function totalrecordsControlPolitico(Request $request){
        $filter = $request->input('idFilter');
        $legislatura = $request->input('legislatura');
        $cuatrienio = $request->input('cuatrienio');
        $comision = $request->input('comision');
        $estado = $request->input('estado');
        $tema = $request->input('tema');
        $corporacion = $request->input('corporacion');
        $nombre = $request->input('search');
        $tipoCitacion = $request->input('tipoCitacion');

        $query = ControlPolitico::query();

        if (!is_null($corporacion) && $corporacion != '-1')
        {
            $query->where(
                'corporacion_id',
                $corporacion
            );
        }
        if (!is_null($legislatura) && $legislatura != '-1')
        {
            $query->where(
                'legislatura_id',
                $legislatura
            );
        }
        if (!is_null($cuatrienio) && $cuatrienio != '-1')
        {
            $query->where(
                'cuatrienio_id',
                $cuatrienio
            );
        }
        if (!is_null($estado) && $estado != '-1')
        {
            $query->where(
                'estado_control_politico_id',
                $estado
            );
        }
        if (!is_null($tema) && $tema != '-1')
        {
            $query->where(
                'tema_id_principal',
                $tema
            );
        }
        if (!is_null($comision) && $comision != '-1')
        {
            $query->where(
                'comision_id',
                $comision
            );
        }
        if (!is_null($tipoCitacion) && $tipoCitacion != '-1')
        {
            $query->where( function ($query) use ($tipoCitacion){
                $query->WhereHas('controlPoliticoCitados', function ($query) use ($tipoCitacion){
                    $query->where('tipo_citacion',  $tipoCitacion );
                });
            });
        }
        if ($request->has('search') && !is_null($request["search"])) {
            $search = $request->input('search');
            $query->Where(function ($query) use ($search) {
                    $query->Where('titulo', 'like', '%' . $search . '%');
                    $query->orWhere(function ($query) use ($search) {
                        $query->whereHas('controlPoliticoCitados', function ($query) use ($search){
                            $query->WhereHas('persona', function ($query) use ($search){
                            $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                            });
                        });
                        $query->orWhereHas('controlPoliticoCitados', function ($query) use ($search){
                            $query->WhereHas('persona', function ($query) use ($search){
                                $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                            });
                        });
                    });
                }
            );
        }

        $count = $query->select([
                'id',
                'titulo',
                'fecha',
                'comision_id',
                'legislatura_id',
                'cuatrienio_id',
                'estado_control_politico_id',
                'tema_id_principal',
                'tema_id_secundario',
                'corporacion_id',
                'detalles',
                'numero_proposicion',
                'activo',]
        )->with(
            'legislatura',
            'cuatrienio',
            'estadoControlPolitico',
            'comision',
            'temaPrincipalControlPolitico',
            'temaSecundarioControlPolitico',
            'corporacion',
            'controlPoliticoCitantes',
            'controlPoliticoCitados'
        )->where(
            'activo',
            ($filter != "-1") ? '=' : '!=',
            $filter
        )
            ->count();

        return response($count);
    }
    public function getControlPolitico(Request $request){
        $filter = $request->input('idFilter');
        $legislatura = $request->input('legislatura');
        $cuatrienio = $request->input('cuatrienio');
        $comision = $request->input('comision');
        $estado = $request->input('estado');
        $tema = $request->input('tema');
        $corporacion = $request->input('corporacion');
        $tipoCitacion = $request->input('tipoCitacion');

        $query = ControlPolitico::query();

        if (!is_null($corporacion) && $corporacion != '-1')
        {
            $query->where(
                'corporacion_id',
                $corporacion
            );
        }
        if (!is_null($legislatura) && $legislatura != '-1')
        {
            $query->where(
                'legislatura_id',
                $legislatura
            );
        }
        if (!is_null($cuatrienio) && $cuatrienio != '-1')
        {
            $query->where(
                'cuatrienio_id',
                $cuatrienio
            );
        }
        if (!is_null($estado) && $estado != '-1')
        {
            $query->where(
                'estado_control_politico_id',
                $estado
            );
        }
        if (!is_null($tema) && $tema != '-1')
        {
            $query->where(
                'tema_id_principal',
                $tema
            );
        }
        if (!is_null($comision) && $comision != '-1')
        {
            $query->where(
                'comision_id',
                $comision
            );
        }
        if (!is_null($tipoCitacion) && $tipoCitacion != '-1')
        {
            $query->where( function ($query) use ($tipoCitacion){
                $query->WhereHas('controlPoliticoCitados', function ($query) use ($tipoCitacion){
                    $query->where('tipo_citacion',  $tipoCitacion );
                });
            });
        }

        if ($request->has('search') && !is_null($request["search"])) {
            $search = $request->input('search');
            $search = str_replace(' ', '%', $search);
            $query->Where(function ($query) use ($search) {
                    $query->Where('titulo', 'like', '%' . $search . '%');
                    $query->orWhere(function ($query) use ($search) {
                        $query->whereHas('controlPoliticoCitados', function ($query) use ($search){
                            $query->WhereHas('persona', function ($query) use ($search){
                            $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                            });
                        });
                        $query->orWhereHas('controlPoliticoCitados', function ($query) use ($search){
                            $query->WhereHas('persona', function ($query) use ($search){
                                $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                            });
                        });
                    });
                }
            );
        }

        $controlPolitico = $query->select([
            'id',
            'titulo',
            'fecha',
            'comision_id',
            'legislatura_id',
            'cuatrienio_id',
            'estado_control_politico_id',
            'tema_id_principal',
            'tema_id_secundario',
            'corporacion_id',
            'detalles',
            'numero_proposicion',
            'activo',]
        )->with(
            'legislatura',
            'cuatrienio',
            'estadoControlPolitico',
            'comision',
            'temaPrincipalControlPolitico',
            'temaSecundarioControlPolitico',
            'corporacion',
            'controlPoliticoCitantes',
            'controlPoliticoCitados'
        )->where(
            'activo',
            ($filter != "-1") ? '=' : '!=',
            $filter
        )
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->orderBy('cuatrienio_id','desc')
        ->groupBy('id')
        ->get()
        ->toJson(JSON_PRETTY_PRINT);

        return response($controlPolitico);
    }
    public function totalrecordsElecciones(Request $request){
        $filter = $request->input('idFilter');
        $corporacion = $request->input('corporacion');
        $cuatrienio = $request->input('cuatrienio');
        $comision = $request->input('comision');
        $titulo = $request->input('search');
        $count = Eleccion::where('activo', $request->input('idFilter'))
        ->where('activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->where('corporacion_id', ($corporacion != "-1") ? '=' : '!=', $corporacion)
        ->where('comision_id', ($comision != "-1") ? '=' : '!=', $comision)
        ->where('cuatrienio_id', ($cuatrienio != "-1") ? '=' : '!=', $cuatrienio)
        ->where('titulo', 'LIKE', '%' . $titulo . '%' )
        ->count();
        return response($count, 200);
    }
    public function getElecciones(Request $request){
        $filter = $request->input('idFilter');
        $corporacion = $request->input('corporacion');
        $cuatrienio = $request->input('cuatrienio');
        $comision = $request->input('comision');
        $titulo = $request->input('search');
        $eleccion = Eleccion::with('cuatrienio', 'comision', 'funcionarioActual', 'corporacion', 'cargoProveer')
        ->where('activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->where('corporacion_id', ($corporacion != "-1") ? '=' : '!=', $corporacion)
        ->where('comision_id', ($comision != "-1") ? '=' : '!=', $comision)
        ->where('cuatrienio_id', ($cuatrienio != "-1") ? '=' : '!=', $cuatrienio)
        ->where('titulo', 'LIKE', '%' . $titulo . '%' )
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($eleccion, 200);
    }
    public function totalrecordsPartidos(Request $request){
        $filter = $request->input('idFilter');
        $count = Partido::where('activo', ($filter != "-1") ? '=' : '!=', $filter)
        ->where('nombre', 'LIKE', '%' . $request->input('search') . '%' )
        ->count();
        return response($count, 200);
    }
    public function getPartidos(Request $request){
        $filter = $request->input('idFilter');
        $partido = Partido::select('id','nombre','fechaDeCreacion', 'resenaHistorica', 'lineamientos', 'lugar', 'color', 'estatutos','activo', 'partidoActivo')
        ->with('partidoImagen', 'partidoDatosContacto')
        ->where('activo', 1)
        ->where('partidoActivo', ($filter != "-1") ? '=' : '!=', ($filter == "2") ? 0 : $filter)
        ->where('nombre', 'LIKE', '%' . $request->input('search') . '%' )
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->orderBy('id','desc')
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($partido, 200);
    }

    public function getAgendaByFecha(Request $request){
        // $fecha = "2021-03-09";
        //$corporacion = $request->input('corporacion');
        // $tipocomision = $request->input('tipocomision');
        $AgendaLegislativa = AgendaLegislativa::select('id','fecha','activo')
        ->with("agendaActividad","cuatrienio")
        ->where('fecha',$request->fecha)
        ->where('activo', $request->input('idFilter'))
        //->where('corporacion_id', ($corporacion != "-1") ? '=' : '!=', $corporacion)
        ->where('fecha', 'LIKE', '%' . $request->input('search') . '%')
        // ->where('tipo_comision_id', ($tipocomision != "-1") ? '=' : '!=', $tipocomision)
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->orderBy('id','desc')
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($AgendaLegislativa, 200);
    }

    public function getAgendaActividad(Request $request){
            $fecha=$request->input('fecha');
            $tactividad=$request->input('idtactividad');
            $corporacion=$request->input('idcorporacion');
            $comision=$request->input('idcomision');
            $destacado = 0;
            if($request->input('destacado') != null)
                $destacado = $request->input('destacado');

            $AgendaLegislativaComision = AgendaLegislativaActividad::select('id','agenda_legislativa_id','titulo','destacado','tipo_actividad_id','proyecto_ley_id','control_politico_id','activo')
            ->with('agenda','tipoActividad','selected','citacion')
            ->whereHas('agenda', function($q) use ($fecha,$corporacion,$comision){
                    $q->whereDate('fecha',$fecha)
                    ->whereHas('agendaComision',function($u) use ($corporacion,$comision){
                        $u->where('corporacion_id', ($corporacion != "-1") ? '=' : '!=', $corporacion)
                        ->where('comision_id', ($comision != "-1") ? '=' : '!=', $comision);
                    });
            })

            ->where('activo', $request->input('idFilter'))
            ->where('tipo_actividad_id', ($tactividad != "-1") ? '=' : '!=', $tactividad)
            ->where('titulo', 'LIKE', '%' . $request->input('search') . '%')
            ->where('destacado', $destacado)
            ->skip(($request->input('page') - 1) * $request->input('rows'))
            ->take($request->input('rows'))
            ->orderBy('orden','asc')
            ->orderBy('id','asc')
            ->get()
            ->toJson(JSON_PRETTY_PRINT);

        return response($AgendaLegislativaComision, 200);
    }

    public function totalrecordsAgendaActividad(Request $request){

        $fecha=$request->input('fecha');
        $tactividad=$request->input('idtactividad');
        $corporacion=$request->input('idcorporacion');
        $comision=$request->input('idcomision');
            $AgendaLegislativaActividad = AgendaLegislativaActividad::select('id','agenda_legislativa_id')
            ->with('agenda','tipoActividad')
            ->whereHas('agenda', function($q) use ($fecha,$corporacion,$comision){

                    $q->whereDate('fecha', $fecha)
                    ->whereHas('agendaComision',function($u) use ($corporacion,$comision){
                        $u->where('corporacion_id', ($corporacion != "-1") ? '=' : '!=', $corporacion)
                        ->where('comision_id', ($comision != "-1") ? '=' : '!=', $comision);
                    });

            })
            ->where('activo', $request->input('idFilter'))
            ->where('tipo_actividad_id', ($tactividad != "-1") ? '=' : '!=', $tactividad)
            ->where('titulo', 'LIKE', '%' . $request->input('search') . '%')
            ->count();
            return response($AgendaLegislativaActividad, 200);


    }

    public function getAgendaDetalle($id){

        $agendaactividad = AgendaLegislativaActividad::select(
            'id',
            'agenda_legislativa_id',
            'titulo',
            'descripcion',
            'destacado',
            'realizado',
            'tipo_actividad_id',
            'proyecto_ley_id',
            'control_politico_id',
            'activo')
            ->with('agenda','tipoActividad','selected','citacion')
            ->where('id', $id)
            ->get()
            ->toJson(JSON_PRETTY_PRINT);
            return response($agendaactividad, 200);
    }
    public function getDataByYearAndMonth(Request $request){
        $destacado=$request->input('destacado');
        $year = $request->input('year');
        $month = $request->input('month');
        $dataCalendar = AgendaLegislativa::with('agendaActividad')
        ->whereHas('agendaActividad', function($q) use ($destacado){
           if($destacado)
            $q->where('destacado', $destacado);
        })
        ->where('activo', 1)
        ->whereYear('fecha', '=', $year)
        ->whereMonth('fecha', '=', $month)
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($dataCalendar, 200);
    }
}
