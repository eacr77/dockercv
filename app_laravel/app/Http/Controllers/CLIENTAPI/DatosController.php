<?php

namespace App\Http\Controllers\CLIENTAPI;

use App\Http\Controllers\Controller;
use App\Models\Cuatrienio;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Exception;

class DatosController extends Controller
{
    public function camara_temas_recurrentes_por_partido(Request $request)
    {
        try
        {

            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `partido`, `tema`, COUNT(*) AS `n`, `color`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`, `partido_id`, `color`, `partido`, `LHS`.`tema` AS `tema`, `n`
            FROM (SELECT DISTINCT *
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`, `partido_id`, `color`, `partido`, `tema`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`, `partido_id`, `color`, `partido`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`, `partido_id`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`, `corporacion`, `tipo_proyecto`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`, `corporacion`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `fechaInicio`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`
            FROM `proyecto_ley_autor_legislativos`) `LHS`
            LEFT JOIN (SELECT `id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_ley_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `fechaInicio`
            FROM `legislaturas`) `RHS`
            ON (`LHS`.`legislatura_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`LHS`.`corporacion_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `tipo_proyecto`
            FROM `tipo_proyectos`) `RHS`
            ON (`LHS`.`tipo_proyecto_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombres`, `apellidos`
            FROM `personas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `persona_id`
            FROM `congresistas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`persona_id`)
            JOIN (SELECT `congresista_id` AS `cid`, `partido_id` FROM `congresista_detalles`) `CD`
            ON (`RHS`.`id` = `CD`.`cid`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `color`, `nombre` AS `partido`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`id`)
            ) `q01`) `LHS`
            INNER JOIN (SELECT `tema`, COUNT(*) AS `n`
            FROM (SELECT `LHS`.`id` AS `id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema`
            FROM (SELECT `id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `tema_id_principal`
            FROM `proyecto_leys`) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`id`)
            ) `q01`
            GROUP BY `tema`
            LIMIT 20) `RHS`
            ON (`LHS`.`tema` = `RHS`.`tema`)
            ) `q02`
            WHERE (`tipo_proyecto` = 'Proyecto de Ley' 
            AND `corporacion_id` ".$operador_corporacion." ".$corporacion_id." 
            AND `legislatura_id` ".$operador_legislatura." ".$legislatura_id." 
            AND `cuatrienio_id` = ".$cuatrienio_id.")
            GROUP BY `partido`, `tema`) `q03`
            WHERE (NOT(((`partido`) IS NULL)))
            ");

            return response($result);
        }
        catch (QueryException $e)
        {
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function camara_origen_iniciativas(Request $request)
    {
        try
        {
            $corporacion_id = $request->input('corporacion_id');
            $legislatura_id = $request->input('legislatura_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;
            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;

            $result = DB::select("
                SELECT `tipo_proyecto`, `iniciativa`, COUNT(*) AS `n`
                FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`, `corporacion`, `iniciativa`, `tipo_proyecto`
                FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`, `corporacion`, `iniciativa`
                FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`, `corporacion`
                FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`
                FROM `proyecto_leys` AS `LHS`
                LEFT JOIN (SELECT `id`, `fechaInicio`
                FROM `legislaturas`) `RHS`
                ON (`LHS`.`legislatura_id` = `RHS`.`id`)
                ) `LHS`
                LEFT JOIN (SELECT `id`, `nombre` AS `corporacion`
                FROM `corporacions`) `RHS`
                ON (`LHS`.`corporacion_id` = `RHS`.`id`)
                ) `LHS`
                LEFT JOIN (SELECT `id`, `nombre` AS `iniciativa`
                FROM `iniciativas`) `RHS`
                ON (`LHS`.`iniciativa_id` = `RHS`.`id`)
                ) `LHS`
                LEFT JOIN (SELECT `id`, `nombre` AS `tipo_proyecto`
                FROM `tipo_proyectos`) `RHS`
                ON (`LHS`.`tipo_proyecto_id` = `RHS`.`id`)
                ) `q01`
                WHERE (`tipo_proyecto` = 'Proyecto de Ley' AND `corporacion_id` ".$operador_corporacion."  ".$corporacion_id." 
                AND `legislatura_id` ".$operador_legislatura."  ".$legislatura_id.")
                GROUP BY `tipo_proyecto`, `iniciativa`
                ORDER BY `n` DESC
            ");

            return response($result);
        }
        catch (QueryException $e)
        {
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function camara_partidos_mayor_numero_autorias_proyecto_ley(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;
            
            $result = DB::select("
            SELECT `partido`, `color`, COUNT(*) AS `n`
            FROM (SELECT *
            FROM (SELECT *
            FROM (SELECT *
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`, `partido_id`, `partido`, `color`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`, `partido_id`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`
            FROM (SELECT `proyecto_ley_id`, `congresista_id`
            FROM `proyecto_ley_autor_legislativos`) `LHS`
            LEFT JOIN (SELECT `id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_ley_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `fechaInicio`
            FROM `legislaturas`) `RHS`
            ON (`LHS`.`legislatura_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`LHS`.`corporacion_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `tipo_proyecto`
            FROM `tipo_proyectos`) `RHS`
            ON (`LHS`.`tipo_proyecto_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombres`, `apellidos`
            FROM `personas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `persona_id`
            FROM `congresistas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`persona_id`)
            JOIN (SELECT `partido_id` FROM `congresista_detalles`) `CD`
            ON (`LHS`.`congresista_id` = `CD`.`partido_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `partido`, `color`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`id`)
            ) `q01`
            WHERE (`tipo_proyecto` = 'Proyecto de Ley')) `q02`
            WHERE (`corporacion_id` ".$operador_corporacion." ".$corporacion_id.")) `q03`
            WHERE (`cuatrienio_id` = ".$cuatrienio_id.")) `q04`
            WHERE (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.")
            GROUP BY `partido`, `color`
            ORDER BY `n` DESC
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function camara_representantes_mayor_numero_autorias_proyecto_ley(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;
            
            $result = DB::select("
                SELECT `nombre_final`, COUNT(*) AS `n`
                FROM (SELECT *
                FROM (SELECT *
                FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`, CONCAT_WS(' ', `nombres`, `apellidos`) AS `nombre_final`
                FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `nombres`, `apellidos`
                FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`
                FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`
                FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`
                FROM (SELECT `proyecto_ley_id`, `congresista_id`, `legislatura_id`, `cuatrienio_id`, `corporacion_id`, `tipo_proyecto_id`
                FROM (SELECT `proyecto_ley_id`, `congresista_id`
                FROM `proyecto_ley_autor_legislativos`) `LHS`
                LEFT JOIN (SELECT `id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `cuatrienio_id`
                FROM `proyecto_leys`) `RHS`
                ON (`LHS`.`proyecto_ley_id` = `RHS`.`id`)
                ) `LHS`
                LEFT JOIN (SELECT `id`, `fechaInicio`
                FROM `legislaturas`) `RHS`
                ON (`LHS`.`legislatura_id` = `RHS`.`id`)
                ) `LHS`
                LEFT JOIN (SELECT `id`, `nombre` AS `corporacion`
                FROM `corporacions`) `RHS`
                ON (`LHS`.`corporacion_id` = `RHS`.`id`)
                ) `LHS`
                LEFT JOIN (SELECT `id`, `nombre` AS `tipo_proyecto`
                FROM `tipo_proyectos`) `RHS`
                ON (`LHS`.`tipo_proyecto_id` = `RHS`.`id`)
                ) `LHS`
                LEFT JOIN (SELECT `id`, `nombres`, `apellidos`
                FROM `personas`) `RHS`
                ON (`LHS`.`congresista_id` = `RHS`.`id`)
                ) `q01`) `q02`
                WHERE (`tipo_proyecto` = 'Proyecto de Ley')) `q03`
                WHERE (`corporacion_id` ".$operador_corporacion."  ".$corporacion_id.")) `q04`
                WHERE (`cuatrienio_id` = ".$cuatrienio_id.")
                AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.")
                GROUP BY `nombre_final`
                ORDER BY `n` DESC
                LIMIT 10;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }

    // Composición

    public function camara_asientos_por_partidos(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `corporacion`, `corporacion_id`, `partido`, `cuatrienio_id`, `color`, COUNT(*) AS `n`, GROUP_CONCAT(`nombre_congresista` SEPARATOR ', ') AS `miembros`
            FROM (SELECT DISTINCT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, CONCAT_WS(' ', `nombres`, `apellidos`) AS `nombre_congresista`
            FROM (SELECT `id`, `LHS`.`persona_id` AS `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `LHS`.`partido_id` AS `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`, `partido`, `color`
            FROM (SELECT `id`, `persona_id`, `CD`.`corporacion_id` AS `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`
            FROM `congresistas` AS `LHS`
            JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
            ON `LHS`.`id` = `CD`.`cid`
            LEFT JOIN (SELECT `id` AS `corporacion_id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`CD`.`corporacion_id` = `RHS`.`corporacion_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `persona_id`, `nombres`, `apellidos`
            FROM `personas`) `RHS`
            ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
            ) `q01`) `q02`
            GROUP BY `corporacion`, `partido`, `cuatrienio_id`, `color`) `q03`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio."  ".$cuatrienio_id.") AND (`corporacion_id` = 1))
            ORDER BY `n` DESC
            ");
            // Se elimina el filtro por legislatura, por la observación de que podrían mostrar muy pocos datos.

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function camara_piramide_poblacional(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT `corporacion`, `corporacion_id`, `cuatrienio_id`, `genero`, `rango`, `n`, CASE
            WHEN (`genero` = 'Masculino') THEN (`n` * -1.0)
            ELSE (`n`)
            END AS `n2`
            FROM (SELECT `corporacion`, `corporacion_id`, `cuatrienio_id`, `genero`, `rango`, COUNT(*) AS `n`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`, `genero`, `nombre_congresista`, `edad`, CASE
            WHEN (`edad` < 18.0) THEN ('Menores de 18')
            WHEN (`edad` >= 18.0 AND `edad` < 30.0) THEN ('18 - 29')
            WHEN (`edad` >= 30.0 AND `edad` < 40.0) THEN ('30 - 39')
            WHEN (`edad` >= 40.0 AND `edad` < 50.0) THEN ('40 - 49')
            WHEN (`edad` >= 50.0 AND `edad` < 60.0) THEN ('50 - 59')
            WHEN (`edad` >= 60.0 AND `edad` < 70.0) THEN ('60 - 69')
            WHEN (`edad` >= 70.0 AND `edad` < 80.0) THEN ('70 - 79')
            WHEN (`edad` >= 80.0) THEN ('80 o más')
            END AS `rango`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`, `genero`, ROUND(DATEDIFF(CURDATE(), `fechaNacimiento`) / 365.0, 0) AS `edad`, CONCAT_WS(' ', `nombres`, `apellidos`) AS `nombre_congresista`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `LHS`.`genero_id` AS `genero_id`, `fechaNacimiento`, `genero`
            FROM (SELECT `id`, `LHS`.`persona_id` AS `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `LHS`.`partido_id` AS `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`
            FROM (SELECT `id`, `persona_id`, `LHS`.`corporacion_id` AS `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `CD`.`cuatrienio_id` AS `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`
            FROM `congresistas` AS `LHS`
            JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
            ON `LHS`.`id` = `CD`.`cid`
            LEFT JOIN (SELECT `id` AS `legislatura`, `cuatrienio_id`
            FROM `legislaturas`) `RHS`
            ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `corporacion_id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`LHS`.`corporacion_id` = `RHS`.`corporacion_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `persona_id`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
            FROM `personas`) `RHS`
            ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `genero_id`, `nombre` AS `genero`
            FROM `generos`) `RHS`
            ON (`LHS`.`genero_id` = `RHS`.`genero_id`)
            ) `q01`) `q02`) `q03`
            GROUP BY `corporacion`, `cuatrienio_id`, `genero`, `rango`) `q04`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`corporacion_id` = 1) AND (NOT(((`rango`) IS NULL))))
            ORDER BY `rango` DESC
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function edad_mediana_por_partidos(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT `partido`, `color`, AVG(`edad`) AS `mediana`
            FROM (SELECT `legislatura`, `cuatrienio_id`, `corporacion`, `partido`, `edad`, CASE
            WHEN (`edad` < 18.0) THEN ('Menores de 18')
            WHEN (`edad` >= 18.0 AND `edad` < 30.0) THEN ('18 - 29')
            WHEN (`edad` >= 30.0 AND `edad` < 40.0) THEN ('30 - 39')
            WHEN (`edad` >= 40.0 AND `edad` < 50.0) THEN ('40 - 49')
            WHEN (`edad` >= 50.0 AND `edad` < 60.0) THEN ('50 - 59')
            WHEN (`edad` >= 60.0 AND `edad` < 70.0) THEN ('60 - 69')
            WHEN (`edad` >= 70.0 AND `edad` < 80.0) THEN ('70 - 79')
            WHEN (`edad` >= 80.0) THEN ('80 o más')
            END AS `rango`, `genero`, `color`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`, `genero`, ROUND(DATEDIFF(CURDATE(), `fechaNacimiento`) / 365.0, 0) AS `edad`, CONCAT_WS(' ', `nombres`, `apellidos`) AS `nombre_congresista`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `LHS`.`genero_id` AS `genero_id`, `fechaNacimiento`, `genero`
            FROM (SELECT `id`, `LHS`.`persona_id` AS `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `LHS`.`partido_id` AS `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`
            FROM (SELECT `id`, `persona_id`, `LHS`.`corporacion_id` AS `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `CD`.`cuatrienio_id` AS `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`
            FROM `congresistas` AS `LHS`
            JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
            ON `LHS`.`id` = `CD`.`cid`
            LEFT JOIN (SELECT `id` AS `legislatura`, `cuatrienio_id`
            FROM `legislaturas`) `RHS`
            ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `corporacion_id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`LHS`.`corporacion_id` = `RHS`.`corporacion_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `persona_id`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
            FROM `personas`) `RHS`
            ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `genero_id`, `nombre` AS `genero`
            FROM `generos`) `RHS`
            ON (`LHS`.`genero_id` = `RHS`.`genero_id`)
            ) `q01`) `q02`) `q03`
            WHERE (`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.")
            GROUP BY `partido`, `color`
            ORDER BY `mediana` DESC LIMIT 20;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function total_proyecto_de_ley_por_genero(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT `genero`, COUNT(*) AS `n`
            FROM (SELECT `genero`, `proyecto_ley_id`
                  FROM (SELECT `congresista_id`, `proyecto_ley_id`, `genero`
                        FROM (SELECT `id`,
                                     `proyecto_ley_id`,
                                     `congresista_id`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `corporacion_id`,
                                     `cuatrienio_id`,
                                     `LHS`.`genero_id` AS `genero_id`,
                                     `nombres`,
                                     `apellidos`,
                                     `nombre_congresista`,
                                     `genero`
                              FROM (SELECT `id`,
                                           `proyecto_ley_id`,
                                           `congresista_id`,
                                           `activo`,
                                           `usercreated`,
                                           `usermodifed`,
                                           `created_at`,
                                           `updated_at`,
                                           `corporacion_id`,
                                           `cuatrienio_id`,
                                           `genero_id`,
                                           `nombres`,
                                           `apellidos`,
                                           CONCAT_WS('', `nombres`, `apellidos`) AS `nombre_congresista`
                                    FROM (SELECT `id`,
                                                 `proyecto_ley_id`,
                                                 `LHS`.`congresista_id` AS `congresista_id`,
                                                 `activo`,
                                                 `usercreated`,
                                                 `usermodifed`,
                                                 `created_at`,
                                                 `updated_at`,
                                                 `corporacion_id`,
                                                 `cuatrienio_id`,
                                                 `genero_id`,
                                                 `nombres`,
                                                 `apellidos`
                                          FROM (SELECT `id`,
                                                       `proyecto_ley_id`,
                                                       `LHS`.`congresista_id` AS `congresista_id`,
                                                       `activo`,
                                                       `usercreated`,
                                                       `usermodifed`,
                                                       `created_at`,
                                                       `updated_at`,
                                                       `corporacion_id`,
                                                       `cuatrienio_id`
                                                FROM `proyecto_ley_autor_legislativos` AS `LHS`
                                                         LEFT JOIN (SELECT `persona_id` AS `congresista_id`
                                                         FROM `congresistas`) `RHS`
                                                         ON (`LHS`.`congresista_id` = `RHS`.`congresista_id`)
                                                         JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
														 ON `LHS`.`congresista_id` = `CD`.`cid`
                                               ) `LHS`
                                                   LEFT JOIN (SELECT `id` AS `congresista_id`, `genero_id`, `nombres`, `apellidos`
                                                              FROM `personas`) `RHS`
                                                             ON (`LHS`.`congresista_id` = `RHS`.`congresista_id`)
                                         ) `q01`) `LHS`
                                       LEFT JOIN (SELECT `id` AS `genero_id`, `nombre` AS `genero`
                                                  FROM `generos`) `RHS`
                                                 ON (`LHS`.`genero_id` = `RHS`.`genero_id`)
                             ) `q01`
                        WHERE (`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.")) `q02`
                  GROUP BY `genero`, `proyecto_ley_id`) `q03`
            GROUP BY `genero`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function minimo_promedio_maximo_cuatrienio(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT `corporacion`,
                   AVG(`n`)           AS `media`,
                   MIN(`n`)           AS `minimo`,
                   MAX(`n`)           AS `maxi`,
                   ROUND(AVG(`n`), 1) AS `media2`
            FROM (SELECT `persona_id`, `corporacion`, COUNT(*) AS `n`
                  FROM (SELECT `id`,
                               `persona_id`,
                               `CD`.`corporacion_id` AS `corporacion_id`,
                               `cuatrienio_id`,
                               `partido_id`,
                               `curul_id`,
                               `circunscripcion_id`,
                               `departamento_id_mayor_votacion`,
                               `urlHojaVida`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `corporacion`
                        FROM `congresistas` AS `LHS`
                        JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
						ON `LHS`.`id` = `CD`.`cid`
                                 LEFT JOIN (SELECT `id` AS `corporacion_id`, `nombre` AS `corporacion`
                                            FROM `corporacions`) `RHS`
                                           ON (`CD`.`corporacion_id` = `RHS`.`corporacion_id`)
                       ) `q01`
                       WHERE cuatrienio_id ".$operador_cuatrienio."  ".$cuatrienio_id."
                  GROUP BY `persona_id`, `corporacion`) `q02`
            GROUP BY `corporacion`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function senado_asientos_por_partidos(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `corporacion`, `corporacion_id`, `partido`, `cuatrienio_id`, `color`, COUNT(*) AS `n`, GROUP_CONCAT(`nombre_congresista` SEPARATOR ', ') AS `miembros`
            FROM (SELECT DISTINCT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, CONCAT_WS(' ', `nombres`, `apellidos`) AS `nombre_congresista`
            FROM (SELECT `id`, `LHS`.`persona_id` AS `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `LHS`.`partido_id` AS `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`, `partido`, `color`
            FROM (SELECT `id`, `persona_id`, `CD`.`corporacion_id` AS `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `corporacion`
            FROM `congresistas` AS `LHS`
            JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
            ON `LHS`.`id` = `CD`.`cid`
            LEFT JOIN (SELECT `id` AS `corporacion_id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`CD`.`corporacion_id` = `RHS`.`corporacion_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `persona_id`, `nombres`, `apellidos`
            FROM `personas`) `RHS`
            ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
            ) `q01`) `q02`
            GROUP BY `corporacion`, `partido`, `cuatrienio_id`, `color`) `q03`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio."  ".$cuatrienio_id.") AND (`corporacion_id` = 2))
            ORDER BY `n` DESC
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function senado_piramide_poblacional(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT `corporacion`, `corporacion_id`, `cuatrienio_id`, `genero`, `rango`, `n`, CASE
            WHEN (`genero` = 'Masculino') THEN (`n` * -1.0)
            ELSE (`n`)
            END AS `n2`
            FROM (SELECT `corporacion`, `corporacion_id`, `cuatrienio_id`, `genero`, `rango`, COUNT(*) AS `n`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`, `genero`, `nombre_congresista`, `edad`, CASE
            WHEN (`edad` < 18.0) THEN ('Menores de 18')
            WHEN (`edad` >= 18.0 AND `edad` < 30.0) THEN ('18 - 29')
            WHEN (`edad` >= 30.0 AND `edad` < 40.0) THEN ('30 - 39')
            WHEN (`edad` >= 40.0 AND `edad` < 50.0) THEN ('40 - 49')
            WHEN (`edad` >= 50.0 AND `edad` < 60.0) THEN ('50 - 59')
            WHEN (`edad` >= 60.0 AND `edad` < 70.0) THEN ('60 - 69')
            WHEN (`edad` >= 70.0 AND `edad` < 80.0) THEN ('70 - 79')
            WHEN (`edad` >= 80.0) THEN ('80 o más')
            END AS `rango`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`, `genero`, ROUND(DATEDIFF(CURDATE(), `fechaNacimiento`) / 365.0, 0) AS `edad`, CONCAT_WS(' ', `nombres`, `apellidos`) AS `nombre_congresista`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `LHS`.`genero_id` AS `genero_id`, `fechaNacimiento`, `genero`
            FROM (SELECT `id`, `LHS`.`persona_id` AS `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `LHS`.`partido_id` AS `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`, `partido`, `color`
            FROM (SELECT `id`, `persona_id`, `LHS`.`corporacion_id` AS `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`, `corporacion`
            FROM (SELECT `id`, `persona_id`, `corporacion_id`, `CD`.`cuatrienio_id` AS `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura`
            FROM `congresistas` AS `LHS`
            JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
            ON `LHS`.`id` = `CD`.`cid`
            LEFT JOIN (SELECT `id` AS `legislatura`, `cuatrienio_id`
            FROM `legislaturas`) `RHS`
            ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `corporacion_id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`LHS`.`corporacion_id` = `RHS`.`corporacion_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `persona_id`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
            FROM `personas`) `RHS`
            ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `genero_id`, `nombre` AS `genero`
            FROM `generos`) `RHS`
            ON (`LHS`.`genero_id` = `RHS`.`genero_id`)
            ) `q01`) `q02`) `q03`
            GROUP BY `corporacion`, `cuatrienio_id`, `genero`, `rango`) `q04`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`corporacion_id` = 2) AND (NOT(((`rango`) IS NULL))))
            ORDER BY `rango` DESC
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    
    // End composición
    public function total_proyecto_de_ley_sancionados_por_anio(Request $request){
        try{
            $result = DB::select("
            SELECT `cuatrienio`, COUNT(*) AS `n`
            FROM (SELECT `id`,
                         `proyecto_ley_id`,
                         `fecha`,
                         `LHS`.`estado_proyecto_ley_id` AS `estado_proyecto_ley_id`,
                         `gaceta_texto`,
                         `gaceta_url`,
                         `nota`,
                         `corporacion_id`,
                         `observaciones`,
                         `orden`,
                         `activo`,
                         `usercreated`,
                         `usermodifed`,
                         `created_at`,
                         `updated_at`,
                         `cuatrienio_id`,
                         `cuatrienio`,
                         `estado`
                  FROM (SELECT `id`,
                               `proyecto_ley_id`,
                               `fecha`,
                               `estado_proyecto_ley_id`,
                               `gaceta_texto`,
                               `gaceta_url`,
                               `nota`,
                               `corporacion_id`,
                               `observaciones`,
                               `orden`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `LHS`.`cuatrienio_id` AS `cuatrienio_id`,
                               `cuatrienio`
                        FROM (SELECT `LHS`.`id` AS `id`,
                                     `proyecto_ley_id`,
                                     `fecha`,
                                     `estado_proyecto_ley_id`,
                                     `gaceta_texto`,
                                     `gaceta_url`,
                                     `nota`,
                                     `corporacion_id`,
                                     `observaciones`,
                                     `orden`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `cuatrienio_id`
                              FROM (SELECT *
                                    FROM `proyecto_ley_estados`
                                    WHERE (`estado_proyecto_ley_id` = '40')) `LHS`
                                       LEFT JOIN (SELECT `id`, `cuatrienio_id`
                                                  FROM `proyecto_leys`) `RHS`
                                                 ON (`LHS`.`proyecto_ley_id` = `RHS`.`id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                            FROM `cuatrienios`) `RHS`
                                           ON (`LHS`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                       ) `LHS`
                           LEFT JOIN (SELECT `id` AS `estado_proyecto_ley_id`, `nombre` AS `estado`
                                      FROM `estado_proyecto_leys`) `RHS`
                                     ON (`LHS`.`estado_proyecto_ley_id` = `RHS`.`estado_proyecto_ley_id`)
                 ) `q01`
            WHERE (`estado_proyecto_ley_id` = 40)
            GROUP BY `cuatrienio`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function cantidad_temas_todos_cuatrienios(Request $request){
        try{
            $result = DB::select("
            SELECT *
            FROM (SELECT `cuatrienio`, `tema`, COUNT(*) AS `n`
                  FROM (SELECT `tema_id_principal`, `n`, `id`, `LHS`.`cuatrienio_id` AS `cuatrienio_id`, `tema`, `cuatrienio`
                        FROM (SELECT `LHS`.`tema_id_principal` AS `tema_id_principal`, `n`, `id`, `cuatrienio_id`, `tema`
                              FROM (SELECT `LHS`.`tema_id_principal` AS `tema_id_principal`, `n`, `id`, `cuatrienio_id`
                                    FROM (SELECT `tema_id_principal`, COUNT(*) AS `n`
                                          FROM `proyecto_leys`
                                          GROUP BY `tema_id_principal`
                                          LIMIT 10) `LHS`
                                             LEFT JOIN (SELECT `id`, `cuatrienio_id`, `tema_id_principal`
                                                        FROM `proyecto_leys`) `RHS`
                                                       ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
                                   ) `LHS`
                                       LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
                                                  FROM `temas`) `RHS`
                                                 ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                            FROM `cuatrienios`) `RHS`
                                           ON (`LHS`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                       ) `q01`
                  GROUP BY `cuatrienio`, `tema`) `q02`
            WHERE (`n` != 1.0);
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function cantidad_temas_un_cuatrienio(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            $result = DB::select("
            SELECT *
            FROM (SELECT *
                  FROM (SELECT `cuatrienio`, `cuatrienio_id`, `tema`, COUNT(*) AS `n`
                        FROM (SELECT `tema_id_principal`, `n`, `id`, `LHS`.`cuatrienio_id` AS `cuatrienio_id`, `tema`, `cuatrienio`
                              FROM (SELECT `LHS`.`tema_id_principal` AS `tema_id_principal`, `n`, `id`, `cuatrienio_id`, `tema`
                                    FROM (SELECT `LHS`.`tema_id_principal` AS `tema_id_principal`, `n`, `id`, `cuatrienio_id`
                                          FROM (SELECT `tema_id_principal`, COUNT(*) AS `n`
                                                FROM `proyecto_leys`
                                                GROUP BY `tema_id_principal`
                                                LIMIT 10) `LHS`
                                                   LEFT JOIN (SELECT `id`, `cuatrienio_id`, `tema_id_principal`
                                                              FROM `proyecto_leys`) `RHS`
                                                             ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
                                         ) `LHS`
                                             LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
                                                        FROM `temas`) `RHS`
                                                       ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
                                   ) `LHS`
                                       LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                                  FROM `cuatrienios`) `RHS`
                                                 ON (`LHS`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                             ) `q01`
                        GROUP BY `cuatrienio`,`cuatrienio_id`, `tema`) `q02`
                  WHERE (`n` != 1.0)) `q03`
            WHERE (`cuatrienio_id` ".$operador_cuatrienio."  ".$cuatrienio_id.");
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function tiempo_sancion_proyectos_de_ley(Request $request){
        try{
            $result = DB::select("
            SELECT `id`, `proyecto_ley_id`, `fecha`, `estado_proyecto_ley_id`, `gaceta_texto`, `gaceta_url`, `nota`, `corporacion_id`, `observaciones`, `orden`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `estado`, `cuatrienio_id`, `tipo_proyecto_id`, `fecha_radicacion`, `proyecto`, `cuatrienio`, `dif`, ROUND(`dif` / 365.0, 2) AS `variable_deborah`
FROM (SELECT `id`, `proyecto_ley_id`, `fecha`, `estado_proyecto_ley_id`, `gaceta_texto`, `gaceta_url`, `nota`, `corporacion_id`, `observaciones`, `orden`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `estado`, `cuatrienio_id`, `tipo_proyecto_id`, `fecha_radicacion`, `proyecto`, `cuatrienio`, DATEDIFF(`fecha`, `fecha_radicacion`) AS `dif`
FROM (SELECT `id`, `proyecto_ley_id`, `fecha`, `estado_proyecto_ley_id`, `gaceta_texto`, `gaceta_url`, `nota`, `corporacion_id`, `observaciones`, `orden`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `estado`, `LHS`.`cuatrienio_id` AS `cuatrienio_id`, `tipo_proyecto_id`, `fecha_radicacion`, `proyecto`, `cuatrienio`
FROM (SELECT `id`, `proyecto_ley_id`, `fecha`, `estado_proyecto_ley_id`, `gaceta_texto`, `gaceta_url`, `nota`, `corporacion_id`, `observaciones`, `orden`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `estado`, `cuatrienio_id`, `LHS`.`tipo_proyecto_id` AS `tipo_proyecto_id`, `fecha_radicacion`, `proyecto`
FROM (SELECT `LHS`.`id` AS `id`, `proyecto_ley_id`, `fecha`, `estado_proyecto_ley_id`, `gaceta_texto`, `gaceta_url`, `nota`, `corporacion_id`, `observaciones`, `orden`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `estado`, `cuatrienio_id`, `tipo_proyecto_id`, `fecha_radicacion`
FROM (SELECT `id`, `proyecto_ley_id`, `fecha`, `LHS`.`estado_proyecto_ley_id` AS `estado_proyecto_ley_id`, `gaceta_texto`, `gaceta_url`, `nota`, `corporacion_id`, `observaciones`, `orden`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `estado`
FROM (SELECT *
FROM `proyecto_ley_estados`
WHERE (`estado_proyecto_ley_id` = '40')) `LHS`
LEFT JOIN (SELECT `id` AS `estado_proyecto_ley_id`, `nombre` AS `estado`
FROM `estado_proyecto_leys`) `RHS`
ON (`LHS`.`estado_proyecto_ley_id` = `RHS`.`estado_proyecto_ley_id`)
) `LHS`
INNER JOIN (SELECT `id`, `cuatrienio_id`, `tipo_proyecto_id`, `fecha_radicacion`
FROM `proyecto_leys`) `RHS`
ON (`LHS`.`proyecto_ley_id` = `RHS`.`id`)
) `LHS`
LEFT JOIN (SELECT `id` AS `tipo_proyecto_id`, `nombre` AS `proyecto`
FROM `tipo_proyectos`) `RHS`
ON (`LHS`.`tipo_proyecto_id` = `RHS`.`tipo_proyecto_id`)
) `LHS`
LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
FROM `cuatrienios`) `RHS`
ON (`LHS`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
) `q01`) `q02`
            ");

            $result_collection = collect($result);
            $proyectos = $result_collection->unique('proyecto')->pluck('proyecto');
            $data_source = [];
            $index_proyecto = 0;
            foreach($proyectos as $proyecto){
                $data = [];
                $data_source[] = [
                    "name" => $proyecto,
                    "data" => $data,
                    "id"=> $proyecto,
                    "type"=> "boxplot"
                ];

                $cuatrienios_por_proyecto = $result_collection->where('proyecto', $proyecto)
                    ->unique('cuatrienio')->pluck('cuatrienio');

                foreach ($cuatrienios_por_proyecto as $cuatrienio){
                    $dif = $result_collection->where('proyecto', $proyecto)
                        ->where('cuatrienio', $cuatrienio)
                        ->sortBy('dif')
                        ->pluck('dif');

                    $low = count($dif) > 0
                        ? $dif[0]
                        : 0;

                    $high = count($dif) > 0
                        ? $dif[count($dif) - 1]
                        : 0;

                    $cuartiles = $this->get_cuartiles_metodo_turkey($dif);

                    $data = [
                        "name" => $cuatrienio,
                        "low" => $low,
                        "q1" => $cuartiles["cuartil_uno"],
                        "median" => $cuartiles["cuartil_dos"],
                        "q3" => $cuartiles["cuartil_tres"],
                        "high" => $high,
                    ];

                    $data_source[$index_proyecto]["data"][] = $data;
                }
                $index_proyecto++;
            }

            return response($data_source);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function proporcion_de_mujeres(Request $request){
        try{
            $result = DB::select("
            SELECT `fechaInicio`, `genero`, COUNT(*) AS `n`
            FROM (SELECT `congresista_id`,
                         `persona_id`,
                         `cuatrienio_id`,
                         `partido_id`,
                         `fechaInicio`,
                         `partido`,
                         `color`,
                         `LHS`.`genero_id` AS `genero_id`,
                         `genero`
                  FROM (SELECT `congresista_id`,
                               `LHS`.`persona_id` AS `persona_id`,
                               `cuatrienio_id`,
                               `partido_id`,
                               `fechaInicio`,
                               `partido`,
                               `color`,
                               `genero_id`
                        FROM (SELECT `congresista_id`,
                                     `persona_id`,
                                     `cuatrienio_id`,
                                     `LHS`.`partido_id` AS `partido_id`,
                                     `fechaInicio`,
                                     `partido`,
                                     `color`
                              FROM (SELECT `congresista_id`,
                                           `persona_id`,
                                           `CD`.`cuatrienio_id` AS `cuatrienio_id`,
                                           `partido_id`,
                                           `fechaInicio`
                                    FROM (SELECT `id` AS `congresista_id`, `persona_id`
                                          FROM `congresistas`) `LHS`
                                          JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
										  ON `LHS`.`congresista_id` = `CD`.`cid`
                                             LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `fechaInicio`
                                                        FROM `cuatrienios`) `RHS`
                                                       ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                                   ) `LHS`
                                       LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
                                                  FROM `partidos`) `RHS`
                                                 ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `persona_id`, `genero_id`
                                            FROM `personas`) `RHS`
                                           ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
                       ) `LHS`
                           LEFT JOIN (SELECT `id` AS `genero_id`, `nombre` AS `genero`
                                      FROM `generos`) `RHS`
                                     ON (`LHS`.`genero_id` = `RHS`.`genero_id`)
                 ) `q01`
            WHERE (NOT (((`genero`) IS NULL)))
            GROUP BY `fechaInicio`, `genero`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function proporcion_de_mujeres_por_cuatrienio(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            
            $result = DB::select("
            SELECT `genero`, `partido`, COUNT(*) AS `n`
            FROM (SELECT `congresista_id`,
                         `persona_id`,
                         `cuatrienio_id`,
                         `partido_id`,
                         `fechaInicio`,
                         `cuatrienio`,
                         `partido`,
                         `color`,
                         `LHS`.`genero_id` AS `genero_id`,
                         `genero`
                  FROM (SELECT `congresista_id`,
                               `LHS`.`persona_id` AS `persona_id`,
                               `cuatrienio_id`,
                               `partido_id`,
                               `fechaInicio`,
                               `cuatrienio`,
                               `partido`,
                               `color`,
                               `genero_id`
                        FROM (SELECT `congresista_id`,
                                     `persona_id`,
                                     `cuatrienio_id`,
                                     `LHS`.`partido_id` AS `partido_id`,
                                     `fechaInicio`,
                                     `cuatrienio`,
                                     `partido`,
                                     `color`
                              FROM (SELECT `congresista_id`,
                                           `persona_id`,
                                           `CD`.`cuatrienio_id` AS `cuatrienio_id`,
                                           `partido_id`,
                                           `fechaInicio`,
                                           `cuatrienio`
                                    FROM (SELECT `id` AS `congresista_id`, `persona_id`
                                          FROM `congresistas`) `LHS`
                                          JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
										  ON `LHS`.`congresista_id` = `CD`.`cid`
                                             LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `fechaInicio`, `nombre` AS `cuatrienio`
                                                        FROM `cuatrienios`) `RHS`
                                                       ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                                   ) `LHS`
                                       LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
                                                  FROM `partidos`) `RHS`
                                                 ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `persona_id`, `genero_id`
                                            FROM `personas`) `RHS`
                                           ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
                       ) `LHS`
                           LEFT JOIN (SELECT `id` AS `genero_id`, `nombre` AS `genero`
                                      FROM `generos`) `RHS`
                                     ON (`LHS`.`genero_id` = `RHS`.`genero_id`)
                 ) `q01`
            WHERE ((NOT (((`genero`) IS NULL))) AND (`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id."))
            GROUP BY `genero`, `partido`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function numero_iniciativas_gubernamentales(Request $request){
        try{
            $result = DB::select("
            SELECT `cuatrienio`, `iniciativa`, COUNT(*) AS `n`
            FROM (SELECT `id`,
                         `LHS`.`cuatrienio_id` AS `cuatrienio_id`,
                         `legislatura_id`,
                         `corporacion_id`,
                         `titulo`,
                         `alias`,
                         `fecha_radicacion`,
                         `numero_camara`,
                         `numero_senado`,
                         `iniciativa_id`,
                         `tipo_proyecto_id`,
                         `tema_id_principal`,
                         `tema_id_secundario`,
                         `sinopsis`,
                         `se_acumula_a_id`,
                         `alcance_id`,
                         `iniciativa_popular`,
                         `activo`,
                         `usercreated`,
                         `usermodifed`,
                         `created_at`,
                         `updated_at`,
                         `iniciativa`,
                         `cuatrienio`
                  FROM (SELECT `id`,
                               `cuatrienio_id`,
                               `legislatura_id`,
                               `corporacion_id`,
                               `titulo`,
                               `alias`,
                               `fecha_radicacion`,
                               `numero_camara`,
                               `numero_senado`,
                               `LHS`.`iniciativa_id` AS `iniciativa_id`,
                               `tipo_proyecto_id`,
                               `tema_id_principal`,
                               `tema_id_secundario`,
                               `sinopsis`,
                               `se_acumula_a_id`,
                               `alcance_id`,
                               `iniciativa_popular`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `iniciativa`
                        FROM `proyecto_leys` AS `LHS`
                                 LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
                                            FROM `iniciativas`) `RHS`
                                           ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
                       ) `LHS`
                           LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                      FROM `cuatrienios`) `RHS`
                                     ON (`LHS`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                 ) `q01`
            WHERE (`iniciativa` = 'Gubernamental')
            GROUP BY `cuatrienio`, `iniciativa`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function top_5_partidos_congresistas_mayor_edad(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            $result = DB::select("
            SELECT `partido`, AVG(`edad`) AS `mediana`
            FROM (SELECT `id`,
                         `persona_id`,
                         `corporacion_id`,
                         `cuatrienio_id`,
                         `partido_id`,
                         `curul_id`,
                         `circunscripcion_id`,
                         `departamento_id_mayor_votacion`,
                         `urlHojaVida`,
                         `activo`,
                         `usercreated`,
                         `usermodifed`,
                         `created_at`,
                         `updated_at`,
                         `cuatrienio`,
                         `partido`,
                         `color`,
                         `nombres`,
                         `apellidos`,
                         `genero_id`,
                         `fechaNacimiento`,
                         ROUND(DATEDIFF(CURDATE(), `fechaNacimiento`) / 365.0, 0) AS `edad`
                  FROM (SELECT `id`,
                               `LHS`.`persona_id` AS `persona_id`,
                               `corporacion_id`,
                               `cuatrienio_id`,
                               `partido_id`,
                               `curul_id`,
                               `circunscripcion_id`,
                               `departamento_id_mayor_votacion`,
                               `urlHojaVida`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `cuatrienio`,
                               `partido`,
                               `color`,
                               `nombres`,
                               `apellidos`,
                               `genero_id`,
                               `fechaNacimiento`
                        FROM (SELECT `id`,
                                     `persona_id`,
                                     `corporacion_id`,
                                     `cuatrienio_id`,
                                     `LHS`.`partido_id` AS `partido_id`,
                                     `curul_id`,
                                     `circunscripcion_id`,
                                     `departamento_id_mayor_votacion`,
                                     `urlHojaVida`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `cuatrienio`,
                                     `partido`,
                                     `color`
                              FROM (SELECT `id`,
                                           `persona_id`,
                                           `corporacion_id`,
                                           `CD`.`cuatrienio_id` AS `cuatrienio_id`,
                                           `partido_id`,
                                           `curul_id`,
                                           `circunscripcion_id`,
                                           `departamento_id_mayor_votacion`,
                                           `urlHojaVida`,
                                           `activo`,
                                           `usercreated`,
                                           `usermodifed`,
                                           `created_at`,
                                           `updated_at`,
                                           `cuatrienio`
                                    FROM `congresistas` AS `LHS`
                                    JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
                                    ON `LHS`.`id` = `CD`.`cid`
                                             LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                                        FROM `cuatrienios`) `RHS`
                                                       ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                                   ) `LHS`
                                       LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
                                                  FROM `partidos`) `RHS`
                                                 ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `persona_id`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
                                            FROM `personas`) `RHS`
                                           ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
                       ) `q01`) `q02`
                        where `cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id."
                        GROUP BY `partido`
                        ORDER BY `mediana` DESC
                        LIMIT 10;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function top_5_partidos_congresistas_mas_jovenes(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";

            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;
            
            $result = DB::select("
            SELECT *
            FROM (SELECT `partido`, AVG(`edad`) AS `mediana`
                  FROM (SELECT `id`,
                               `persona_id`,
                               `corporacion_id`,
                               `cuatrienio_id`,
                               `partido_id`,
                               `curul_id`,
                               `circunscripcion_id`,
                               `departamento_id_mayor_votacion`,
                               `urlHojaVida`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `cuatrienio`,
                               `partido`,
                               `color`,
                               `nombres`,
                               `apellidos`,
                               `genero_id`,
                               `fechaNacimiento`,
                               ROUND(DATEDIFF(CURDATE(), `fechaNacimiento`) / 365.0, 0) AS `edad`
                        FROM (SELECT `id`,
                                     `LHS`.`persona_id` AS `persona_id`,
                                     `corporacion_id`,
                                     `cuatrienio_id`,
                                     `partido_id`,
                                     `curul_id`,
                                     `circunscripcion_id`,
                                     `departamento_id_mayor_votacion`,
                                     `urlHojaVida`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `cuatrienio`,
                                     `partido`,
                                     `color`,
                                     `nombres`,
                                     `apellidos`,
                                     `genero_id`,
                                     `fechaNacimiento`
                              FROM (SELECT `id`,
                                           `persona_id`,
                                           `corporacion_id`,
                                           `cuatrienio_id`,
                                           `LHS`.`partido_id` AS `partido_id`,
                                           `curul_id`,
                                           `circunscripcion_id`,
                                           `departamento_id_mayor_votacion`,
                                           `urlHojaVida`,
                                           `activo`,
                                           `usercreated`,
                                           `usermodifed`,
                                           `created_at`,
                                           `updated_at`,
                                           `cuatrienio`,
                                           `partido`,
                                           `color`
                                    FROM (SELECT `id`,
                                                 `persona_id`,
                                                 `corporacion_id`,
                                                 `CD`.`cuatrienio_id` AS `cuatrienio_id`,
                                                 `partido_id`,
                                                 `curul_id`,
                                                 `circunscripcion_id`,
                                                 `departamento_id_mayor_votacion`,
                                                 `urlHojaVida`,
                                                 `activo`,
                                                 `usercreated`,
                                                 `usermodifed`,
                                                 `created_at`,
                                                 `updated_at`,
                                                 `cuatrienio`
                                          FROM `congresistas` AS `LHS`
                                          JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
										  ON `LHS`.`id` = `CD`.`cid`
                                                   LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                                              FROM `cuatrienios`) `RHS`
                                                             ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                                         ) `LHS`
                                             LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
                                                        FROM `partidos`) `RHS`
                                                       ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
                                   ) `LHS`
                                       LEFT JOIN (SELECT `id` AS `persona_id`,
                                                         `nombres`,
                                                         `apellidos`,
                                                         `genero_id`,
                                                         `fechaNacimiento`
                                                  FROM `personas`) `RHS`
                                                 ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
                             ) `q01`) `q02`
                             where `cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id."
                  GROUP BY `partido`) `q03`
            WHERE (NOT (((`mediana`) IS NULL)))
            ORDER BY `mediana`
            LIMIT 10;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function partidos_mayor_representacion_congreso_por_cuatrienio(Request $request){
        try{
            $result = DB::select("
            SELECT *
            FROM (SELECT `partido`, `cuatrienio`, COUNT(*) AS `n`
                  FROM (SELECT `id`,
                               `persona_id`,
                               `corporacion_id`,
                               `cuatrienio_id`,
                               `LHS`.`partido_id` AS `partido_id`,
                               `curul_id`,
                               `circunscripcion_id`,
                               `departamento_id_mayor_votacion`,
                               `urlHojaVida`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `cuatrienio`,
                               `partido`,
                               `color`
                        FROM (SELECT `id`,
                                     `persona_id`,
                                     `corporacion_id`,
                                     `CD`.`cuatrienio_id` AS `cuatrienio_id`,
                                     `partido_id`,
                                     `curul_id`,
                                     `circunscripcion_id`,
                                     `departamento_id_mayor_votacion`,
                                     `urlHojaVida`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `cuatrienio`
                              FROM `congresistas` AS `LHS`
                              JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
							  ON `LHS`.`id` = `CD`.`cid`
                                       LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                                  FROM `cuatrienios`) `RHS`
                                                 ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
                                            FROM `partidos`) `RHS`
                                           ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
                       ) `q01`
                  GROUP BY `partido`, `cuatrienio`) `q02`
            WHERE (`n` >= 10.0)
            ORDER BY `cuatrienio`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function numero_iniciativas_gubernamentales_sancionadas_como_ley(Request $request){
        try{
            $result = DB::select("
            SELECT `cuatrienio`, COUNT(*) AS `n`
            FROM (SELECT `id`,
                         `proyecto_ley_id`,
                         `fecha`,
                         `estado_proyecto_ley_id`,
                         `gaceta_texto`,
                         `gaceta_url`,
                         `nota`,
                         `corporacion_id`,
                         `observaciones`,
                         `orden`,
                         `activo`,
                         `usercreated`,
                         `usermodifed`,
                         `created_at`,
                         `updated_at`,
                         `estado`,
                         `LHS`.`cuatrienio_id` AS `cuatrienio_id`,
                         `iniciativa_id`,
                         `fecha_radicacion`,
                         `iniciativa`,
                         `cuatrienio`
                  FROM (SELECT `id`,
                               `proyecto_ley_id`,
                               `fecha`,
                               `estado_proyecto_ley_id`,
                               `gaceta_texto`,
                               `gaceta_url`,
                               `nota`,
                               `corporacion_id`,
                               `observaciones`,
                               `orden`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `estado`,
                               `cuatrienio_id`,
                               `LHS`.`iniciativa_id` AS `iniciativa_id`,
                               `fecha_radicacion`,
                               `iniciativa`
                        FROM (SELECT `LHS`.`id` AS `id`,
                                     `proyecto_ley_id`,
                                     `fecha`,
                                     `estado_proyecto_ley_id`,
                                     `gaceta_texto`,
                                     `gaceta_url`,
                                     `nota`,
                                     `corporacion_id`,
                                     `observaciones`,
                                     `orden`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `estado`,
                                     `cuatrienio_id`,
                                     `iniciativa_id`,
                                     `fecha_radicacion`
                              FROM (SELECT `id`,
                                           `proyecto_ley_id`,
                                           `fecha`,
                                           `LHS`.`estado_proyecto_ley_id` AS `estado_proyecto_ley_id`,
                                           `gaceta_texto`,
                                           `gaceta_url`,
                                           `nota`,
                                           `corporacion_id`,
                                           `observaciones`,
                                           `orden`,
                                           `activo`,
                                           `usercreated`,
                                           `usermodifed`,
                                           `created_at`,
                                           `updated_at`,
                                           `estado`
                                    FROM (SELECT *
                                          FROM `proyecto_ley_estados`
                                          WHERE (`estado_proyecto_ley_id` = '40')) `LHS`
                                             LEFT JOIN (SELECT `id` AS `estado_proyecto_ley_id`, `nombre` AS `estado`
                                                        FROM `estado_proyecto_leys`) `RHS`
                                                       ON (`LHS`.`estado_proyecto_ley_id` = `RHS`.`estado_proyecto_ley_id`)
                                   ) `LHS`
                                       INNER JOIN (SELECT `id`, `cuatrienio_id`, `iniciativa_id`, `fecha_radicacion`
                                                   FROM `proyecto_leys`) `RHS`
                                                  ON (`LHS`.`proyecto_ley_id` = `RHS`.`id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
                                            FROM `iniciativas`) `RHS`
                                           ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
                       ) `LHS`
                           LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`
                                      FROM `cuatrienios`) `RHS`
                                     ON (`LHS`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                 ) `q01`
            WHERE (`iniciativa` = 'Gubernamental')
            GROUP BY `cuatrienio`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function distribucion_edad_congreso_todos_cuatrienios(Request $request){
        try {
            $cuatrienios = Cuatrienio::where("activo", 1)->get();

            $result = [];

            foreach($cuatrienios as $cuatrienio){
                $data = Db::Select("
                SELECT *
                FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `cuatrienio`, `fechaInicio`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`, ROUND(DATEDIFF(`fechaInicio`, `fechaNacimiento`) / 365.0, 0) AS `edad`
                FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `cuatrienio`, DATE(CONCAT(`fechaInicio`, '-07-20')) AS `fechaInicio`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
                FROM (SELECT `id`, `LHS`.`persona_id` AS `persona_id`, `corporacion_id`, `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `cuatrienio`, `fechaInicio`, `partido`, `color`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
                FROM (SELECT `id`, `persona_id`, `corporacion_id`, `cuatrienio_id`, `LHS`.`partido_id` AS `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `cuatrienio`, `fechaInicio`, `partido`, `color`
                FROM (SELECT `id`, `persona_id`, `corporacion_id`, `CD`.`cuatrienio_id` AS `cuatrienio_id`, `partido_id`, `curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion`, `urlHojaVida`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `cuatrienio`, `fechaInicio`
                FROM `congresistas` AS `LHS`
                JOIN (SELECT `congresista_id` as `cid`, `corporacion_id`, `cuatrienio_id`, `partido_id`,`curul_id`, `circunscripcion_id`, `departamento_id_mayor_votacion` FROM `congresista_detalles`) `CD` 
			    ON `LHS`.`id` = `CD`.`cid`
                LEFT JOIN (SELECT `id` AS `cuatrienio_id`, `nombre` AS `cuatrienio`, `fechaInicio`
                FROM `cuatrienios`) `RHS`
                ON (`CD`.`cuatrienio_id` = `RHS`.`cuatrienio_id`)
                ) `LHS`
                LEFT JOIN (SELECT `id` AS `partido_id`, `nombre` AS `partido`, `color`
                FROM `partidos`) `RHS`
                ON (`LHS`.`partido_id` = `RHS`.`partido_id`)
                ) `LHS`
                LEFT JOIN (SELECT `id` AS `persona_id`, `nombres`, `apellidos`, `genero_id`, `fechaNacimiento`
                FROM `personas`) `RHS`
                ON (`LHS`.`persona_id` = `RHS`.`persona_id`)
                ) `q01`
                WHERE ((`cuatrienio_id` = ".$cuatrienio->id.") AND (NOT(((`fechaNacimiento`) IS NULL))))) `q02`) `q03`
                WHERE (`edad` >= 25.0)
                ");
                $data_collection = collect($data);
                $result[] = [
                    'cuatrienio' => $cuatrienio->nombre,
                    'data' => $data_collection->pluck('edad')
                ];
            }
            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    private function get_cuartiles_metodo_turkey($array){
        $longitud = count($array);

        if($longitud == 2){
            $cuartil_uno = $array[0];
            $cuartil_dos= ($array[0] + $array[1]) / 2;
            $cuartil_tres= $array[1];
        }
        else{
            if($longitud%2 == 0){
                $mitad = $longitud/2;
                $cuartil_dos = ($array[$mitad-1] + $array[$mitad]) / 2;
                $index_grupo_uno = ($mitad / 2 );
                $index_grupo_dos = ($mitad * 1.5);
                $cuartil_uno = ($array[$index_grupo_uno-1] + $array[$index_grupo_uno]) / 2;
                $cuartil_tres = ($array[$index_grupo_dos-1] + $array[$index_grupo_dos]) / 2;
            }
            else{
                $mitad = ($longitud - 1 )/2;
                $cuartil_dos = $array[$mitad];

                if(($mitad + 1) % 2 == 0){
                    $index_grupo_uno = (($mitad+1) / 2 );
                    $index_grupo_dos = (($mitad+1) * 1.5);
                    $cuartil_uno = ($array[$index_grupo_uno-1] + $array[$index_grupo_uno ]) / 2;
                    $cuartil_tres = ($array[$index_grupo_dos-2] + $array[$index_grupo_dos - 1]) / 2;
                }
                else{
                    $index_grupo_uno = (($mitad) / 2 );
                    $index_grupo_dos = (($mitad) * 1.5);
                    $cuartil_uno = $array[$index_grupo_uno];
                    $cuartil_tres = $array[$index_grupo_dos];
                }
            }
        }

        return [
            "cuartil_uno" => $cuartil_uno,
            "cuartil_dos" => $cuartil_dos,
            "cuartil_tres" => $cuartil_tres
        ];
    }

    public function top_10_temas_proyectos_ley(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT `tema`, COUNT(*) AS `n`
            FROM (SELECT *
            FROM (SELECT *
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`, `corporacion`, `tema`, `tipo_proyecto`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`, `corporacion`, `tema`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`, `corporacion`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `fechaInicio`
            FROM `proyecto_leys` AS `LHS`
            LEFT JOIN (SELECT `id`, `fechaInicio`
            FROM `legislaturas`) `RHS`
            ON (`LHS`.`legislatura_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`LHS`.`corporacion_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `tipo_proyecto`
            FROM `tipo_proyectos`) `RHS`
            ON (`LHS`.`tipo_proyecto_id` = `RHS`.`id`)
            ) `q01` WHERE (`tipo_proyecto` = 'Proyecto de Ley')) `q02`
            WHERE (`corporacion_id` ".$operador_corporacion."  ".$corporacion_id.")) `q03`
            WHERE (`cuatrienio_id` = ".$cuatrienio_id.")
            AND (`legislatura_id` ".$operador_legislatura."  ".$legislatura_id.")
            GROUP BY `tema`
            ORDER BY `n` DESC
            LIMIT 10

            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    public function estado_proyectos_ley(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT `estado_proyecto`, COUNT(*) AS `n`
            FROM (SELECT *
            FROM (SELECT *
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `estado_proyecto_ley_id`, `estado_proyecto`
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`, `estado_proyecto_ley_id`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`, `tipo_proyecto`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`, `corporacion`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`, `fechaInicio`
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `tipo_proyecto_id`
            FROM `proyecto_leys`) `LHS`
            LEFT JOIN (SELECT `id`, `fechaInicio`
            FROM `legislaturas`) `RHS`
            ON (`LHS`.`legislatura_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `corporacion`
            FROM `corporacions`) `RHS`
            ON (`LHS`.`corporacion_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `tipo_proyecto`
            FROM `tipo_proyectos`) `RHS`
            ON (`LHS`.`tipo_proyecto_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `proyecto_ley_id`, `estado_proyecto_ley_id`
            FROM `proyecto_ley_estados`) `RHS`
            ON (`LHS`.`id` = `RHS`.`proyecto_ley_id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre` AS `estado_proyecto`
            FROM `estado_proyecto_leys`) `RHS`
            ON (`LHS`.`estado_proyecto_ley_id` = `RHS`.`id`)
            ) `q01` WHERE (`tipo_proyecto` = 'Proyecto de Ley')) `q02`
            WHERE (`corporacion_id` ".$operador_corporacion."  ".$corporacion_id.")) `q03`
            WHERE (`cuatrienio_id` = ".$cuatrienio_id.")
            AND (`legislatura_id` ".$operador_legislatura."  ".$legislatura_id.")
            GROUP BY `estado_proyecto`
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    public function value_box_proyecto_de_ley(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT `mes`, COUNT(*) AS `n`
            FROM (SELECT DISTINCT `id`, CAST(CONCAT_WS('-', YEAR(`fecha_estado`), MONTH(`fecha_estado`), '01') AS DATE) AS `mes`
                  FROM (SELECT `id`, `comision_corporacion_id`, MAX(`fecha_estado`) AS `fecha_estado`
                        FROM (SELECT `id`,
                                     `cuatrienio_id`,
                                     `legislatura_id`,
                                     `corporacion_id`,
                                     `titulo`,
                                     `alias`,
                                     `fecha_radicacion`,
                                     `numero_camara`,
                                     `numero_senado`,
                                     `iniciativa_id`,
                                     `tipo_proyecto_id`,
                                     `tema_id_principal`,
                                     `tema_id_secundario`,
                                     `sinopsis`,
                                     `se_acumula_a_id`,
                                     `alcance_id`,
                                     `iniciativa_popular`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `proyecto_ley_estado_id`,
                                     `estado_proyecto_ley_id`,
                                     `fecha_estado`,
                                     `plcomision_id`,
                                     `LHS`.`comision_id` AS `comision_id`,
                                     `comision_corporacion_id`,
                                     `tipo_comision_id`,
                                     `descripcion`
                              FROM (SELECT `id`,
                                           `cuatrienio_id`,
                                           `legislatura_id`,
                                           `corporacion_id`,
                                           `titulo`,
                                           `alias`,
                                           `fecha_radicacion`,
                                           `numero_camara`,
                                           `numero_senado`,
                                           `iniciativa_id`,
                                           `tipo_proyecto_id`,
                                           `tema_id_principal`,
                                           `tema_id_secundario`,
                                           `sinopsis`,
                                           `se_acumula_a_id`,
                                           `alcance_id`,
                                           `iniciativa_popular`,
                                           `activo`,
                                           `usercreated`,
                                           `usermodifed`,
                                           `created_at`,
                                           `updated_at`,
                                           `LHS`.`proyecto_ley_estado_id` AS `proyecto_ley_estado_id`,
                                           `estado_proyecto_ley_id`,
                                           `fecha_estado`,
                                           `plcomision_id`,
                                           `comision_id`
                                    FROM (SELECT `LHS`.`id` AS `id`,
                                                 `cuatrienio_id`,
                                                 `legislatura_id`,
                                                 `corporacion_id`,
                                                 `titulo`,
                                                 `alias`,
                                                 `fecha_radicacion`,
                                                 `numero_camara`,
                                                 `numero_senado`,
                                                 `iniciativa_id`,
                                                 `tipo_proyecto_id`,
                                                 `tema_id_principal`,
                                                 `tema_id_secundario`,
                                                 `sinopsis`,
                                                 `se_acumula_a_id`,
                                                 `alcance_id`,
                                                 `iniciativa_popular`,
                                                 `activo`,
                                                 `usercreated`,
                                                 `usermodifed`,
                                                 `created_at`,
                                                 `updated_at`,
                                                 `proyecto_ley_estado_id`,
                                                 `estado_proyecto_ley_id`,
                                                 `fecha_estado`
                                          FROM (SELECT *
                                                FROM `proyecto_leys`
                                                WHERE ((`corporacion_id` ".$operador_corporacion." ".$corporacion_id.") AND (`cuatrienio_id` = ".$cuatrienio_id.") AND
                                                       (`legislatura_id` ".$operador_legislatura." ".$legislatura_id."))) `LHS`
                                                   LEFT JOIN (SELECT `id`              AS `proyecto_ley_estado_id`,
                                                                     `proyecto_ley_id` AS `id`,
                                                                     `estado_proyecto_ley_id`,
                                                                     `fecha`           AS `fecha_estado`
                                                              FROM `proyecto_ley_estados`) `RHS`
                                                             ON (`LHS`.`id` = `RHS`.`id`)
                                         ) `LHS`
                                             LEFT JOIN (SELECT `id` AS `plcomision_id`, `comision_id`, `proyecto_ley_estado_id`
                                                        FROM `proyecto_ley_comisions`) `RHS`
                                                       ON (`LHS`.`proyecto_ley_estado_id` = `RHS`.`proyecto_ley_estado_id`)
                                   ) `LHS`
                                       LEFT JOIN (SELECT `id`             AS `comision_id`,
                                                         `corporacion_id` AS `comision_corporacion_id`,
                                                         `tipo_comision_id`,
                                                         `descripcion`
                                                  FROM `comisions`) `RHS`
                                                 ON (`LHS`.`comision_id` = `RHS`.`comision_id`)
                             ) `q01`
                        GROUP BY `id`, `comision_corporacion_id`) `q02`
                  WHERE ((NOT (((`comision_corporacion_id`) IS NULL))) AND (`comision_corporacion_id` = 2))) `q03`
            GROUP BY `mes`
            ORDER BY `mes`
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function value_box_audiencias_publicas_citadas(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT `mes`, COUNT(*) AS `n`
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `congresista_id`, `corporacion_id`, CAST(CONCAT_WS('-', YEAR(`fecha`), MONTH(`fecha`), '01') AS DATE) AS `mes`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`congresista_id` AS `congresista_id`, `corporacion_id`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `congresista_id`
            FROM (SELECT *
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`
            FROM `control_politicos`) `q01`
            WHERE (`tipo_control_politico_id` = 2.0)) `LHS`
            LEFT JOIN (SELECT DISTINCT `control_politico_id` AS `id`, `congresista_id`
            FROM `control_politico_citantes`) `RHS`
            ON (`LHS`.`id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `congresistas`.`id` AS `congresista_id`
            FROM `congresistas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`congresista_id`)
            JOIN (SELECT `congresista_id`, `corporacion_id` FROM `congresista_detalles`) `CD` 
            ON `RHS`.`congresista_id` = `CD`.`congresista_id`
            ) `q01`) `q02`) `q03`
            WHERE ((`corporacion_id` ".$operador_corporacion." ".$corporacion_id.") AND (`cuatrienio_id` = ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id."))
            GROUP BY `mes`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function value_box_debates_de_control_politico(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT `mes`, COUNT(*) AS `n`
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `congresista_id`, `corporacion_id`, CAST(CONCAT_WS('-', YEAR(`fecha`), MONTH(`fecha`), '01') AS DATE) AS `mes`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`congresista_id` AS `congresista_id`, `corporacion_id`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `congresista_id`
            FROM (SELECT `id`, `cuatrienio_id`, `legislatura_id`, `tipo_control_politico_id`, `comision_id`, `estado_control_politico_id`, `titulo`, `fecha`, `tema_id_principal`, `tema_id_secundario`, `plenaria`, `tags`, `detalles`, `numero_proposicion`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`
            FROM `control_politicos`) `LHS`
            LEFT JOIN (SELECT DISTINCT `control_politico_id` AS `id`, `congresista_id`
            FROM `control_politico_citantes`) `RHS`
            ON (`LHS`.`id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id` AS `congresista_id`
            FROM `congresistas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`congresista_id`)
            JOIN (SELECT `congresista_id`, `corporacion_id` FROM `congresista_detalles`) `CD`
            ON (`LHS`.`congresista_id` = `CD`.`congresista_id`)
            ) `q01`) `q02`) `q03`
            WHERE ((`corporacion_id` ".$operador_corporacion." ".$corporacion_id.") 
            AND (`cuatrienio_id` = ".$cuatrienio_id.") 
            AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id."))
            GROUP BY `mes`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function value_box_sentencias_emitidas(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
           SELECT `mes`, COUNT(*) AS `n`
            FROM (SELECT `id`,
                         `cuatrienio_id`,
                         `legislatura_id`,
                         `corporacion_id`,
                         `titulo`,
                         `alias`,
                         `fecha_radicacion`,
                         `numero_camara`,
                         `numero_senado`,
                         `iniciativa_id`,
                         `tipo_proyecto_id`,
                         `tema_id_principal`,
                         `tema_id_secundario`,
                         `sinopsis`,
                         `se_acumula_a_id`,
                         `alcance_id`,
                         `iniciativa_popular`,
                         `activo`,
                         `usercreated`,
                         `usermodifed`,
                         `created_at`,
                         `updated_at`,
                         `proyecto_ley_estado_id`,
                         `estado_proyecto_ley_id`,
                         `estado`,
                         CAST(CONCAT_WS('-', YEAR(`fecha_radicacion`), MONTH(`fecha_radicacion`), '01') AS DATE) AS `mes`
                  FROM (SELECT `id`,
                               `cuatrienio_id`,
                               `legislatura_id`,
                               `corporacion_id`,
                               `titulo`,
                               `alias`,
                               `fecha_radicacion`,
                               `numero_camara`,
                               `numero_senado`,
                               `iniciativa_id`,
                               `tipo_proyecto_id`,
                               `tema_id_principal`,
                               `tema_id_secundario`,
                               `sinopsis`,
                               `se_acumula_a_id`,
                               `alcance_id`,
                               `iniciativa_popular`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `proyecto_ley_estado_id`,
                               `LHS`.`estado_proyecto_ley_id` AS `estado_proyecto_ley_id`,
                               `estado`
                        FROM (SELECT `LHS`.`id` AS `id`,
                                     `cuatrienio_id`,
                                     `legislatura_id`,
                                     `corporacion_id`,
                                     `titulo`,
                                     `alias`,
                                     `fecha_radicacion`,
                                     `numero_camara`,
                                     `numero_senado`,
                                     `iniciativa_id`,
                                     `tipo_proyecto_id`,
                                     `tema_id_principal`,
                                     `tema_id_secundario`,
                                     `sinopsis`,
                                     `se_acumula_a_id`,
                                     `alcance_id`,
                                     `iniciativa_popular`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `proyecto_ley_estado_id`,
                                     `estado_proyecto_ley_id`
                              FROM (SELECT *
                                    FROM `proyecto_leys`
                                    WHERE ((`corporacion_id` ".$operador_corporacion." ".$corporacion_id.") AND (`cuatrienio_id` = ".$cuatrienio_id.") AND
                                           (`legislatura_id` ".$operador_legislatura." ".$legislatura_id."))) `LHS`
                                       LEFT JOIN (SELECT `id`              AS `proyecto_ley_estado_id`,
                                                         `proyecto_ley_id` AS `id`,
                                                         `estado_proyecto_ley_id`
                                                  FROM `proyecto_ley_estados`) `RHS`
                                                 ON (`LHS`.`id` = `RHS`.`id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `estado_proyecto_ley_id`, `nombre` AS `estado`
                                            FROM `estado_proyecto_leys`) `RHS`
                                           ON (`LHS`.`estado_proyecto_ley_id` = `RHS`.`estado_proyecto_ley_id`)
                       ) `q01`
                  WHERE (`estado` IN ('Declarado Inexequible Parcial', 'Declarado Inexequible Total', 'Declarado Exequible Total',
                                      'Declarado Exequible Parcial'))) `q02`
            GROUP BY `mes`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function value_box_objeciones_emitidas(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT `mes`, COUNT(*) AS `n`
            FROM (SELECT `id`,
                         `cuatrienio_id`,
                         `legislatura_id`,
                         `corporacion_id`,
                         `titulo`,
                         `alias`,
                         `fecha_radicacion`,
                         `numero_camara`,
                         `numero_senado`,
                         `iniciativa_id`,
                         `tipo_proyecto_id`,
                         `tema_id_principal`,
                         `tema_id_secundario`,
                         `sinopsis`,
                         `se_acumula_a_id`,
                         `alcance_id`,
                         `iniciativa_popular`,
                         `activo`,
                         `usercreated`,
                         `usermodifed`,
                         `created_at`,
                         `updated_at`,
                         `proyecto_ley_estado_id`,
                         `estado_proyecto_ley_id`,
                         `estado`,
                         CAST(CONCAT_WS('-', YEAR(`fecha_radicacion`), MONTH(`fecha_radicacion`), '01') AS DATE) AS `mes`
                  FROM (SELECT `id`,
                               `cuatrienio_id`,
                               `legislatura_id`,
                               `corporacion_id`,
                               `titulo`,
                               `alias`,
                               `fecha_radicacion`,
                               `numero_camara`,
                               `numero_senado`,
                               `iniciativa_id`,
                               `tipo_proyecto_id`,
                               `tema_id_principal`,
                               `tema_id_secundario`,
                               `sinopsis`,
                               `se_acumula_a_id`,
                               `alcance_id`,
                               `iniciativa_popular`,
                               `activo`,
                               `usercreated`,
                               `usermodifed`,
                               `created_at`,
                               `updated_at`,
                               `proyecto_ley_estado_id`,
                               `LHS`.`estado_proyecto_ley_id` AS `estado_proyecto_ley_id`,
                               `estado`
                        FROM (SELECT `LHS`.`id` AS `id`,
                                     `cuatrienio_id`,
                                     `legislatura_id`,
                                     `corporacion_id`,
                                     `titulo`,
                                     `alias`,
                                     `fecha_radicacion`,
                                     `numero_camara`,
                                     `numero_senado`,
                                     `iniciativa_id`,
                                     `tipo_proyecto_id`,
                                     `tema_id_principal`,
                                     `tema_id_secundario`,
                                     `sinopsis`,
                                     `se_acumula_a_id`,
                                     `alcance_id`,
                                     `iniciativa_popular`,
                                     `activo`,
                                     `usercreated`,
                                     `usermodifed`,
                                     `created_at`,
                                     `updated_at`,
                                     `proyecto_ley_estado_id`,
                                     `estado_proyecto_ley_id`
                              FROM (SELECT *
                                    FROM `proyecto_leys`
                                    WHERE ((`corporacion_id` ".$operador_corporacion." ".$corporacion_id.") AND (`cuatrienio_id` = ".$cuatrienio_id.") AND
                                           (`legislatura_id` ".$operador_legislatura." ".$legislatura_id."))) `LHS`
                                       LEFT JOIN (SELECT `id`              AS `proyecto_ley_estado_id`,
                                                         `proyecto_ley_id` AS `id`,
                                                         `estado_proyecto_ley_id`
                                                  FROM `proyecto_ley_estados`) `RHS`
                                                 ON (`LHS`.`id` = `RHS`.`id`)
                             ) `LHS`
                                 LEFT JOIN (SELECT `id` AS `estado_proyecto_ley_id`, `nombre` AS `estado`
                                            FROM `estado_proyecto_leys`) `RHS`
                                           ON (`LHS`.`estado_proyecto_ley_id` = `RHS`.`estado_proyecto_ley_id`)
                       ) `q01`
                  WHERE (`estado` IN
                         ('Objeción Total del Ejecutivo', 'Objeción Parcial del Ejecutivo', 'Objetado Por Presidencia'))) `q02`
            GROUP BY `mes`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function value_box_proyectos_radicados(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;

            $result = DB::select("
            SELECT `mes`, COUNT(*) AS `n`
            FROM (SELECT `id`,
                         `cuatrienio_id`,
                         `legislatura_id`,
                         `corporacion_id`,
                         `titulo`,
                         `alias`,
                         `fecha_radicacion`,
                         `numero_camara`,
                         `numero_senado`,
                         `iniciativa_id`,
                         `tipo_proyecto_id`,
                         `tema_id_principal`,
                         `tema_id_secundario`,
                         `sinopsis`,
                         `se_acumula_a_id`,
                         `alcance_id`,
                         `iniciativa_popular`,
                         `activo`,
                         `usercreated`,
                         `usermodifed`,
                         `created_at`,
                         `updated_at`,
                         CAST(CONCAT_WS('-', YEAR(`fecha_radicacion`), MONTH(`fecha_radicacion`), '01') AS DATE) AS `mes`
                  FROM `proyecto_leys`
                  WHERE ((`corporacion_id` ".$operador_corporacion." ".$corporacion_id.") AND (`cuatrienio_id` = ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id."))) `q01`
            GROUP BY `mes`
            ORDER BY `mes`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    // Votaciones

    public function votaciones_por_tema_camara(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `tema`, `tipo`, `votos_num`, `votos_totales`, ROUND(`votos_num` * 100.0 / `votos_totales`, 1) AS `votos`
            FROM (SELECT `LHS`.`tema` AS `tema`, `tipo`, `votos_num`, `votos_totales`
            FROM (SELECT `tema`, CASE
            WHEN (`tipo` = 'votosAbstencion') THEN ('Votos-Abstención')
            WHEN (`tipo` = 'votosContra') THEN ('En contra')
            WHEN (`tipo` = 'votosFavor') THEN ('A favor')
            END AS `tipo`, `votos_num`
            FROM (SELECT `tema`, `tipo`, SUM(`value`) AS `votos_num`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))) `q02`
            GROUP BY `tema`, `tipo`) `q03`) `LHS`
            LEFT JOIN (SELECT `tema`, SUM(`value`) AS `votos_totales`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))) `q02`
            GROUP BY `tema`) `RHS`
            ON (`LHS`.`tema` = `RHS`.`tema`)
            ) `q03`) `q04`
            WHERE (NOT(((`tema`) IS NULL)));
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function votaciones_por_tema_senado(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `tema`, `tipo`, `votos_num`, `votos_totales`, ROUND(`votos_num` * 100.0 / `votos_totales`, 1) AS `votos`
            FROM (SELECT `LHS`.`tema` AS `tema`, `tipo`, `votos_num`, `votos_totales`
            FROM (SELECT `tema`, CASE
            WHEN (`tipo` = 'votosAbstencion') THEN ('Votos-Abstención')
            WHEN (`tipo` = 'votosContra') THEN ('En contra')
            WHEN (`tipo` = 'votosFavor') THEN ('A favor')
            END AS `tipo`, `votos_num`
            FROM (SELECT `tema`, `tipo`, SUM(`value`) AS `votos_num`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))) `q02`
            GROUP BY `tema`, `tipo`) `q03`) `LHS`
            LEFT JOIN (SELECT `tema`, SUM(`value`) AS `votos_totales`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`, `tema`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`tema_id_principal` AS `tema_id_principal`, `corporacion_id`, `tema`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `tema_id_principal`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `tema_id_principal`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))) `q02`
            GROUP BY `tema`) `RHS`
            ON (`LHS`.`tema` = `RHS`.`tema`)
            ) `q03`) `q04`
            WHERE (NOT(((`tema`) IS NULL)));
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    public function votaciones_por_iniciativa_camara(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `iniciativa`, `tipo`, `votos_num`, `votos_totales`, ROUND(`votos_num` * 100.0 / `votos_totales`, 0) AS `votos`, 1.0 AS `1`
            FROM (SELECT `LHS`.`iniciativa` AS `iniciativa`, `tipo`, `votos_num`, `votos_totales`
            FROM (SELECT `iniciativa`, CASE
            WHEN (`tipo` = 'votosAbstencion') THEN ('Votos-Abstención')
            WHEN (`tipo` = 'votosContra') THEN ('En contra')
            WHEN (`tipo` = 'votosFavor') THEN ('A favor')
            END AS `tipo`, `votos_num`
            FROM (SELECT `iniciativa`, `tipo`, SUM(`value`) AS `votos_num`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))) `q02`
            GROUP BY `iniciativa`, `tipo`) `q03`) `LHS`
            LEFT JOIN (SELECT `iniciativa`, SUM(`value`) AS `votos_totales`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1)))) `q02`
            GROUP BY `iniciativa`) `RHS`
            ON (`LHS`.`iniciativa` = `RHS`.`iniciativa`)
            ) `q03`) `q04`
            WHERE (NOT(((`iniciativa`) IS NULL)));
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function votaciones_por_iniciativa_senado(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `iniciativa`, `tipo`, `votos_num`, `votos_totales`, ROUND(`votos_num` * 100.0 / `votos_totales`, 0) AS `votos`, 1.0 AS `1`
            FROM (SELECT `LHS`.`iniciativa` AS `iniciativa`, `tipo`, `votos_num`, `votos_totales`
            FROM (SELECT `iniciativa`, CASE
            WHEN (`tipo` = 'votosAbstencion') THEN ('Votos-Abstención')
            WHEN (`tipo` = 'votosContra') THEN ('En contra')
            WHEN (`tipo` = 'votosFavor') THEN ('A favor')
            END AS `tipo`, `votos_num`
            FROM (SELECT `iniciativa`, `tipo`, SUM(`value`) AS `votos_num`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))) `q02`
            GROUP BY `iniciativa`, `tipo`) `q03`) `LHS`
            LEFT JOIN (SELECT `iniciativa`, SUM(`value`) AS `votos_totales`
            FROM (((SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosFavor' AS `tipo`, `votosFavor` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosContra' AS `tipo`, `votosContra` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2))))
            UNION ALL
            (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `acta`, `observaciones`, `aprobada`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `iniciativa`, 'votosAbstencion' AS `tipo`, `votosAbstencion` AS `value`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `LHS`.`iniciativa_id` AS `iniciativa_id`, `corporacion_id`, `iniciativa`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `iniciativa_id`, `nombre` AS `iniciativa`
            FROM `iniciativas`) `RHS`
            ON (`LHS`.`iniciativa_id` = `RHS`.`iniciativa_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2)))) `q02`
            GROUP BY `iniciativa`) `RHS`
            ON (`LHS`.`iniciativa` = `RHS`.`iniciativa`)
            ) `q03`) `q04`
            WHERE (NOT(((`iniciativa`) IS NULL)));
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    public function tipo_votaciones_camara(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `tipo`, ROUND(`n` * 100.0 / `total`, 0) AS `n`, `variable`, `total`, `n` AS `n2`
            FROM (SELECT `tipo`, `n`, `LHS`.`variable` AS `variable`, `total`
            FROM (SELECT `tipo`, `n`, 'total' AS `variable`
            FROM (SELECT `tipo`, COUNT(*) AS `n`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `LHS`.`tipo_votacion_id` AS `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `tipo`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tipo_votacion_id`, `nombre` AS `tipo`
            FROM `tipo_votacions`) `RHS`
            ON (`LHS`.`tipo_votacion_id` = `RHS`.`tipo_votacion_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1))
            GROUP BY `tipo`) `q02`) `LHS`
            LEFT JOIN (SELECT `total`, 'total' AS `variable`
            FROM (SELECT SUM(`n`) AS `total`
            FROM (SELECT `tipo`, COUNT(*) AS `n`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `LHS`.`tipo_votacion_id` AS `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `tipo`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tipo_votacion_id`, `nombre` AS `tipo`
            FROM `tipo_votacions`) `RHS`
            ON (`LHS`.`tipo_votacion_id` = `RHS`.`tipo_votacion_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 1))
            GROUP BY `tipo`) `q02`) `q03`) `RHS`
            ON (`LHS`.`variable` = `RHS`.`variable`)
            ) `q04`) `q05`
            WHERE (`n` > 0.0)
            ORDER BY `n` DESC;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
    public function tipo_votaciones_senado(Request $request){
        try{
            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');

            $operador_cuatrienio = $cuatrienio_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $cuatrienio_id = $cuatrienio_id == null ? 0 : $cuatrienio_id;

            $result = DB::select("
            SELECT *
            FROM (SELECT `tipo`, ROUND(`n` * 100.0 / `total`, 0) AS `n`, `variable`, `total`, `n` AS `n2`
            FROM (SELECT `tipo`, `n`, `LHS`.`variable` AS `variable`, `total`
            FROM (SELECT `tipo`, `n`, 'total' AS `variable`
            FROM (SELECT `tipo`, COUNT(*) AS `n`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `LHS`.`tipo_votacion_id` AS `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `tipo`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tipo_votacion_id`, `nombre` AS `tipo`
            FROM `tipo_votacions`) `RHS`
            ON (`LHS`.`tipo_votacion_id` = `RHS`.`tipo_votacion_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2))
            GROUP BY `tipo`) `q02`) `LHS`
            LEFT JOIN (SELECT `total`, 'total' AS `variable`
            FROM (SELECT SUM(`n`) AS `total`
            FROM (SELECT `tipo`, COUNT(*) AS `n`
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `LHS`.`tipo_votacion_id` AS `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`, `tipo`
            FROM (SELECT DISTINCT *
            FROM (SELECT `id`, `fecha`, `legislatura_id`, `cuatrienio_id`, `LHS`.`proyecto_de_ley_id` AS `proyecto_de_ley_id`, `esPlenaria`, `esComision`, `urlGaceta`, `motivo`, `tipo_votacion_id`, `votosFavor`, `votosContra`, `acta`, `observaciones`, `aprobada`, `votosAbstencion`, `numero_no_asistencias`, `clase_votacion_id`, `numero_asistencias`, `voto_general`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `iniciativa_id`, `corporacion_id`
            FROM `votacions` AS `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_de_ley_id`, `iniciativa_id`, `corporacion_id`
            FROM `proyecto_leys`) `RHS`
            ON (`LHS`.`proyecto_de_ley_id` = `RHS`.`proyecto_de_ley_id`)
            ) `q01`) `LHS`
            LEFT JOIN (SELECT `id` AS `tipo_votacion_id`, `nombre` AS `tipo`
            FROM `tipo_votacions`) `RHS`
            ON (`LHS`.`tipo_votacion_id` = `RHS`.`tipo_votacion_id`)
            ) `q01`
            WHERE ((`cuatrienio_id` ".$operador_cuatrienio." ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` = 2))
            GROUP BY `tipo`) `q02`) `q03`) `RHS`
            ON (`LHS`.`variable` = `RHS`.`variable`)
            ) `q04`) `q05`
            WHERE (`n` > 0.0)
            ORDER BY `n` DESC;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    // End votaciones

    public function datos_inicio(Request $request){
        try{
            
            $result1 = DB::select("
            SELECT COUNT(*) AS `n`
            FROM (SELECT `id`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `legislatura_id`, `corporacion_id`, `titulo`, `alias`, `fecha_radicacion`, `numero_camara`, `numero_senado`, `iniciativa_id`, `tipo_proyecto_id`, `tema_id_principal`, `tema_id_secundario`, `sinopsis`, `se_acumula_a_id`, `alcance_id`, `iniciativa_popular`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `proyecto_ley_estado_id`, `estado_proyecto_ley_id`, `fecha_estado`
            FROM (SELECT *
            FROM `proyecto_leys`
            WHERE (`cuatrienio_id` = 8.0)) `LHS`
            LEFT JOIN (SELECT `id` AS `proyecto_ley_estado_id`, `proyecto_ley_id` AS `id`, `estado_proyecto_ley_id`, `fecha` AS `fecha_estado`
            FROM `proyecto_ley_estados`) `RHS`
            ON (`LHS`.`id` = `RHS`.`id`)
            ) `q01`
            WHERE (`estado_proyecto_ley_id` = 40.0)
            GROUP BY `id`) `q02`;
            ");

            $result2 = DB::select("
            SELECT COUNT(*) AS `n`
            FROM (SELECT `LHS`.`tema_id_principal` AS `tema_id_principal`, `n`, `tema`
            FROM (SELECT `tema_id_principal`, COUNT(*) AS `n`
            FROM `proyecto_leys`
            WHERE (`cuatrienio_id` = 8.0)
            GROUP BY `tema_id_principal`) `LHS`
            LEFT JOIN (SELECT `id` AS `tema_id_principal`, `nombre` AS `tema`
            FROM `temas`) `RHS`
            ON (`LHS`.`tema_id_principal` = `RHS`.`tema_id_principal`)
            ) `q01`
            WHERE (NOT(((`tema`) IS NULL)));
            ");

            $result3 = DB::select("
            SELECT COUNT(*) AS `n`
            FROM (SELECT `id`
            FROM `control_politicos`
            WHERE (`cuatrienio_id` = 8.0)
            GROUP BY `id`) `q01`;
            ");

            $result4 = DB::select("
            SELECT COUNT(*) AS `n`
            FROM (SELECT `id`
            FROM (SELECT DISTINCT *
            FROM (SELECT * FROM (SELECT *
            FROM `proyecto_leys`
            WHERE (`cuatrienio_id` = 8.0)) `LHS`
            WHERE NOT EXISTS (
              SELECT 1 FROM (SELECT *
            FROM (SELECT `id`, GROUP_CONCAT(`estado_proyecto_ley_id` SEPARATOR ', ') AS `estado_proyecto_ley_id`
            FROM (SELECT `id` AS `proyecto_ley_estado_id`, `proyecto_ley_id` AS `id`, `estado_proyecto_ley_id`, `fecha` AS `fecha_estado`
            FROM `proyecto_ley_estados`) `q01`
            GROUP BY `id`) `q02`
            WHERE (`estado_proyecto_ley_id` REGEXP '40')) `RHS`
              WHERE (`LHS`.`id` = `RHS`.`id`)
            )) `q03`) `q04`
            GROUP BY `id`) `q05`;
            ");

            $result = [$result1[0], $result2[0], $result3[0], $result4[0]];

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }

    public function congresistas_mas_citaciones(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;
            
            $result = DB::select("
            SELECT `nombre`, COUNT(*) AS `n`
            FROM (SELECT `id`, `control_politico_id`, `congresista_id`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura_id`, `cuatrienio_id`, `persona_id`, `corporacion_id`, `nombres`, `apellidos`, CONCAT_WS(' ', `nombres`, `apellidos`) AS `nombre`
            FROM (SELECT `LHS`.`id` AS `id`, `control_politico_id`, `congresista_id`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura_id`, `cuatrienio_id`, `persona_id`, `corporacion_id`, `nombres`, `apellidos`
            FROM (SELECT `LHS`.`id` AS `id`, `control_politico_id`, `congresista_id`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura_id`, `cuatrienio_id`, `persona_id`, `corporacion_id`
            FROM (SELECT `LHS`.`id` AS `id`, `control_politico_id`, `congresista_id`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura_id`, `cuatrienio_id`
            FROM `control_politico_citantes` AS `LHS`
            LEFT JOIN (SELECT `id`, `legislatura_id`, `cuatrienio_id`
            FROM `control_politicos`) `RHS`
            ON (`LHS`.`control_politico_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `persona_id`
            FROM `congresistas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`id`)
            JOIN (SELECT `congresista_id` AS `cid`, `corporacion_id` FROM `congresista_detalles`) `CD`
            ON (`RHS`.`id` = `CD`.`cid`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombres`, `apellidos`
            FROM `personas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`id`)
            ) `q01`
            WHERE ((NOT(((`nombres`) IS NULL))) AND (`cuatrienio_id` = ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` ".$operador_corporacion." ".$corporacion_id."))) `q02`
            GROUP BY `nombre`
            ORDER BY `n` DESC
            LIMIT 5;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function partidos_mas_citaciones(Request $request){
        try{

            $cuatrienio_id = $request->input('cuatrienio_id');
            $legislatura_id = $request->input('legislatura_id');
            $corporacion_id = $request->input('corporacion_id');

            $operador_corporacion = $corporacion_id == null ? "!=" : "=";
            $operador_legislatura = $legislatura_id == null ? "!=" : "=";

            $legislatura_id = $legislatura_id == null ? 0 : $legislatura_id;
            $corporacion_id = $corporacion_id == null ? 0 : $corporacion_id;
            
            $result = DB::select("
            SELECT `nombre`, `color`, COUNT(*) AS `n`
            FROM (SELECT `LHS`.`id` AS `id`, `control_politico_id`, `congresista_id`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura_id`, `cuatrienio_id`, `persona_id`, `corporacion_id`, `partido_id`, `nombre`, `color`
            FROM (SELECT `LHS`.`id` AS `id`, `control_politico_id`, `congresista_id`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura_id`, `cuatrienio_id`, `persona_id`, `corporacion_id`, `partido_id`
            FROM (SELECT `LHS`.`id` AS `id`, `control_politico_id`, `congresista_id`, `activo`, `usercreated`, `usermodifed`, `created_at`, `updated_at`, `legislatura_id`, `cuatrienio_id`
            FROM `control_politico_citantes` AS `LHS`
            LEFT JOIN (SELECT `id`, `legislatura_id`, `cuatrienio_id`
            FROM `control_politicos`) `RHS`
            ON (`LHS`.`control_politico_id` = `RHS`.`id`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `persona_id`
            FROM `congresistas`) `RHS`
            ON (`LHS`.`congresista_id` = `RHS`.`id`)
            JOIN (SELECT `congresista_id` AS `cid`, `corporacion_id`, `partido_id` FROM `congresista_detalles`) `CD`
            ON (`RHS`.`id` = `CD`.`cid`)
            ) `LHS`
            LEFT JOIN (SELECT `id`, `nombre`, `color`
            FROM `partidos`) `RHS`
            ON (`LHS`.`partido_id` = `RHS`.`id`)
            ) `q01`
            WHERE ((`cuatrienio_id` = ".$cuatrienio_id.") AND (`legislatura_id` ".$operador_legislatura." ".$legislatura_id.") AND (`corporacion_id` ".$operador_corporacion." ".$corporacion_id."))
            GROUP BY `nombre`, `color`
            ORDER BY `n` DESC
            LIMIT 5;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => 'Error'], 422);
        }
    }
    public function total_citaciones_por_cuatrienio(Request $request){
        try{
            
            $result = DB::select("
            SELECT `nombre`, COUNT(*) AS `n`
            FROM (SELECT `LHS`.`id` AS `id`, `cuatrienio_id`, `nombre`
            FROM (SELECT `id`, `cuatrienio_id`
            FROM `control_politicos`) `LHS`
            LEFT JOIN (SELECT `id`, `nombre`
            FROM `cuatrienios`) `RHS`
            ON (`LHS`.`cuatrienio_id` = `RHS`.`id`)
            ) `q01`
            GROUP BY `nombre`
            ORDER BY `nombre`;
            ");

            return response($result);
        }
        catch(QueryException $e){
            return response()->json(['message' => $e], 422);
        }
    }
}
