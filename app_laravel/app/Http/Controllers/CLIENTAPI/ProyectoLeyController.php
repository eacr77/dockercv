<?php

namespace App\Http\Controllers\CLIENTAPI;

use App\Http\Controllers\Controller;
use App\Models\ProyectoLey;
use App\Models\ProyectoLeyAutor;
use App\Models\ProyectoLeyPonente;
use App\Models\VotacionCongresista;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProyectoLeyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $query = ProyectoLey::query()->where(
            'activo', 1
        );
        if ($request->input('corporacion') != "-1") {
            $query->where(
                'corporacion_id',
                $request->corporacion
            );
        }
        if ($request->input('cuatrienio') != "-1") {
            $query->where(
                'cuatrienio_id',
                $request->cuatrienio
            );
        }
        if ($request->input('legislatura') != "-1") {
            $query->where(
                'legislatura_id', $request->legislatura
            );
        }
        if ($request->input('iniciativa') != "-1") {
            $query->where(
                'iniciativa_id', $request->iniciativa
            );
        }
        if ($request->input('tema') != "-1") {
            $tema_id = $request->input('tema');
            $query->where(
                function ($query) use (
                    $tema_id
                ) {
                    $query->where(
                        'tema_id_principal', $tema_id
                    )->orWhere(
                        'tema_id_secundario', $tema_id
                    );
                }
            );
        }
        if ($request->input('tipo') != "-1") {
            $query->where(
                'tipo_proyecto_id', $request->tipo
            );
        }
        if ($request->input('estado') != "-1") {
            $estado_id = $request->input('estado');
            $query->Where(
                function ($query) use (
                    $estado_id
                ) {
                    $query->WhereHas(
                        'ProyectoLeyEstado', function ($query) use (
                            $estado_id
                        ) {
                            $query->where(
                                'estado_proyecto_ley_id', $estado_id
                            );
                        }
                    );
                }
            );
        }
        if ($request->input('search') != "") {
            $search = $request->input('search');
            $search = str_replace(' ', '%', $search);
            $query->Where(
                function ($query) use (
                    $search
                ) {
                    $query->Where('numero_camara', 'like', '%' . $search . '%')
                        ->orWhere('numero_senado', 'like', '%' . $search . '%')
                        ->orWhere('titulo', 'like', '%' . $search . '%')
                        ->orWhere(function ($query) use ($search) {
                            $query->orWhereHas('ProyectoLeyAutorLegislativos', function ($query) use ($search) {
                                $query->WhereHas('Congresista', function ($query) use ($search) {
                                    $query->WhereHas('persona', function ($query) use ($search) {
                                        $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                            ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                    });
                                });
                            });
                        }
                        )->orWhere(function ($query) use ($search) {
                        $query->orWhereHas('ProyectoLeyAutorPersonas', function ($query) use ($search) {
                            $query->WhereHas(
                                'persona', function ($query) use ($search) {
                                    $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                });
                        });
                    });
                }
            );
        }
        $items = $query->with(
            [
                'Legislatura',
                'Cuatrienio',
                'TipoProyectoLey',
                'Iniciativa',
                'ProyectoLeyAutorLegislativos',
                'ProyectoLeyAutorPersonas',
            ]
        )->skip(($request->input('page') - 1) * $request->input('rows'))
            ->take($request->input('rows'))
            ->orderBy('cuatrienio_id', 'desc')
            ->get()->toJson(JSON_PRETTY_PRINT);

        return response($items);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = ProyectoLey::where('id', $id)
            ->with(['Legislatura',
                'Cuatrienio',
                'TipoProyectoLey',
                'Iniciativa',
                'TemaPrincipal',
                'TemaSecundario',
                'ProyectoLeyDetalleVotacion',
                'ProyectoLeyAutorLegislativosCliente',
                'ProyectoLeyAutorPersonas',
                'ProyectoLeyEstado'])
            ->get()
            ->first()
            ->toJson(JSON_PRETTY_PRINT);

        return response($item);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function totalrecords(Request $request)
    {
        $query = ProyectoLey::query()->where(
            'activo', 1
        );
        if ($request->input('corporacion') != "-1") {
            $query->where(
                'corporacion_id',
                $request->corporacion
            );
        }
        if ($request->input('cuatrienio') != "-1") {
            $query->where(
                'cuatrienio_id',
                $request->cuatrienio
            );
        }
        if ($request->input('legislatura') != "-1") {
            $query->where(
                'legislatura_id', $request->legislatura
            );
        }
        if ($request->input('iniciativa') != "-1") {
            $query->where(
                'iniciativa_id', $request->iniciativa
            );
        }
        if ($request->input('tema') != "-1") {
            $tema_id = $request->input('tema');
            $query->where(
                function ($query) use (
                    $tema_id
                ) {
                    $query->where(
                        'tema_id_principal', $tema_id
                    )->orWhere(
                        'tema_id_secundario', $tema_id
                    );
                }
            );
        }
        if ($request->input('tipo') != "-1") {
            $query->where(
                'tipo_proyecto_id', $request->tipo
            );
        }
        if ($request->input('estado') != "-1") {
            $estado_id = $request->input('estado');
            $query->Where(
                function ($query) use (
                    $estado_id
                ) {
                    $query->WhereHas(
                        'ProyectoLeyEstado', function ($query) use (
                        $estado_id
                    ) {
                        $query->where(
                            'estado_proyecto_ley_id', $estado_id
                        );
                    }
                    );
                }
            );
        }
        if ($request->input('search') != "") {
            $search = $request->input('search');
            $search = str_replace(' ', '%', $search);
            $query->Where(
                function ($query) use (
                    $search
                ) {
                    $query->Where('numero_camara', 'like', '%' . $search . '%')
                        ->orWhere('numero_senado', 'like', '%' . $search . '%')
                        ->orWhere('titulo', 'like', '%' . $search . '%')
                        ->orWhere(function ($query) use ($search) {
                            $query->orWhereHas('ProyectoLeyAutorLegislativos', function ($query) use ($search) {
                                $query->WhereHas('Congresista', function ($query) use ($search) {
                                    $query->WhereHas('persona', function ($query) use ($search) {
                                        $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                            ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                    });
                                });
                            });
                        }
                        )->orWhere(function ($query) use ($search) {
                            $query->orWhereHas('ProyectoLeyAutorPersonas', function ($query) use ($search) {
                                $query->WhereHas(
                                    'persona', function ($query) use ($search) {
                                    $query->where(DB::raw("CONCAT(`nombres`, ' ', COALESCE(`apellidos`,''))"), 'LIKE', "%" . $search . "%")
                                        ->orWhere(DB::raw("CONCAT(COALESCE(`apellidos`,''), ' ', 'nombres')"), 'LIKE', "%" . $search . "%");
                                });
                            });
                        });
                }
            );
        }
        $items = $query->with(
            [
                'Legislatura',
                'Cuatrienio',
                'TipoProyectoLey',
                'Iniciativa',
                'ProyectoLeyAutorLegislativos',
                'ProyectoLeyAutorPersonas',
            ]
        )->count();

        return response($items);
    }

    public function getAutoresFilter(Request $request)
    {
        $nombre = $request->input('nombre');
        $proyecto = $request->input('proyecto');
        $partido = $request->input('partido');
        $items = ProyectoLeyAutor::select('proyecto_ley_autors.congresista_id')
            ->join('congresistas', 'proyecto_ley_autors.congresista_id', 'congresistas.id')
            ->where('congresistas.activo', '1')
            ->where('proyecto_ley_autors.proyecto_ley_id', $proyecto)
            ->where('congresistas.partido_id', ($partido != "0") ? '=' : '!=', $partido)
            ->where('congresistas.nombre', 'LIKE', '%' . $nombre . '%')
            ->with('congresista')
            ->get()
            ->toJson(JSON_PRETTY_PRINT);

        return response($items, 200);
    }

    public function getPonentesFilter(Request $request)
    {
        $tipo = $request->input('tipo');
        $proyecto = $request->input('proyecto');
        $partido = $request->input('partido');
        $items = ProyectoLeyPonente::select('proyecto_ley_ponentes.congresista_id')
            ->join('congresistas', 'proyecto_ley_ponentes.congresista_id', 'congresistas.id')
            ->where('congresistas.activo', '1')
            ->where('proyecto_ley_ponentes.proyecto_ley_id', $proyecto)
            ->where('congresistas.partido_id', ($partido != "0") ? '=' : '!=', $partido)
            ->where('proyecto_ley_ponentes.tipo_publicacion_proyecto_ley_id', ($tipo != "0") ? '=' : '!=', $tipo)
            ->with('congresista')
            ->get()
            ->toJson(JSON_PRETTY_PRINT);

        return response($items, 200);
    }

    public function getCountVotos(Request $request)
    {
        $votacion = $request->input('votacion');

        $countVotos = VotacionCongresista::select(DB::raw('tipo_respuesta_votacion_id, count(*) as conteo'))
            ->where('votacion_id', ($votacion != "0") ? '=' : '!=', $votacion)
            ->groupBy('tipo_respuesta_votacion_id')
            ->get()
            ->toJson(JSON_PRETTY_PRINT);

        return response($countVotos, 200);
    }
}
