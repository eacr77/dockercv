<?php

namespace App\Http\Controllers\CLIENTAPI;

use App\Http\Controllers\Controller;
use App\Models\ComisionMiembro;
use Illuminate\Http\Request;
use App\Models\Congresista;
use App\Models\CongresistaDetalle;
use App\Models\CongresistaDatosContacto;
use App\Models\CongresistaPerfil;
use App\Models\GrupoEdad;
use App\Models\Persona;
use App\Models\ProyectoLeyAutorLegislativo;
use App\Models\ProyectoLeyPonente;
use App\Models\ControlPoliticoCitante;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CongresistaController extends Controller
{

    public function index(Request $request)
    {
        $partido = $request->input('partido');
        $operador_partido = $partido != "-1" ? "=" : "!=";
        $genero = $request->input('genero');
        $operador_genero = $genero != "-1" ? "=" : "!=";
        $circunscripcion = $request->input('circunscripcion');
        $operador_circunscripcion = $circunscripcion != "-1" ? "=" : "!=";
        $gradoEstudio = $request->input('gradoEstudio');
        $operador_gradoEstudio = $gradoEstudio != "-1" ? "=" : "!=";
        $profesion = $request->input('profesion');
        $operador_profesion = $profesion != "-1" ? "=" : "!=";
        $grupoEdad = $request->input('grupoEdad');
        $itemGrupoEdad = $grupoEdad != "-1" ? null : GrupoEdad::find($grupoEdad);
        $fechaInicio = $itemGrupoEdad != null ? Carbon::now()->subYears($itemGrupoEdad->edad_inicial)->format("Y-m-d") : null;
        $fechafinal = $itemGrupoEdad != null ? Carbon::now()->subYears($itemGrupoEdad->edad_final)->format("Y-m-d") : null;
        $comision = $request->input('comision');
        $operador_comision = $comision != "-1" ? "=" : "!=";
        $departamento = $request->input('departamento');
        $operador_departamento = $departamento != "-1" ? "=" : "!=";
        $search = $request->input('search');
        $search = str_replace(' ', '%', $search);
        $corporacion = $request->input('corporacion');
        $cuatrienio = $request->input('cuatrienio');

        $searchStr = !empty($search) ? "and (CONCAT(COALESCE(pp.apellidos,''), ' ', pp.nombres) like '%".$search."%' or CONCAT(pp.nombres, ' ', COALESCE(pp.apellidos,'')) like '%".$search."%')" : "";
        $queryStr = "select distinct
        pp.id as persona_id,
        pp.nombres,
        pp.apellidos,
        (SELECT imagen from persona_imagens where persona_id = pp.id and activo = 1 limit 1,1) as persona_imagen,
        partidos.nombre as partido,
        (SELECT imagen from partido_imagens where partido_id = partidos.id limit 1,1) as partido_imagen,
        ".($comision == "-1" ? "IFNULL((SELECT nombre from comisions left join comision_miembros cm on cm.comision_id = comisions.id where cm.congresista_id = cc.id order by comisions.id desc limit 1), NULL) as comision," 
        : "comisions.nombre as comision,")."
        cd.corporacion_id,
        cd.cuatrienio_id,
        cc.activo
        from congresistas cc
        join personas pp on pp.id = cc.persona_id
        join congresista_detalles cd on cc.id = cd.congresista_id
        join partidos on cd.partido_id = partidos.id
        ".($comision == "-1" ? "" : "left join comision_miembros cm on cm.congresista_id = cc.id left join comisions on cm.comision_id = comisions.id")."
        where pp.genero_id ".$operador_genero." ".$genero."
        ".($partido == "-1" ? "" : "and cd.partido_id ".$operador_partido." ".$partido."")."
        ".($profesion == "-1" ? "" : "and pp.profesion_id ".$operador_profesion." ".$profesion."")."
        ".($fechaInicio != null ? "and pp.fechaNacimiento <= ".$fechaInicio."" : "")."
        ".($fechafinal != null ? "and pp.fechaNacimiento >= ".$fechafinal."" : "")."
        ".($circunscripcion == "-1" ? "" : "and (cd.circunscripcion_id ".$operador_circunscripcion." ".$circunscripcion." or cd.circunscripcion_id is null)")."
        ".($comision == "-1" ? "" : "and (cm.comision_id = ".$comision." or cm.comision_id is null)")."
        ".($departamento == "-1" ? "" : "and (cd.departamento_id_mayor_votacion ".$operador_departamento." ".$departamento." or cd.departamento_id_mayor_votacion is null)")."
        ".$searchStr."
        and cd.cuatrienio_id = ".$cuatrienio."
        and cd.corporacion_id = ".$corporacion."
        group by pp.id
        order by pp.apellidos asc
        LIMIT ".(($request->input('page') - 1) * $request->input('rows')).", ".$request->input('rows').";";
       
        $items = DB::select($queryStr);
        
        
        return response($items);
    }
    public function totalrecords(Request $request)
    {
        $partido = $request->input('partido');
        $operador_partido = $partido != "-1" ? "=" : "!=";
        $genero = $request->input('genero');
        $operador_genero = $genero != "-1" ? "=" : "!=";
        $circunscripcion = $request->input('circunscripcion');
        $operador_circunscripcion = $circunscripcion != "-1" ? "=" : "!=";
        $gradoEstudio = $request->input('gradoEstudio');
        $operador_gradoEstudio = $gradoEstudio != "-1" ? "=" : "!=";
        $profesion = $request->input('profesion');
        $operador_profesion = $profesion != "-1" ? "=" : "!=";
        $grupoEdad = $request->input('grupoEdad');
        $itemGrupoEdad = $grupoEdad != "-1" ? null : GrupoEdad::find($grupoEdad);
        $fechaInicio = $itemGrupoEdad != null ? Carbon::now()->subYears($itemGrupoEdad->edad_inicial)->format("Y-m-d") : null;
        $fechafinal = $itemGrupoEdad != null ? Carbon::now()->subYears($itemGrupoEdad->edad_final)->format("Y-m-d") : null;
        $comision = $request->input('comision');
        $operador_comision = $comision != "-1" ? "=" : "!=";
        $departamento = $request->input('departamento');
        $operador_departamento = $departamento != "-1" ? "=" : "!=";
        $search = $request->input('search');
        $search = str_replace(' ', '%', $search);
        $corporacion = $request->input('corporacion');
        $cuatrienio = $request->input('cuatrienio');

        $searchStr = !empty($search) ? "and (CONCAT(COALESCE(pp.apellidos,''), ' ', pp.nombres) like '%".$search."%' or CONCAT(pp.nombres, ' ', COALESCE(pp.apellidos,'')) like '%".$search."%')" : "";
        $queryStr = "select count(ids.id) as n from 
        (SELECT pp.id from congresistas cc
        join personas pp on pp.id = cc.persona_id
        join congresista_detalles cd on cc.id = cd.congresista_id
        join partidos on cd.partido_id = partidos.id
        ".($comision == "-1" ? "" : "left join comision_miembros cm on cm.congresista_id = cc.id left join comisions on cm.comision_id = comisions.id")."
        where pp.genero_id ".$operador_genero." ".$genero."
        ".($partido == "-1" ? "" : "and cd.partido_id ".$operador_partido." ".$partido."")."
        ".($profesion == "-1" ? "" : "and pp.profesion_id ".$operador_profesion." ".$profesion."")."
        ".($fechaInicio != null ? "and pp.fechaNacimiento <= ".$fechaInicio."" : "")."
        ".($fechafinal != null ? "and pp.fechaNacimiento >= ".$fechafinal."" : "")."
        ".($circunscripcion == "-1" ? "" : "and (cd.circunscripcion_id ".$operador_circunscripcion." ".$circunscripcion." or cd.circunscripcion_id is null)")."
        ".($comision == "-1" ? "" : "and (cm.comision_id = ".$comision." or cm.comision_id is null)")."
        ".($departamento == "-1" ? "" : "and (cd.departamento_id_mayor_votacion ".$operador_departamento." ".$departamento." or cd.departamento_id_mayor_votacion is null)")."
        ".$searchStr."
        and cd.cuatrienio_id = ".$cuatrienio."
        and cd.corporacion_id = ".$corporacion."
        group by pp.id) as ids;";
        $items = DB::select($queryStr);

        return response($items[0]->n);
    }


    public function show($id)
    {
        $Congresista = Persona::where('id', $id)->with("Congresista", "PersonaTrayectoriaPublica", "PersonaTrayectoriaPublica", "LugarNacimiento", "GradoEstudio", "Genero", "Profesion", "Imagenes", "Contactos")
        ->get()->first()
        ->toJson(JSON_PRETTY_PRINT);
        return response($Congresista, 200);
    }

    public function getBasico($id)
    {
        $Congresista = Persona::where('id', $id)->with("Congresista", "Imagenes", "Contactos")
        ->get()->first()
        ->toJson(JSON_PRETTY_PRINT);
        return response($Congresista, 200);
    }

    public function getAutoriasByIdCongresista(Request $request, $id){
        $search = $request->input("search");
        $ProyectoLeyAutor = ProyectoLeyAutorLegislativo::with("Proyecto")
        ->where('congresista_id', $id)
        ->whereHas('Proyecto', function($q) use ($search){
            $q->where('titulo', 'LIKE', '%' . $search . '%');
        })
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($ProyectoLeyAutor, 200);
    }

    public function totalrecordsAutoriasByIdCongresista(Request $request, $id){
        $search = $request->input("search");
        $ProyectoLeyAutor = ProyectoLeyAutorLegislativo::with("Proyecto")
        ->where('congresista_id', $id)
        ->whereHas('Proyecto', function($q) use ($search){
            $q->where('titulo', 'LIKE', '%' . $search . '%');
        })
        ->count();
        return response($ProyectoLeyAutor, 200);
    }

    public function getPonenciasByIdCongresista(Request $request, $id){
        $search = $request->input("search");
        $ProyectoLeyPonente = ProyectoLeyPonente::with("estadoProyectoLey")
        ->where('congresista_id', $id)
        ->whereHas('estadoProyectoLey', function($q) use ($search){
            $q->whereHas('ProyectoLey', function($q2) use ($search){
                $q2->where('titulo', 'LIKE', '%' . $search . '%');
            });
        })
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->get()
        ->toJson(JSON_PRETTY_PRINT);
        return response($ProyectoLeyPonente, 200);
    }
    public function totalrecordsPonenciasByIdCongresista(Request $request, $id){
        $search = $request->input("search");
        $ProyectoLeyPonente = ProyectoLeyPonente::with("estadoProyectoLey")
        ->where('congresista_id', $id)
        ->whereHas('estadoProyectoLey', function($q) use ($search){
            $q->whereHas('ProyectoLey', function($q2) use ($search){
                $q2->where('titulo', 'LIKE', '%' . $search . '%');
            });
        })
        ->count();
        return response($ProyectoLeyPonente, 200);
    }

    public function getCitantesByIdCongresista(Request $request, $id){

        $search = $request->input("search");
        $ControlPoliticoCitante = ControlPoliticoCitante::with("controlPolitico")
        ->where('congresista_id', $id)
        ->whereHas('controlPolitico', function($q) use ($search){
            $q->where('titulo', 'LIKE', '%' . $search . '%');
        })
        ->skip(($request->input('page') - 1) * $request->input('rows'))
        ->take($request->input('rows'))
        ->get()
        ->toJson(JSON_PRETTY_PRINT);

        return response($ControlPoliticoCitante, 200);
    }
    public function totalrecordsCitantesByIdCongresista(Request $request, $id){
        $search = $request->input("search");
        $ControlPoliticoCitante = ControlPoliticoCitante::with("controlPolitico")
        ->where('congresista_id', $id)
        ->whereHas('controlPolitico', function($q) use ($search){
            $q->where('titulo', 'LIKE', '%' . $search . '%');
        })
        ->count();
        return response($ControlPoliticoCitante, 200);
    }
    public function getConflictoInteresByIdCongresista($id, $idCuatrienio){
        $result = CongresistaDetalle::with("cfInfoGeneral", "cfJuntasAsociaciones", "cfAcreencias", "cfBienes", "cfDocumentos", "cfInfoCercanias", "cfIngresos")
        ->where('congresista_id', $id)
        ->where('cuatrienio_id', $idCuatrienio)
        ->get()->first()
        ->toJson(JSON_PRETTY_PRINT);
        return response($result, 200);
    }
}
