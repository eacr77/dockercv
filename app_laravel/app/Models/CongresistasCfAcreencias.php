<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CongresistasCfAcreencias extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'congresista_detalle_id'         => 'required|int|min:0',
        'nombre'         => 'required',
        'cf_acreencia_cpt'         => 'required',
        'saldo'         => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
    ];

    public static $messagesPost = [
        'congresista_detalle_id.required' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.int' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.min' => 'Debe seleccionar un congresista',

        'nombre.required' => 'Debe ingresar un nombre',

        'cf_acreencia_cpt.required' => 'Debe ingresar un tipo de concepto',

        'saldo.required' => 'Debe ingresar un conflicto de interes',
        'saldo.regex' => 'El saldo debe estar en un rango de 13 digitos y un máximo de 2 decimales',
    ];

    protected     $fillable          = [
        'congresista_detalle_id',
        'cf_acreencia_cpt',
        'saldo',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

//    public function acreenciaConcepto()
//    {
//        return $this->hasOne(CfAcreenciaCpt::class, 'id', 'cf_acreencia_cpt_id')->where('activo',1);
//    }
}
