<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ComisionMiembro extends Model
{
    use HasFactory;

    protected $fillable = [
        'comision_id',
        'congresista_id',
        'cuatrienio_id',
        'cargo_legislativo_id_comision',
        'fecha_inicio',
        'fecha_fin',
        'usercreated',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];
    protected $hidden = [
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    public function comision()
    {
        return $this->hasOne('App\Models\Comision', 'id', 'comision_id');
    }

    public function congresista()
    {
        return $this->hasOne('App\Models\Congresista', 'id', 'congresista_id')
        ->with(["detalle", "persona"]);
    }

    public function congresistaCliente()
    {
        return $this->hasOne('App\Models\Congresista', 'id', 'congresista_id')
        ->with(["detalleComisiones", "personaComisiones"]);
    }

    public function congresistaElecciones()
    {
        return $this->hasOne('App\Models\Congresista', 'id', 'congresista_id')
        ->with("detalle", "personaElecciones");
    }

    public function cuatrienio()
    {
        return $this->hasOne('App\Models\Cuatrienio', 'id', 'cuatrienio_id');
    }

    public function comisionCargoCongresista()
    {
        return $this->hasOne('App\Models\CargoLegislativo', 'id', 'cargo_legislativo_id_comision');
    }
}
