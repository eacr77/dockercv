<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CongresistaDetalle extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'corporacion_id'     => 'required|int|min:1',
        'cuatrienio_id'      => 'required|int|min:1',
        'partido_id'         => 'required|int|min:1',
        // 'curul_id'           => 'required|int|min:1',
        'circunscripcion_id' => 'required|int|min:1'
    ];

    public static $messagesPost = [
        'corporacion_id.min' => 'Debe seleccionar un tipo de corporación',
        'cuatrienio_id.min' => 'Debe seleccionar un cuatrienio',
        'partido_id.min' => 'Debe seleccionar un partido',
        // 'curul_id.min' => 'Debe seleccionar un curul',
        'circunscripcion_id.min' => 'Debe seleccionar una circunscripción'
    ];

    public static $rulesPut = [
        'congresista_id' => 'required|int|min:1',
        'corporacion_id' => 'required|int|min:1',
        'cuatrienio_id' => 'required|int|min:1',
        'partido_id' => 'required|int|min:1',
        // 'curul_id' => 'required|int|min:1',
        'circunscripcion_id' => 'required|int|min:1'
    ];

    public static $messagesPut = [
        'congresista_id.min' => 'Debe seleccionar un congresista',
        'corporacion_id.min' => 'Debe seleccionar un tipo de corporación',
        'cuatrienio_id.min' => 'Debe seleccionar un cuatrienio',
        'partido_id.min' => 'Debe seleccionar un partido',
        // 'curul_id.min' => 'Debe seleccionar un curul',
        'circunscripcion_id.min' => 'Debe seleccionar una circunscripción'
    ];

    protected     $fillable          = [
        'congresista_id',
        'corporacion_id',
        'cuatrienio_id',
        'departamento_id_mayor_votacion',
        'partido_id',
        'curul_id',
        'circunscripcion_id',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];
    protected     $hidden            = [
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];


    public function departamento()
    {
        return $this->hasOne('App\Models\Departamento', 'id', 'departamento_id_mayor_votacion')->where('activo',1);
    }
    public function corporacion()
    {
        return $this->hasOne('App\Models\Corporacion', 'id', 'corporacion_id')->where('activo',1);
    }
    public function cuatrienio()
    {
        return $this->hasOne('App\Models\Cuatrienio', 'id', 'cuatrienio_id')->where('activo',1);
    }
    public function partido()
    {
        return $this->hasOne('App\Models\Partido', 'id', 'partido_id')->with("partidoImagen")->where('activo',1);
    }
    public function curul()
    {
        return $this->hasOne('App\Models\Curul','id','curul_id')->where('activo',1);
    }
    public function circunscripcion()
    {
        return $this->hasOne('App\Models\Circunscripcion','id', 'circunscripcion_id')->where("activo", 1);
    }
    public function congresista()
    {
        return $this->hasOne('App\Models\Congresista','id', 'congresista_id')->with("reemplazo", "persona")->where("congresistas.activo", 1);
    }
    public function cfInfoGeneral()
    {
        return $this->hasOne(Congresistas_cf_general::class, 'congresista_detalle_id', 'id')->where('activo',1);
    }
    public function cfJuntasAsociaciones()
    {
        return $this->hasMany(Congresistas_cf_juntas_asociaciones::class, 'congresista_detalle_id', 'id')->with("calidadMiembro", "organo", "pais")->where('activo',1);
    }
    public function cfAcreencias()
    {
        return $this->hasMany(CongresistasCfAcreencias::class, 'congresista_detalle_id', 'id')
            //->with("acreenciaConcepto")
            ->where('activo',1);
    }
    public function cfBienes()
    {
        return $this->hasMany(CongresistasCfBienes::class, 'congresista_detalle_id', 'id')
            //->with("tipoBien", "pais", "departamento")
            ->with("pais", "departamento")
            ->where('activo',1);
    }
    public function cfDocumentos()
    {
        return $this->hasMany(CongresistasCfDocumentos::class, 'congresista_detalle_id', 'id')->with("tipoDocumento",)->orderBy('year', 'desc')->where('activo',1);
    }
    public function cfInfoCercanias()
    {
        return $this->hasMany(CongresistasCfInfoCercanias::class, 'congresista_detalle_id', 'id')->with("parentesco")->where('activo',1);
    }
    public function cfIngresos()
    {
        return $this->hasOne(CongresistasCfIngresos::class, 'congresista_detalle_id', 'id')->where('activo',1);
    }
}
