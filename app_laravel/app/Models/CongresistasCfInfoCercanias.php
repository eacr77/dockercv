<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CongresistasCfInfoCercanias extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'congresista_detalle_id'         => 'required|int|min:0',
        'nombre'         => 'required',
        'cf_parentesco_id'         => 'required|int|min:0',
        'conflicto_interes'         => 'required',
    ];

    public static $messagesPost = [
        'congresista_detalle_id.required' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.int' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.min' => 'Debe seleccionar un congresista',

        'nombre.required' => 'Debe ingresar un nombre',

        'cf_parentesco_id.required' => 'Debe seleccionar un tipo de cf parentesco',
        'cf_parentesco_id.int' => 'Debe seleccionar un tipo de cf parentesco',
        'cf_parentesco_id.min' => 'Debe seleccionar un tipo de cf parentesco',

        'conflicto_interes.required' => 'Debe ingresar un conflicto de interes',
    ];

    protected     $fillable          = [
        'congresista_detalle_id',
        'nombre',
        'cf_parentesco_id',
        'conflicto_interes',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    public function parentesco()
    {
        return $this->hasOne(CfParentesco::class, 'id', 'cf_parentesco_id')->where('activo',1);
    }
}
