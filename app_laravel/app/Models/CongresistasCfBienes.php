<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CongresistasCfBienes extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'congresista_detalle_id'         => 'required|int|min:0',
        'cf_tipo_bien'         => 'required',
        'pais_id'         => 'required|int|min:0',
        'cf_departamento_id'         => 'required|int|min:0',
        'valor'         => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
    ];

    public static $messagesPost = [
        'congresista_detalle_id.required' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.int' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.min' => 'Debe seleccionar un congresista',

        'cf_tipo_bien.required' => 'Debe ingresar un cf tipo de bien',

        'pais_id.required' => 'Debe seleccionar un tipo pais',
        'pais_id.int' => 'Debe seleccionar un tipo pais',
        'pais_id.min' => 'Debe seleccionar un tipo pais',

        'cf_departamento_id.required' => 'Debe seleccionar un departamento',
        'cf_departamento_id.int' => 'Debe seleccionar un departamento',
        'cf_departamento_id.min' => 'Debe seleccionar un departamento',

        'valor.required' => 'Debe ingresar un conflicto de interes',
        'valor.regex' => 'El saldo debe estar en un rango de 13 digitos y un máximo de 2 decimales',
    ];

    protected     $fillable          = [
        'congresista_detalle_id',
        'cf_tipo_bien',
        'pais_id',
        'cf_departamento_id',
        'valor',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

//    public function tipoBien()
//    {
//        return $this->hasOne(CfTipoBien::class, 'id', 'cf_tipo_bien_id')->where('activo',1);
//    }
    public function pais()
    {
        return $this->hasOne(Pais::class, 'id', 'pais_id')->where('activo',1);
    }
    public function departamento()
    {
        return $this->hasOne(CfDepartamento::class, 'id', 'cf_departamento_id')->where('activo',1);
    }
}
