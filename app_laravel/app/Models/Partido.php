<?php
    namespace App\Models;

    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Database\Eloquent\Model;

    class Partido extends Model
    {
        use HasFactory;

        public static $rulesPost         = [
            'nombre'                 => 'required|max:200|min:3',
            'fechaDeCreacion'        => 'required',
            'lugar'                  => 'required|max:100|min:3',
            'color'                  => 'required|string',
            'imagen'                 => 'required'
        ];
        public static $rulesPostMessages = [
            'nombre.required'                 => 'El nombre del partido es requerido.',
            'nombre.max'                      => 'El nombre del partido no puede ser mayor a :max caracteres.',
            'nombre.min'                      => 'El nombre del partido no puede ser menor a :min caracteres.',
            'fechaDeCreacion.required'        => 'La fecha es requerido.',
            'lugar.required'                  => 'El lugar es requerido.',
            'lugar.max'                       => 'El lugar no puede ser mayor a :max caracteres.',
            'lugar.min'                       => 'El lugar no puede ser menor a :min caracteres.',
            'color.required'                  => 'El color del partido es requerido',
            'imagen.required'                       => 'La imagen del partido es requerida',
        ];
        public static $rulesPut          = [
            'nombre'                 => 'required|max:200|min:3',
            'fechaDeCreacion'        => 'required',
            'lugar'                  => 'required|max:100|min:3',
            'color'                  => 'required|string'
        ];
        public static $rulesPutMessages  = [
            'nombre.required'                 => 'El nombre del partido es requerido.',
            'nombre.max'                      => 'El nombre del partido no puede ser mayor a :max caracteres.',
            'nombre.min'                      => 'El nombre del partido no puede ser menor a :min caracteres.',
            'fechaDeCreacion.required'        => 'La fecha es requerido.',
            'lugar.required'                  => 'El lugar es requerido.',
            'lugar.max'                       => 'El lugar no puede ser mayor a :max caracteres.',
            'lugar.min'                       => 'El lugar no puede ser menor a :min caracteres.',
            'color.required'                  => 'El color del partido es requerido'
        ];
        protected     $fillable          = [
            'nombre',
            'resenaHistorica',
            'lineamientos',
            'lugar',
            'color',
            'fechaDeCreacion',
            'estatutos',
            'activo',
            'usercreated',
            'usermodifed',
            'created_at',
            'updated_at'
        ];
        protected     $hidden            = [
            'usercreated',
            'usermodifed',
            'created_at',
            'updated_at'
        ];

        public function partidoDatosContacto()
        {
            return $this->hasMany('App\Models\PartidoDatosContacto')->with("datosContacto")->where(
                'activo',
                1
            );
        }

        public function partidoImagen()
        {
            return $this->hasMany('App\Models\PartidoImagen')->where(
                'activo',
                1
            );
        }
    }
