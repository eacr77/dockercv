<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Congresistas_cf_juntas_asociaciones extends Model
{
    use HasFactory;
    public static $rulesPost = [
        'congresista_detalle_id'         => 'required|int|min:0',
        'cf_organo_id'         => 'required|int|min:0',
        'cf_calidad_miembro_id'         => 'required|int|min:0',
        'pais_id'         => 'required|int|min:0',
        'nombre_entidad'         => 'required',
    ];

    public static $messagesPost = [
        'congresista_detalle_id.required' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.int' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.min' => 'Debe seleccionar un congresista',

        'cf_organo_id.required' => 'Debe seleccionar un tipo de organo',
        'cf_organo_id.int' => 'Debe seleccionar un tipo de organo',
        'cf_organo_id.min' => 'Debe seleccionar un tipo de organo',

        'cf_calidad_miembro_id.required' => 'Debe seleccionar un tipo de calidad de miembro',
        'cf_calidad_miembro_id.int' => 'Debe seleccionar un tipo de calidad de miembro',
        'cf_calidad_miembro_id.min' => 'Debe seleccionar un tipo de calidad de miembro',

        'pais_id.required' => 'Debe seleccionar un pais',
        'pais_id.int' => 'Debe seleccionar un pais',
        'pais_id.min' => 'Debe seleccionar un pais',

        'nombre_entidad.required' => 'Debe ingresar un nombre a la entidad',

    ];

    protected     $fillable          = [
        'congresista_detalle_id',
        'nombre_entidad',
        'cf_organo_id',
        'cf_calidad_miembro_id',
        'pais_id',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    public function calidadMiembro()
    {
        return $this->hasOne(CfCalidadMiembro::class, 'id', 'cf_calidad_miembro_id')->where('activo',1);
    }
    public function organo()
    {
        return $this->hasOne(CfOrgano::class, 'id', 'cf_organo_id')->where('activo',1);
    }
    public function pais()
    {
        return $this->hasOne(Pais::class, 'id', 'pais_id')->where('activo',1);
    }
}
