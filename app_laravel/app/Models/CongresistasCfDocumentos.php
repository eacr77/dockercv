<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CongresistasCfDocumentos extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'congresista_detalle_id'         => 'required|int|min:0',
        'cf_tipo_documento_id'         => 'required|int|min:0',
        'year'         => 'required',
        'url_documento'         => 'required',
        'nombre'         => 'required',
    ];

    public static $messagesPost = [
        'congresista_detalle_id.required' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.int' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.min' => 'Debe seleccionar un congresista',

        'cf_tipo_documento_id.required' => 'Debe seleccionar un tipo de cf tipo de bien',
        'cf_tipo_documento_id.int' => 'Debe seleccionar un tipo de cf tipo de bien',
        'cf_tipo_documento_id.min' => 'Debe seleccionar un tipo de cf tipo de bien',

        'year.required' => 'Debe ingresar un año',

        'url_documento.required' => 'Debe ingresar una url del documento',

        'nombre.required' => 'Debe ingresar un nombre para el documento',
    ];

    protected     $fillable          = [
        'congresista_detalle_id',
        'cf_tipo_documento_id',
        'year',
        'url_documento',
        'nombre',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    public function tipoDocumento()
    {
        return $this->hasOne(CfTipoDocumento::class, 'id', 'cf_tipo_documento_id')->where('activo',1);
    }
}
