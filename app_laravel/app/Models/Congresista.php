<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Congresista extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'persona_id'         => 'required|int|min:1'
    ];

    public static $messagesPost = [
        'persona_id.min' => 'Debe seleccionar una persona'
    ];

    public static $rulesPut = [
        'persona_id' => 'required|int|min:1'
    ];

    public static $messagesPut = [
        'persona_id.min' => 'Debe seleccionar una persona',
    ];

    protected     $fillable          = [
        'persona_id',
        'urlHojaVida',
        'reemplazado',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];
    protected     $hidden            = [
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];
    public function reemplazo()
    {
        return $this->hasOne('App\Models\CongresistaReemplazo', 'persona_id_reemplazado', 'persona_id')->with("persona", "congresista")->where('activo',1);
    }
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'persona_id')->with("LugarNacimiento", "Imagenes", "Profesion", 'GradoEstudio', 'Genero', 'PersonaTrayectoriaPublica','PersonaTrayectoriaPrivada', 'Contactos');
    }
    public function personaAutorLegislativo()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'persona_id')->with("Imagenes");
    }
    public function personaVotaciones()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'persona_id')->with("Imagenes");
    }
    public function personaComisiones()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'persona_id')->with("Imagenes");
    }
    public function personaElecciones()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'persona_id')->with("PersonaTrayectoriaPublica", "PersonaTrayectoriaPrivada", "Profesion", "GradoEstudio", "Imagenes");
    }
    public function investigaciones()
    {
        return $this->hasMany('App\Models\Investigacion')->with("tipoInvestigacion")->where('activo',1);
    }
    public function cargo()
    {
        return $this->hasOne('App\Models\CargoCamara')->with("cuatrienio", "corporacion", "cargo")->where('activo',1);
    }
    public function autorias()
    {
        return $this->hasMany('App\Models\ProyectoLeyAutorLegislativo', 'congresista_id', 'id')->with("Proyecto");
    }
    public function ponencias()
    {
        return $this->hasMany('App\Models\ProyectoLeyPonente','congresista_id', 'id');
    }
    public function citante()
    {
        return $this->hasMany('App\Models\ControlPoliticoCitante','congresista_id', 'id');
    }
    public function detalle()
    {
        return $this->hasMany('App\Models\CongresistaDetalle', 'congresista_id', 'id')->with("departamento", "corporacion", "cuatrienio", "partido", "circunscripcion")->orderBy("cuatrienio_id", "desc");
    }
    public function detalleProyecto()
    {
        return $this->hasOne('App\Models\CongresistaDetalle', 'congresista_id', 'id')->with("partido", "corporacion")->orderBy("cuatrienio_id", "desc");
    }
    public function detalleVotaciones()
    {
        return $this->hasOne('App\Models\CongresistaDetalle', 'congresista_id', 'id')->with("partido", "corporacion")->orderBy("cuatrienio_id", "desc");
    }
    public function detalleComisiones()
    {
        return $this->hasOne('App\Models\CongresistaDetalle', 'congresista_id', 'id')->with("partido", "corporacion")->orderBy("cuatrienio_id", "desc");
    }
}
