<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CongresistasCfIngresos extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'congresista_detalle_id'         => 'required|int|min:0',
        'salario_anual'         => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
        'interes_cesantias_anual'  => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
        'gasto_representacion_anual' => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
        'arriendo_anual'         => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
        'honorario_anual'         => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
        'otros_anual'         => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
        'total_anual'         => 'required|regex:/^[0-9]{0,13}+([.][0-9]{0,2}+)?$/',
    ];

    public static $messagesPost = [
        'congresista_detalle_id.required' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.int' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.min' => 'Debe seleccionar un congresista',

        'salario_anual.required' => 'Debe ingresar un salario anual',
        'salario_anual.regex' => 'El salario anual debe estar en un rango de 13 digitos y un máximo de 2 decimales',

        'interes_cesantias_anual.required' => 'Debe ingresar un interes de cesantía',
        'interes_cesantias_anual.regex' => 'El salario anual debe estar en un rango de 13 digitos y un máximo de 2 decimales',

        'gasto_representacion_anual.required' => 'Debe ingresar un gasto de representación anual',
        'gasto_representacion_anual.regex' => 'El salario anual debe estar en un rango de 13 digitos y un máximo de 2 decimales',

        'arriendo_anual.required' => 'Debe ingresar un arriendo anual',
        'arriendo_anual.regex' => 'El arriendo anual debe estar en un rango de 13 digitos y un máximo de 2 decimales',

        'honorario_anual.required' => 'Debe ingresar un honorario anual',
        'honorario_anual.regex' => 'El honorario anual debe estar en un rango de 13 digitos y un máximo de 2 decimales',

        'otros_anual.required' => 'Debe ingresar un otros anual',
        'otros_anual.regex' => 'El otros anual debe estar en un rango de 13 digitos y un máximo de 2 decimales',

        'total_anual.required' => 'Debe ingresar un total anual',
        'total_anual.regex' => 'El total anual debe estar en un rango de 13 digitos y un máximo de 2 decimales',
    ];

    protected     $fillable          = [
        'congresista_detalle_id',
        'salario_anual',
        'interes_cesantias_anual',
        'gasto_representacion_anual',
        'arriendo_anual',
        'honorario_anual',
        'otros_anual',
        'total_anual',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];
}
