<?php

namespace App\Models;

use App\Http\Controllers\API\CongresistasCfBienesController;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Congresistas_cf_general extends Model
{
    use HasFactory;
    public static $rulesPost = [
        'congresista_detalle_id'         => 'required|int|min:0',
    ];

    public static $messagesPost = [
        'congresista_detalle_id.required' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.int' => 'Debe seleccionar un congresista',
        'congresista_detalle_id.min' => 'Debe seleccionar un congresista'
    ];

    protected     $fillable          = [
        'congresista_detalle_id',
        'conflictos_declarados',
        'actividad_economica',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    public function Juntas(){
        return $this->hasMany(
            Congresistas_cf_juntas_asociaciones::class,
            'congresista_detalle_id',
            'congresista_detalle_id'
        )->where('activo',1);
    }

    public function Cercanias(){
        return $this->hasMany(
            CongresistasCfInfoCercanias::class,
            'congresista_detalle_id',
            'congresista_detalle_id'
        )->where('activo',1);
    }

    public function Ingresos(){
        return $this->hasMany(
            CongresistasCfIngresos::class,
            'congresista_detalle_id',
            'congresista_detalle_id'
        )->where('activo',1);
    }

    public function Acreencias(){
        return $this->hasMany(
            CongresistasCfAcreencias::class,
            'congresista_detalle_id',
            'congresista_detalle_id'
        )->where('activo',1);
    }

    public function Bienes(){
        return $this->hasMany(
            CongresistasCfBienes::class,
            'congresista_detalle_id',
            'congresista_detalle_id'
        )->where('activo',1);
    }

    public function Documentos(){
        return $this->hasMany(
            CongresistasCfDocumentos::class,
            'congresista_detalle_id',
            'congresista_detalle_id'
        )->orderBy('year', 'desc')->where('activo',1);
    }
}
