<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'nombre' => 'required|max:50|min:3',
        'departamento_id' => 'numeric|required|min:0|not_in:0',
    ];

    public static $rulesPostMessages = [
        'nombre.required' => 'El nombre municipio es requerido.',
        'nombre.max' => 'El nombre del municipio no puede ser mayor a :max caracteres.',
        'nombre.min' => 'El nombre del municipio no puede ser menor a :min caracteres.',

        'departamento_id.numeric' => 'El departemento es requerido.',
        'departamento_id.required' => 'El departemento es requerido.',
        'departamento_id.min' => 'El departemento es requerido.',
        'departamento_id.not_in' => 'El departemento es requerido.',
    ];

    public static $rulesPut = [
        'nombre' => 'required|max:50|min:3',
        'departamento_id' => 'numeric|required|min:0|not_in:0',
    ];

    public static $rulesPutMessages = [
        'nombre.required' => 'El nombre del municipio es requerido.',
        'nombre.max' => 'El nombre del municipio no puede ser mayor a :max caracteres.',
        'nombre.min' => 'El nombre del municipio no puede ser menor a :min caracteres.',

        'departamento_id.numeric' => 'El departemento es requerido.',
        'departamento_id.required' => 'El departemento es requerido.',
        'departamento_id.min' => 'El departemento es requerido.',
        'departamento_id.not_in' => 'El departemento es requerido.',
    ];

    protected $fillable = [
        'departamento_id',
        'nombre',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    public function Departamento(){
        return $this->hasOne(Departamento::class, 'id', 'departamento_id')
                    ->select(['id', 'nombre']);
    }
}
