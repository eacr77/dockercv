<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ControlPoliticoCitado extends Model
{
    use HasFactory;

    //Rules
    public static $rulesPost                        = [
        'persona_id'                        => 'required|int|min:1',
        // 'asistencia'                        => 'required|int|min:1',
    ];

    public static $rulesPostMessages = [
        'persona_id.min'                    => 'Debe seleccionar una persona.',
        // 'asistencia.min'                    => 'Debe indicar la asistencia.',

    ];

    public static $rulesPut                        = [
        'persona_id'                        => 'required|int|min:1',
        // 'asistencia'                        => 'required|int|min:1',
    ];

    public static $rulesPutMessages = [
        'persona_id.min'                    => 'Debe seleccionar una persona.',
        // 'asistencia.min'                    => 'Debe indicar la asistencia.',

    ];
    //End rules


    //Atributes
    protected     $fillable          = [
        'control_politico_id',
        'persona_id',
        'representante',
        'asistencia',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];
    protected     $hidden            = [
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    //End atributes

    //Relations

    public function controlPolitico()
    {
        return $this->hasOne('App\Models\ControlPolitico', 'id', 'control_politico_id');
    }
    
    public function persona()
    {
        return $this->hasOne('App\Models\Persona', 'id', 'persona_id')->with("Imagenes");
    }
    // public function tipoCitacion()
    // {
    //     return $this->hasOne('App\Models\TipoCitacion', 'id', 'tipo_citacion_id');
    // }

    // public function controlPoliticoCitadoImagenes()
    // {
    //     return $this->hasMany('App\Models\ControlPoliticoCitadoImagen','citado_id','id')->where(
    //         'activo',
    //         1
    //     );
    // }
    //End relations
}
