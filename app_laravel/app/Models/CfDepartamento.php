<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CfDepartamento extends Model
{
    use HasFactory;

    public static $rulesPost = [
        'nombre' => 'required|max:150|min:3',
        'pais_id' => 'numeric|required|min:0|not_in:0',
    ];

    public static $rulesPut = [
        'nombre' => 'required|max:150|min:3',
        'pais_id' => 'numeric|required|min:0|not_in:0',
    ];

    public static $messagesPost = [
        'nombre.required' => 'El nombre es requerido.',
        'nombre.max' => 'El nombre no puede ser mayor a :max caracteres.',
        'nombre.min' => 'El nombre no puede ser menor a :min caracteres.',

        'pais_id.numeric' => 'El pais es requerido.',
        'pais_id.required' => 'El pais es requerido.',
        'pais_id.min' => 'El pais es requerido.',
        'pais_id.not_in' => 'El pais es requerido.',
    ];

    public static $messagesPut = [
        'nombre.required' => 'El nombre es requerido.',
        'nombre.max' => 'El nombre no puede ser mayor a :max caracteres.',
        'nombre.min' => 'El nombre no puede ser menor a :min caracteres.',

        'pais_id.numeric' => 'El pais es requerido.',
        'pais_id.required' => 'El pais es requerido.',
        'pais_id.min' => 'El pais es requerido.',
        'pais_id.not_in' => 'El pais es requerido.',
    ];

    protected $fillable = [
        'pais_id',
        'nombre',
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    public function Pais(){
        return $this->hasOne(Pais::class, 'id', 'pais_id');
    }
}
