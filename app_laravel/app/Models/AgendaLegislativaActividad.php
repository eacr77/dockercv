<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgendaLegislativaActividad extends Model
{
    use HasFactory;
    public $table = "agenda_legislativa_actividads";
    public static $rules = [        
        'agenda_legislativa_id'=>'required',
        'orden'=>'required|int|min:1',
        'titulo'=>'required',                
        'tipo_actividad_id'=>'required|int|min:1'             
    ];

    public static $messages = [                
        'agenda_legislativa_id.required' => 'La id de agenda es requerida.',
        'orden.min' => 'Ingrese el orden',   
        'titulo.required' => 'El titulo es requerido.',
        'orden.required' => 'El orden es requerido.',
        'tipo_actividad_id.required' => 'Debe seleccionar un tipo de actividad',
        'tipo_actividad_id.min' => 'Debe seleccionar un tipo de actividad'
                     
    ];

    protected $fillable = [                
        'agenda_legislativa_id',
        'titulo',        
        'destacado',
        'descripcion',
        'tipo_actividad_id',
        'proyecto_ley_id',
        'control_politico_id',
        'orden',        
        'activo',
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];

    protected $hidden = [
        'usercreated',
        'usermodifed',
        'created_at',
        'updated_at'
    ];
    public function agenda()
    {
        return $this->hasOne('App\Models\AgendaLegislativa', 'id', 'agenda_legislativa_id')->with('agendaComision');
    }
    public function tipoActividad()
    {
        return $this->hasOne('App\Models\TipoActividadAgendaLegislativa', 'id', 'tipo_actividad_id');
    }
    public function selected()
    {
        return $this->hasOne('App\Models\ProyectoLey', 'id', 'proyecto_ley_id');
    }
    public function citacion()
    {
        return $this->hasOne('App\Models\ControlPolitico', 'id', 'control_politico_id')->with(['corporacion','cuatrienio','legislatura']);
    }
}
