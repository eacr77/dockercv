<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogNdConceptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_nd_conceptos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->integer('blog_nd_id')->nullable()->unsigned();
            $table->integer('glosario_legislativo_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('blog_nd_conceptos', function (Blueprint $table) {
            $table->foreign('blog_nd_id')->references('id')->on('blog_nds')->onDelete('cascade');
            $table->foreign('glosario_legislativo_id', 'FK_gl_legislativo_blog_id')->references('id')->on('glosario_legislativos')->onDelete('cascade');
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_nd_conceptos');
    }
}
