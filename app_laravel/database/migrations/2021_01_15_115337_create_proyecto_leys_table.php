<?php

    use App\Models\ProyectoLey;
    use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateProyectoLeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_leys', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->smallInteger('cuatrienio_id')->nullable()->unsigned();
            $table->smallInteger('legislatura_id')->nullable()->unsigned();
            $table->smallInteger('corporacion_id')->nullable()->unsigned();
            $table->string('titulo', 500)->nullable();
            $table->string('alias', 500)->nullable();
            $table->date('fecha_radicacion')->nullable();
            $table->string('numero_camara', 20)->nullable();
            $table->string('numero_senado', 20)->nullable();
            $table->smallInteger('iniciativa_id')->unsigned()->unsigned();
            $table->tinyInteger('tipo_proyecto_id')->nullable()->unsigned();
            $table->smallInteger('tema_id_principal')->nullable()->unsigned();
            $table->smallInteger('tema_id_secundario')->nullable()->unsigned();
            $table->text('sinopsis')->nullable();
            $table->unsignedBigInteger('se_acumula_a_id')->nullable();
            $table->smallInteger('alcance_id')->nullable()->unsigned();
            $table->boolean('iniciativa_popular')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        Schema::table('proyecto_leys', function (Blueprint $table) {
            $table->foreign('cuatrienio_id')->references('id')->on('cuatrienios')->onDelete('cascade');
            $table->foreign('legislatura_id')->references('id')->on('legislaturas')->onDelete('cascade');
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
            $table->foreign('iniciativa_id')->references('id')->on('iniciativas')->onDelete('cascade');
            $table->foreign('tipo_proyecto_id')->references('id')->on('tipo_proyectos')->onDelete('cascade');
            $table->foreign('tema_id_principal')->references('id')->on('temas')->onDelete('cascade');
            $table->foreign('tema_id_secundario')->references('id')->on('temas')->onDelete('cascade');
            $table->foreign('alcance_id')->references('id')->on('alcances')->onDelete('cascade');
        });
        $this->setDataToTable();
    }
    /**
     * Set data to table.
     *
     * @return void
     */
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_proyecto_leys.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            //$data = array_map("utf8_encode", $data); //added
            $data = mb_convert_encoding($data, 'UTF-8', 'Windows-1252');
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $fecha_radicacion = $import_data[6] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[6]
                );

            $fecha_radicacion = $fecha_radicacion
                ? $fecha_radicacion->format('Y-m-d')
                : null;

            $created_at = $import_data[20] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[20]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[21] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[21]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "cuatrienio_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "legislatura_id"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "corporacion_id"=>$import_data[3] === 'NA' ? null : $import_data[3],
                "titulo"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "alias"=>$import_data[5] === 'NA' ? null : $import_data[5],
                "fecha_radicacion"=>$fecha_radicacion,
                "numero_camara"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "numero_senado"=>$import_data[8] === 'NA' ? null : $import_data[8],
                "iniciativa_id"=>$import_data[9] === 'NA' ? null : $import_data[9],
                "tipo_proyecto_id"=>$import_data[10] === 'NA' ? null : $import_data[10],
                "tema_id_principal"=>$import_data[11] === 'NA' ? null : $import_data[11],
                "tema_id_secundario"=>$import_data[12] === 'NA' ? null : $import_data[12],
                "sinopsis"=>$import_data[13] === 'NA' ? null : $import_data[13],
                "se_acumula_a_id"=>$import_data[14] === 'NA' ? null : $import_data[14],
                "alcance_id"=>$import_data[15] === 'NA' ? null : $import_data[15],
                "iniciativa_popular"=>$import_data[16] === 'NA' ? null : $import_data[16],
                "activo"=>$import_data[17] === 'NA' ? null : $import_data[17],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            ProyectoLey::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_leys');
    }
}
