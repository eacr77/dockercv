<?php

use App\Models\Investigacion;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvestigacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investigacions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->integer('congresista_id')->nullable()->unsigned();
            $table->integer('tipo_investigacion_id')->nullable()->unsigned();
            $table->text('descripcion')->nullable();
            $table->boolean('activo')->nullable()->default(1);
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();

            // $table->foreign("congresista_id")
            // ->references("id")
            // ->on("congresistas")
            // ->onDelete("cascade");

            $table->foreign("tipo_investigacion_id")
            ->references("id")
            ->on("tipo_investigacions")
            ->onDelete("cascade");
        });
        $this->setDataToTable();
    }
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_investigacions2.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen($filepath, "r");
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            // $data = array_map(
            //     "utf8_encode",
            //     $data
            // ); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
                // var_dump($cell_value);
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            // $created_at = $import_data[7] === 'NA'
            //     ? null
            //     : DateTime::createFromFormat(
            //         'Y-m-d H:i:s',
            //         $import_data[7]
            //     );

            // $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            // $updated_at = $import_data[8] === 'NA'
            //     ? null
            //     : DateTime::createFromFormat(
            //         'Y-m-d H:i:s',
            //         $import_data[8]
            //     );
            // $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "congresista_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "tipo_investigacion_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "descripcion" => $import_data[3] === 'NA' ? null : $import_data[3],
                "activo" => 1,
                "created_at" => $import_data[4] === 'NA' ? null : $import_data[4],
                "updated_at" => $import_data[5] === 'NA' ? null : $import_data[5],
            ];
            Investigacion::insert($insertData);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investigacions');
    }
}
