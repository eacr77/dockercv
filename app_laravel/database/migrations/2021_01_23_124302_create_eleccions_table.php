<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateEleccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Resultado de votacion - int o string?
        Schema::create('eleccions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->smallInteger('corporacion_id')->unsigned()->nullable();
            $table->smallInteger('cuatrienio_id')->unsigned()->nullable();
            $table->smallInteger('tipo_comision_id')->unsigned()->nullable();
            $table->integer('comision_id')->unsigned()->nullable();
            $table->integer('comision_miembro_id')->unsigned()->nullable();
            $table->tinyInteger('comision_cargo_congresista_id')->nullable()->unsigned();
            $table->string('titulo', 500)->nullable();
            $table->text('infoGeneral')->nullable();
            $table->date('fechaDeEleccion')->nullable();
            $table->string('resultadoVotacion')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('eleccions', function (Blueprint $table){
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
            $table->foreign('cuatrienio_id')->references('id')->on('cuatrienios')->onDelete('cascade');
            $table->foreign('tipo_comision_id')->references('id')->on('tipo_comisions')->onDelete('cascade');
            $table->foreign('comision_id')->references('id')->on('comisions')->onDelete('cascade');
            $table->foreign('comision_miembro_id')->references('id')->on('comision_miembros')->onDelete('cascade');
            $table->foreign('comision_cargo_congresista_id')->references('id')->on('cargo_legislativos')->onDelete('cascade');
        });

        // DB::table('eleccions')->insert(
        //     array(
        //         'corporacion_id' => '1',
        //         'cuatrienio_id' => '1',
        //         'tipo_comision_id' => '1',
        //         'comision_id' => '1',
        //         'comision_miembro_id' => '1',
        //         'titulo' => 'Es una prueba de elección',
        //         'fechaDeEleccion' => '2021/02/03',
        //         'infoGeneral' => 'infomación general XD',
        //         'resultadoVotacion' => '140',
        //         'usercreated' => 'sys@admin.com',
        //         'created_at' => Carbon::now()
        //     )
        // );
    }
  

    /**
     * Reverse the migrations.
     * 
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eleccions');
    }
}
