<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateCorporacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporacions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallInteger('id')->unsigned()->autoIncrement();
            $table->string('nombre', 200)->nullable();
            $table->string('descripcion', 500)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        DB::table('corporacions')->insert(
            array(
                'nombre' => 'Cámara de Representantes',
                'descripcion' => 'Esta es la cámara',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );

        DB::table('corporacions')->insert(
            array(
                'nombre' => 'Senado de la República',
                'descripcion' => 'Esta es senado',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporacions');
    }
}
