<?php

use App\Models\ControlPolitico;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlPoliticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_politicos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->smallInteger('cuatrienio_id')->nullable()->unsigned();
            $table->smallInteger('legislatura_id')->nullable()->unsigned();
            $table->smallInteger('tipo_control_politico_id')->nullable()->unsigned();
            $table->integer('comision_id')->nullable()->unsigned();
            $table->smallInteger('corporacion_id')->unsigned()->nullable();
            $table->tinyInteger('estado_control_politico_id')->nullable()->unsigned();
            $table->string('titulo', 500)->nullable();
            $table->date('fecha')->nullable();
            $table->smallInteger('tema_id_principal')->nullable()->unsigned();
            $table->smallInteger('tema_id_secundario')->nullable()->unsigned();
            $table->boolean('plenaria')->nullable();
            $table->string('tags',500)->nullable();
            $table->text('detalles')->nullable();
            $table->string('numero_proposicion', 100)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('control_politicos', function (Blueprint $table) {
            $table->foreign('legislatura_id')->references('id')->on('legislaturas')->onDelete('cascade');
            $table->foreign('cuatrienio_id')->references('id')->on('cuatrienios')->onDelete('cascade');
            $table->foreign('comision_id')->references('id')->on('comisions')->onDelete('cascade');
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
            $table->foreign('estado_control_politico_id')->references('id')->on('estado_control_politicos')->onDelete('cascade');
            $table->foreign('tema_id_principal')->references('id')->on('tema_control_politicos')->onDelete('cascade');
            $table->foreign('tema_id_secundario')->references('id')->on('tema_control_politicos')->onDelete('cascade');
            $table->foreign('tipo_control_politico_id')->references('id')->on('tipo_citacions')->onDelete('cascade');
        });
        $this->setDataToTable();
    }

    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_control_politicos.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = mb_convert_encoding($data, 'UTF-8', 'Windows-1252');
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                //$import_data_array[$i][] = trim(preg_replace('/\s+/', ' ', $cell_value));
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $fecha = $import_data[7] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[7]
                );

            $fecha = $fecha ? $fecha->format('Y-m-d H:i:s') : null;

            $created_at = $import_data[18] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[18]
                );

            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[19] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[19]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "cuatrienio_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "legislatura_id" => $import_data[3] === 'NA' ? null : $import_data[3],
                "tipo_control_politico_id" => $import_data[4] === 'NA' ? null : $import_data[4],
                "comision_id" => null,//$import_data[3] === 'NA' ? null : $import_data[3],
                "corporacion_id" => null,//$import_data[4] === 'NA' ? null : $import_data[4],
                "estado_control_politico_id" => $import_data[5] === 'NA' ? null : $import_data[5],
                "titulo" => $import_data[6] === 'NA' ? null : $import_data[6],
                "fecha" => $fecha,
                "tema_id_principal" => $import_data[8] === 'NA' ? null : $import_data[8],
                "tema_id_secundario" => $import_data[9] === 'NA' ? null : $import_data[9],
                "plenaria" => $import_data[10] === 'NA' ? null : $import_data[10],
                "tags" => $import_data[11] === 'NA' ? null : $import_data[11],
                "detalles" => $import_data[12] === 'NA' ? null : $import_data[12],
                "numero_proposicion" => $import_data[14] === 'NA' ? null : $import_data[14],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];
            ControlPolitico::insert($insertData);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_politicos');
    }
}
