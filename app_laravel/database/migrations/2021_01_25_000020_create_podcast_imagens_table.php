<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePodcastImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podcast_imagens', function (Blueprint $table) {
            $table->engine = 'InnoDB';        
            $table->increments('id');
            $table->integer('podcast_id')->nullable()->unsigned();
            $table->string('imagen',350)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable(); 
            $table->timestamps();
        });

        Schema::table('podcast_imagens', function (Blueprint $table) {
            $table->foreign('podcast_id')->references('id')->on('podcasts')->onDelete('cascade');
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('podcast_imagens');
    }
}
