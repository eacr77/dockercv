<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateTipoRespuestaVotacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_respuesta_votacions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->tinyInteger('id')->unsigned()->autoIncrement();
            $table->string('nombre', 50)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        $data = [
            ['nombre' => 'Sí', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['nombre' => 'No', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['nombre' => 'Ausente', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()]
        ];
        DB::table('tipo_respuesta_votacions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_respuesta_votacions');
    }
}
