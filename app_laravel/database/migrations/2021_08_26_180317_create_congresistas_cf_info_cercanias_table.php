<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistasCfInfoCercaniasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresistas_cf_info_cercanias', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->integer('congresista_detalle_id')->nullable()->unsigned();
            $table->string('nombre', 50)->nullable();
            $table->integer('cf_parentesco_id')->nullable()->unsigned();
            $table->string('conflicto_interes', 700)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('congresistas_cf_info_cercanias', function (Blueprint $table) {
            $table->foreign('congresista_detalle_id')->references('id')->on('congresista_detalles')->onDelete('cascade');
            $table->foreign('cf_parentesco_id')->references('id')->on('cf_parentescos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresistas_cf_info_cercanias');
    }
}
