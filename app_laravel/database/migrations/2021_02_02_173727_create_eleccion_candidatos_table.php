<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEleccionCandidatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eleccion_candidatos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->bigInteger('eleccion_id')->unsigned()->nullable();
            $table->integer('congresista_id')->nullable()->unsigned();
            $table->tinyInteger('comision_cargo_congresista_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        Schema::table('eleccion_candidatos', function (Blueprint $table) {
            $table->foreign('eleccion_id')->references('id')->on('eleccions')->onDelete('cascade');
            $table->foreign('congresista_id')->references('id')->on('congresistas')->onDelete('cascade');
            $table->foreign('comision_cargo_congresista_id')->references('id')->on('cargo_legislativos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eleccion_candidatos');
    }
}
