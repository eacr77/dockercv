<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateTipoComisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_comisions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallInteger('id')->unsigned()->autoIncrement();
            $table->smallInteger('corporacion_id')->nullable()->unsigned();
            $table->string('nombre', 50)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        Schema::table('tipo_comisions', function (Blueprint $table) {
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
        });

        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '1',
                'nombre' => 'Comisión Constitucional',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '1',
                'nombre' => 'Comisión Legal',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '1',
                'nombre' => 'Comisión Especial',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '1',
                'nombre' => 'Comisión Accidental',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '1',
                'nombre' => 'Comisión Interparlamentaria',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '2',
                'nombre' => 'Accidentales',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '2',
                'nombre' => 'Constitucionales',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '2',
                'nombre' => 'Comisiones Legales',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
        DB::table('tipo_comisions')->insert(
            array(
                'corporacion_id' => '2',
                'nombre' => 'Especiales',
                'usercreated' => 'sys@admin.com',
                'created_at' => Carbon::now()
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_comisions');
    }
}
