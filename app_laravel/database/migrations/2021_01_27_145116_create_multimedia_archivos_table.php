<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMultimediaArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('multimedia_archivos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned()->autoIncrement();        
            $table->string('archivo', 350)->nullable();
            $table->integer('multimedia_id')->nullable()->unsigned();
            $table->string('urlVideo',350)->nullable();
            $table->string('urlAudio',350)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();        
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('multimedia_archivos', function (Blueprint $table) {            
            $table->foreign('multimedia_id')->references('id')->on('multimedia')->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('multimedia_archivos');
    }
}
