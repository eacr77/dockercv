<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateCongresoVisiblesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congreso_visibles', function (Blueprint $table) {
            $table-> engine = 'InnoDB';
            $table->increments('id');
            $table->text('queEs')->nullable();
            $table->text('objetivos')->nullable();
            $table->text('historiaymision')->nullable();
            $table->text('nuestroFuturo')->nullable();
            $table->text('nuestroReto')->nullable();
            $table->boolean('activo')->dafault(1)->nullable();
            $table->string('usercreated',250)->nullable();
            $table->string('usermodifed',250)->nullable();
            $table->timestamps();
        });

        DB::table('congreso_visibles')->insert(
            array(
                'queEs'             => 'Desde 1998, el proyecto Congreso Visible del Departamento de Ciencia Política de la Universidad de losAndes realiza un seguimiento y análisis permanentes del Congreso de la República a través de la publicaciónde su actividad legislativa, fortaleciendo y promoviendo la participación de la ciudadanía en procesos deexigencia de rendición de cuentas a sus elegidos.&nbsp;Desde una perspectiva independiente del gobierno y de los partidos o movimientos políticos, Congreso Visibletrabaja para generar un puente de comunicación entre la ciudadanía y sus representantes. De igual manera,busca promover el conocimiento del Congreso de la República y, en general, de nuestro sistema democráticoproporcionando análisis e información pertinente, organizada y de fácil acceso para ciudadanos,investigadores y académicos de Colombia y el mundo.&nbsp;Así mismo, CV ha ofrecido a los medios de comunicación una fuente alternativa e independiente deinformación del Congreso y ha empoderado a la ciudadanía a través de su difusión y promoción de la votacióninformada en Colombia. Es por esto que cada 4 años se lanza la campaña Candidatos Visibles, la cual tienecomo fin visibilizar a los candidatos que se lanzan a los comicios del legislativo o del ejecutivo.',
                'objetivos'         => 'Garantizar el derecho de la ciudadanía a estar informada sobre cómo votan sus elegidos losproyectos de interés.&nbsp;Facilitar la comprensión del funcionamiento del Congreso para todo tipo de ciudadanos, ofreciendoinformación y análisis objetivos, independientes y confiables.&nbsp;Realizar seguimiento, análisis y divulgación de la actividad legislativa, para promover un mejordesempeño de la misma.Fortalecer el proceso de rendición de cuentas y ayudar a establecer puentes de comunicación entrelos congresistas y los ciudadanos.&nbsp;Mejorar las prácticas políticas y hacer más transparente la gestión del Congreso.&nbsp;Desarrollar una agenda de investigación con la participación de profesores del Departamento deCiencia Política, estudiantes de Doctorado, Maestría y Pregrado.&nbsp;Ser un referente para las investigaciones internacionales académicas sobre política comparada yCongreso.&nbsp;Fortalecer el sistema de partidos, dando a conocer sus posiciones ideológicas y haciéndolos másprogramáticos.',
                'historiaymision'   => '',
                'nuestroFuturo'     => '',
                'nuestroReto'       => 'La independencia y compromiso para comprender y mejorar la calidad de nuestras instituciones democráticases nuestro rasgo definitorio. Esperamos continuar trabajando para esos objetivos, porque creemos que lainformación independiente y accesible y el análisis riguroso son fundamentales en el camino de construir unamejor sociedad y democracia en Colombia.La Universidad de Los Andes y el Departamento de Ciencia Política ofrecen un espacio institucional y unaatmósfera únicos para realizar un análisis más sofisticado de la evolución del comportamiento del Congreso ylos partidos políticos, que permita una mejor comprensión del rol que juega el Congreso en nuestrademocracia, aún en proceso de consolidación. El Departamento de Ciencia Política de la Universidad de losAndes, el más antiguo de Colombia, alberga un grupo altamente calificado de académicos, cuyas agendas deinvestigación abordan el estudio del comportamiento del Congreso y de los legisladores, con un récordsobresaliente de publicaciones indexadas, tanto nacionales como internacionales. Además, la cercanía conotros programas académicos de la Universidad –especialmente con la Facultad de Economía y la Facultad deDerecho–incrementa los prospectos de investigaciones con aproximaciones multidisciplinarias.&nbsp;Finalmente, el compromiso de nuestro equipo ha fortalecido la capacidad institucional del mismo,manteniéndolo altamente activo. CV es un grupo interdisciplinario conformado por profesionales y estudiantesde pregrado y posgrado en programas como Ciencia Política, Derecho, Historia y Economía, lo cual nospermite abordar la realidad política del país desde múltiples perspectivas. Sumado a esto, ha recibido el apoyode cientos de estudiantes voluntarios, quienes han incrementado su capacidad. Muchas de las personas quehan trabajado con CV, de un modo u otro, se han convertido en multiplicadores de los valores quefundamentan el proyecto: la transparencia, la rendición de cuentas y la consolidación de la democracia.',
                'activo'            => true,
                'usercreated'       => 'sys@admin.com',
                'created_at'        => Carbon::now()
            )
        );
    }    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congreso_visibles');
    }
}
