<?php

use App\Models\PersonaImagen;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonaImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona_imagens', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('persona_id');
            $table->string('imagen', 350)->nullable();
            $table->boolean('activo')->nullable()->default(1);
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();

            // $table->foreign('persona_id')
            //       ->references('id')
            //       ->on('personas')
            //       ->onDelete('cascade');
        });

        $this->setDataToTable();
    }

    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_persona_imagens.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {

            if($import_data['3'] === 'NA'){
                $prefix_url_imagen = null;
            }
            // else{
            //     $id = $import_data['0'];
            //     $arr =  explode('/', $import_data['3']);
            //     $name_imagen = $arr[count($arr) - 1];
            //     $prefix_url_imagen = 'persona/'.$id.'/'.$name_imagen;
            // }

            for($i = 0; $i < 3; $i++){
                $insertData = [
                    "persona_id" => $import_data[0] === 'NA' ? null : $import_data[0],
                    "imagen" => $import_data[3],
                    "activo" => 1,
                ];

                PersonaImagen::insert($insertData);
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona_imagens');
    }
}
