<?php

use App\Models\VotacionCamara;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateVotacionCamarasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votacion_camaras', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->bigInteger('votacion_id')->nullable()->unsigned();
            $table->smallInteger('corporacion_id')->nullable()->unsigned();
            $table->timestamps();
        });
        Schema::table('votacion_camaras', function (Blueprint $table) {
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
        });
        $this->setDataToTable();
    }

    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_votacion_camaras.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "votacion_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "corporacion_id" => $import_data[2] === 'NA' ? null : $import_data[2],
            ];

            VotacionCamara::insert($insertData);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votacion_camaras');
    }
}
