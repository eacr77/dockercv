<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTipoMultimediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_multimedia', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallInteger('id')->unsigned()->autoIncrement();
            $table->string('nombre', 200)->nullable();
            $table->string('icono', 50)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        $data = [
            ['nombre' => 'Documento', 'icono' => 'fas fa-paperclip','activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['nombre' => 'Audio', 'icono' => 'fas fa-microphone-alt','activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['nombre' => 'Video', 'icono' => 'fas fa-play-circle','activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['nombre' => 'Url Video', 'icono' => 'fas fa-link','activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['nombre' => 'Url Audio', 'icono' => 'fas fa-link','activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()]
        ];

        DB::table('tipo_multimedia')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_multimedia');
    }
}
