<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresoVisibleAliadoImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congreso_visible_aliado_imagens', function (Blueprint $table) {
            $table->engine = 'InnoDB';        
            $table->increments('id');
            $table->integer('aliado_id')->nullable()->unsigned();
            $table->string('imagen',350)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable(); 
            $table->timestamps();
        });
       
        Schema::table('congreso_visible_aliado_imagens', function (Blueprint $table) {
            $table->foreign('aliado_id')->references('id')->on('congreso_visible_aliados')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congreso_visible_aliado_imagens');
    }
}
