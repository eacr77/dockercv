<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaseListaCongresistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pase_lista_congresistas', function (Blueprint $table) {
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->bigInteger('pase_lista_id')->unsigned();
            $table->integer('congresista_id')->unsigned();
            $table->tinyInteger('tipo_respuesta_pase_lista_id')->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('pase_lista_congresistas', function (Blueprint $table) {
            $table->foreign('pase_lista_id')->references('id')->on('pase_listas')->onDelete('cascade');
            $table->foreign('congresista_id')->references('id')->on('congresistas')->onDelete('cascade');
            $table->foreign('tipo_respuesta_pase_lista_id')->references('id')->on('tipo_respuesta_pase_listas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pase_lista_congresistas');
    }
}
