<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformacionSitiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacion_sitios', function (Blueprint $table) {
            $table->engine = "InnoDB";
             $table->increments('id');
            $table->string('imgPrincipal')->nullable();
            $table->text('congresistas')->nullable();
            $table->text('nuestraDemocracia')->nullable();
            $table->text('actividadLegislativa')->nullable();
            $table->string('igmActividadLegislativa')->nullable();
            $table->text('comisiones')->nullable();
            $table->text('contenidoMultimedia')->nullable();
            $table->text('proyectosDeLey')->nullable();
            $table->text('datos')->nullable();
            $table->text('conflictoInteres')->nullable();
            $table->text('observacionesLegales')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        $data = [
            ['congresistas'=>'Aquí podrá encontrar la hoja de vida, datos de contacto de los congresistas (Senadores y Representantes a la Cámara) y su actividad legislativa desde 1998.',
            'nuestraDemocracia'=>'En esta sección encontrará una descripción de ciertos aspectos relevantes de la democracia colombiana, en especial los relacionados con el Congreso de la República. Siga cada uno de los siguientes enlaces para comprender más el funcionamiento de la democracia en Colombia.',
            'actividadLegislativa'=>'En la Agenda Legislativa podrá encontrar todos los proyectos y citaciones agendadas en comisiones constitucionales y plenarias de Cámara o Senado, para las próximas sesiones.
            En Control Político podrá conocer toda la información acerca de las citaciones de control político, las invitaciones y las audiencias públicas. Y en votaciones podrá encontrar toda la informaci',
            'comisiones'=>'El trabajo legislativo del congreso se realiza en dos fases: La primera se desarrolla en comisiones especializadas por temas y la segunda en las plenarias, donde se aprueban, modifican o rechazan las iniciativas trabajadas en las plenarias. El Senado y la Cámara tienen tres tipos de comisiones: constitucionales permanentes, legales y especiales. Las comisiones const',
            'contenidoMultimedia'=>'En esta sección podrá encontrar toda la información multimedia que produce Congreso Visible. Videos, opiniones, podcasts, balances e informes legislativos en un solo lugar.',
            'proyectosDeLey'=>'Aquí podrá consultar las iniciativas de ley radicadas en el Congreso desde 1998, autores, ponentes y el estado de su trámite legislativo.',
            'datos'=>'La sección de datos presenta información detallada',
            'conflictoInteres' => 'Sección de conflictos de interés',
            'observacionesLegales'=>'Congreso Visible es un programa del Departamento de Ciencia Política de la Facultad de Ciencias Sociales de la Universidad de los Andes que hace seguimiento al Congreso de la República. 

            Universidad de los Andes 
            Vigilada Mineducación. Reconocimiento como Universidad: Decreto 1297 del 30 de mayo de 1964. Reconocimiento personería jurídica: Resolución 28 del 23 de febrero de 1949 Minjusticia.',
            'usercreated' =>'sysadmin@gmail.com'
            ]
        ];
        DB::table('informacion_sitios')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informacion_sitios');
    }
}
