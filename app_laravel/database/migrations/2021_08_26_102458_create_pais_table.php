<?php

use App\Models\Pais;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pais', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->string('iso', 2)->nullable();
            $table->string('nombre', 80)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        $this->setDataToTable();
    }

    /**
     * Set data to table.
     *
     * @return void
     */
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_paises.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $import_data = explode("|", $import_data[0]);

            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "iso" => $import_data[1] === 'NA' ? null : $import_data[1],
                "nombre" => $import_data[2] === 'NA' ? null : $import_data[2],
                "activo" => 1,
                "usercreated" => 'system',
                "created_at" => Carbon::now()
            ];

            Pais::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pais');
    }
}
