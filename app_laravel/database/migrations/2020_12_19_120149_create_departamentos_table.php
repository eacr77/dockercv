<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepartamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departamentos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->bigInteger('region_id')->unsigned()->nullable();
            $table->string('nombre', 50)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();

            $table->foreign('region_id')
                  ->references('id')
                  ->on('regions')
                  ->onDelete('cascade');
        });

        $data = [
            ['id'=>1,'nombre'=>'Distrito Capital','region_id'=>1,'activo'=>1,],
            ['id'=>2,'nombre'=>'Amazonas','region_id'=>6,'activo'=>1,],
            ['id'=>3,'nombre'=>'Antioquia','region_id'=>1,'activo'=>1,],
            ['id'=>4,'nombre'=>'Arauca','region_id'=>9,'activo'=>1,],
            ['id'=>5,'nombre'=>'Atlántico','region_id'=>4,'activo'=>1,],
            ['id'=>6,'nombre'=>'Bolívar','region_id'=>4,'activo'=>1,],
            ['id'=>7,'nombre'=>'Boyacá','region_id'=>1,'activo'=>1,],
            ['id'=>8,'nombre'=>'Caldas','region_id'=>1,'activo'=>1,],
            ['id'=>9,'nombre'=>'Caquetá','region_id'=>6,'activo'=>1,],
            ['id'=>10,'nombre'=>'Casanare','region_id'=>9,'activo'=>1,],
            ['id'=>11,'nombre'=>'Cauca','region_id'=>8,'activo'=>1,],
            ['id'=>12,'nombre'=>'Cesar','region_id'=>4,'activo'=>1,],
            ['id'=>13,'nombre'=>'Chocó','region_id'=>8,'activo'=>1,],
            ['id'=>14,'nombre'=>'Córdoba','region_id'=>4,'activo'=>1,],
            ['id'=>15,'nombre'=>'Cundinamarca','region_id'=>1,'activo'=>1,],
            ['id'=>16,'nombre'=>'Guainía','region_id'=>6,'activo'=>1,],
            ['id'=>17,'nombre'=>'Guaviare','region_id'=>6,'activo'=>1,],
            ['id'=>18,'nombre'=>'Huila','region_id'=>1,'activo'=>1,],
            ['id'=>19,'nombre'=>'La Guajira','region_id'=>4,'activo'=>1,],
            ['id'=>20,'nombre'=>'Magdalena','region_id'=>4,'activo'=>1,],
            ['id'=>22,'nombre'=>'Meta','region_id'=>9,'activo'=>1,],
            ['id'=>23,'nombre'=>'Nariño','region_id'=>8,'activo'=>1,],
            ['id'=>24,'nombre'=>'Norte de Santander','region_id'=>1,'activo'=>1,],
            ['id'=>25,'nombre'=>'Putumayo','region_id'=>6,'activo'=>1,],
            ['id'=>26,'nombre'=>'Quindío','region_id'=>1,'activo'=>1,],
            ['id'=>27,'nombre'=>'Risaralda','region_id'=>1,'activo'=>1,],
            ['id'=>28,'nombre'=>'San Andrés y Providencia','region_id'=>10,'activo'=>1,],
            ['id'=>29,'nombre'=>'Santander','region_id'=>1,'activo'=>1,],
            ['id'=>30,'nombre'=>'Sucre','region_id'=>4,'activo'=>1,],
            ['id'=>31,'nombre'=>'Tolima','region_id'=>1,'activo'=>1,],
            ['id'=>32,'nombre'=>'Valle del Cauca','region_id'=>1,'activo'=>1,],
            ['id'=>33,'nombre'=>'Vaupés','region_id'=>6,'activo'=>1,],
            ['id'=>34,'nombre'=>'Vichada','region_id'=>9,'activo'=>1,],
            ['id'=>36,'nombre'=>'Extranjero','region_id'=>3,'activo'=>1,],
            ['id'=>38,'nombre'=>'Comunidades Afro','region_id'=>5,'activo'=>1,],
            ['id'=>39,'nombre'=>'Comunidades Indígenas','region_id'=>5,'activo'=>1,],
            ['id'=>40,'nombre'=>'Minorías Políticas','region_id'=>5,'activo'=>1,],
        ];

        DB::table('departamentos')->insert($data);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('departamentos');
    }
}
