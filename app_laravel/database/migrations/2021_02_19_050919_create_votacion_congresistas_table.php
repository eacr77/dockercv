<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotacionCongresistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votacion_congresistas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->bigInteger('votacion_id')->nullable()->unsigned();
            $table->integer('congresista_id')->nullable()->unsigned();
            $table->tinyInteger('tipo_respuesta_votacion_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('votacion_congresistas', function (Blueprint $table) {
            $table->foreign('votacion_id')->references('id')->on('votacions')->onDelete('cascade');
            $table->foreign('congresista_id')->references('id')->on('congresistas')->onDelete('cascade');
            $table->foreign('tipo_respuesta_votacion_id')->references('id')->on('tipo_respuesta_votacions')->onDelete('cascade');
        });

        	}

	/**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votacion_congresistas');
    }
}