<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CreateCorporacionMiembrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corporacion_miembros', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('id')->autoIncrement();
            $table->smallInteger('corporacion_id')->unsigned()->nullable();
            $table->integer('congresista_id')->nullable()->unsigned();
            $table->tinyInteger('corporacion_cargo_congresista_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        Schema::table('corporacion_miembros', function (Blueprint $table) {
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
            $table->foreign('congresista_id')->references('id')->on('congresistas')->onDelete('cascade');
            // $table->foreign('corporacion_cargo_congresista_id')->references('id')->on('comision_cargo_congresistas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corporacion_miembros');
    }
}
