<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaseListasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pase_listas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->tinyInteger('tipo_pase_lista_id')->nullable()->unsigned();
            $table->smallInteger('legislatura_id')->nullable()->unsigned();
            $table->smallInteger('cuatrienio_id')->nullable()->unsigned();
            $table->smallInteger('corporacion_id')->nullable()->unsigned();
            $table->smallInteger('tipo_comision_id')->nullable()->unsigned();
            $table->integer('comision_id')->nullable()->unsigned();
            $table->dateTime('fecha');
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('pase_listas', function (Blueprint $table) {
            $table->foreign('tipo_pase_lista_id')->references('id')->on('tipo_pase_listas')->onDelete('cascade');
            $table->foreign('legislatura_id')->references('id')->on('legislaturas')->onDelete('cascade');
            $table->foreign('cuatrienio_id')->references('id')->on('cuatrienios')->onDelete('cascade');
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
            $table->foreign('tipo_comision_id')->references('id')->on('tipo_comisions')->onDelete('cascade');
            $table->foreign('comision_id')->references('id')->on('comisions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pase_listas');
    }
}
