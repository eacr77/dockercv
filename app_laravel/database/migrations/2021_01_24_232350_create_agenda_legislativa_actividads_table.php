<?php
use App\Models\AgendaLegislativaActividad;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaLegislativaActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_legislativa_actividads', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->bigInteger('agenda_legislativa_id')->unsigned();
            $table->string('titulo',250)->nullable();
            $table->boolean('destacado')->default(0)->nullable();
            $table->mediumText('descripcion')->nullable();
            $table->tinyInteger('tipo_actividad_id')->nullable()->unsigned();
            $table->bigInteger('proyecto_ley_id')->unsigned()->nullable();
            $table->bigInteger('control_politico_id')->unsigned()->nullable();
            $table->Integer('orden')->unsigned()->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->boolean('realizado')->default(0)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        Schema::table('agenda_legislativa_actividads', function (Blueprint $table) {
            $table->foreign('agenda_legislativa_id')->references('id')->on('agenda_legislativas')->onDelete('cascade');
            //$table->foreign('tipo_actividad_id')->references('id')->on('tipo_actividad_agenda_legislativas')->onDelete('cascade');
        });
        $this->setDataToTable();
    }

    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_agenda_legislativa_actividads.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file)) !== FALSE)
        {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map(
                "utf8_encode",
                $data
            ); //added
            if ($i === 0)
            {
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data)
        {

           /* $created_at = $import_data[10] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[10]
                );
            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[11] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[11]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;*/
            $insertData = [
                "id"                        => $import_data[0] === 'NA' ? null : $import_data[0],
                "titulo"                    => $import_data[1] === 'NA' ? null : $import_data[1],
                "destacado"                 => $import_data[2] === 'FALSE' ? 0: 1,
                "agenda_legislativa_id"     => $import_data[3] === 'NA' ? 0 : $import_data[3],
                "realizado"                 => $import_data[4] === 'FALSE' ? 0: 1,
                "descripcion"               => $import_data[5] === 'NA' ? null : $import_data[5],
                "tipo_actividad_id"         => $import_data[6] === 'NA' ? 0 : $import_data[6],
                "activo"                    => 1,
                "proyecto_ley_id"           => $import_data[12] === 'NA' ? null : $import_data[12],
                "control_politico_id"       => $import_data[13] === 'NA' ? null : $import_data[13],
                "orden"                     => $import_data[14] === 'NA' ? null : $import_data[14]
            ];
            AgendaLegislativaActividad::insert($insertData);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_legislativa_actividads');
    }
}
