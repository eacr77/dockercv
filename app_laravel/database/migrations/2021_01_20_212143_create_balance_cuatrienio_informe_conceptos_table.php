<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceCuatrienioInformeConceptosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_cuatrienio_informe_conceptos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->integer('balance_informe_id')->nullable()->unsigned();
            $table->integer('glosario_legislativo_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('balance_cuatrienio_informe_conceptos', function (Blueprint $table) {
            $table->foreign('balance_informe_id')->references('id')->on('balance_cuatrienio_informes')->onDelete('cascade');
            $table->foreign('glosario_legislativo_id', 'FK_gl_legislativo_informe_id')->references('id')->on('glosario_legislativos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_cuatrienio_informe_conceptos');
    }
}
