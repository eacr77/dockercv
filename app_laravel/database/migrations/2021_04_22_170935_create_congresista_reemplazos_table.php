<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistaReemplazosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresista_reemplazos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->bigInteger('persona_id_reemplazado')->nullable()->unsigned(); 
            $table->bigInteger('persona_id_reemplaza')->nullable()->unsigned(); 
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->integer('partido_id')->nullable()->unsigned(); 
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();                
            $table->timestamps();
        });

        Schema::table('congresista_reemplazos', function (Blueprint $table) {
            // $table->foreign('persona_id_reemplazado')->references('persona_id')->on('congresistas')->onDelete('cascade');
            $table->foreign('persona_id_reemplaza')->references('id')->on('personas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresista_reemplazos');
    }
}
