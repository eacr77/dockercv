<?php
use App\Models\AgendaLegislativaComision;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaLegislativaComisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_legislativa_comisions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->bigInteger('agenda_legislativa_id')->unsigned();
            $table->smallInteger('corporacion_id')->nullable()->unsigned();
            $table->integer('comision_id')->unsigned()->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        Schema::table('agenda_legislativa_comisions', function (Blueprint $table) {
            $table->foreign('agenda_legislativa_id')->references('id')->on('agenda_legislativas')->onDelete('cascade');
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
            $table->foreign('comision_id')->references('id')->on('comisions')->onDelete('cascade');
        });
        $this->setDataToTable();
    }
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_agenda_legislativa_comisions.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file)) !== FALSE)
        {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map(
                "utf8_encode",
                $data
            ); //added
            if ($i === 0)
            {
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data)
        {

            $created_at = $import_data[7] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[7]
                );
            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[8] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[8]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id"                        => $import_data[0] === 'NA' ? null : $import_data[0],
                "agenda_legislativa_id"     => $import_data[1] === 'NA' ? null : $import_data[1],
                "comision_id"               => $import_data[2] === 'NA' ? null : $import_data[2],
                "corporacion_id"            => $import_data[3] === 'NA' ? null : $import_data[3],
                "activo"                    => 1,
                "created_at"                => $created_at,
                "updated_at"                => $updated_at,
            ];
            AgendaLegislativaComision::insert($insertData);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_legislativa_comisions');
    }
}
