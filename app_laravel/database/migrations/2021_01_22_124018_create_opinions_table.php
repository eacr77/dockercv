<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpinionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titulo',250)->nullable();
            $table->smallInteger('equipo_id')->nullable()->unsigned();
            $table->smallInteger('tipo_publicacion_id')->nullable()->unsigned();
            $table->date('fechaPublicacion')->nullable();
            $table->text('resumen')->nullable();
            $table->text('opinion')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        Schema::table('opinions', function (Blueprint $table) {            
            $table->foreign('equipo_id')->references('id')->on('congreso_visible_equipos')->onDelete('cascade'); 
            $table->foreign('tipo_publicacion_id')->references('id')->on('tipo_publicacions')->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinions');
    }
}
