<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistasCfIngresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresistas_cf_ingresos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->integer('congresista_detalle_id')->nullable()->unsigned();
            $table->decimal('salario_anual', 13)->nullable();
            $table->decimal('interes_cesantias_anual', 13)->nullable();
            $table->decimal('gasto_representacion_anual', 13)->nullable();
            $table->decimal('arriendo_anual', 13)->nullable();
            $table->decimal('honorario_anual', 13)->nullable();
            $table->decimal('otros_anual', 13)->nullable();
            $table->decimal('total_anual', 13)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('congresistas_cf_ingresos', function (Blueprint $table) {
            $table->foreign('congresista_detalle_id')->references('id')->on('congresista_detalles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresistas_cf_ingresos');
    }
}
