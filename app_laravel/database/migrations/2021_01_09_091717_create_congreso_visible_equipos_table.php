<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresoVisibleEquiposTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congreso_visible_equipos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            // $table->increments('id');
            $table->smallInteger('id')->unsigned()->autoIncrement();
            $table->string('nombre',200)->nullable();
            $table->text('descripcion')->nullable();
            $table->integer('congreso_visible_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
        
        Schema::table('congreso_visible_equipos', function (Blueprint $table) {
            $table->foreign('congreso_visible_id')->references('id')->on('congreso_visibles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congreso_visible_equipos');
    }
}
