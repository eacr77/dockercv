<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistasCfDocumentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresistas_cf_documentos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->integer('congresista_detalle_id')->nullable()->unsigned();
            $table->integer('cf_tipo_documento_id')->nullable()->unsigned();
            $table->integer('year')->nullable();
            $table->string('url_documento', 350)->nullable();
            $table->string('nombre', 350)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('congresistas_cf_documentos', function (Blueprint $table) {
            $table->foreign('congresista_detalle_id')
                ->references('id')
                ->on('congresista_detalles')
                ->onDelete('cascade');

            $table->foreign('cf_tipo_documento_id', 'c_cf_docs_cf_t_dct_id_foreign')
                ->references('id')
                ->on('cf_tipo_documentos')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresistas_cf_documentos');
    }
}
