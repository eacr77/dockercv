<?php

    use App\Models\PersonaTrayectoriaPrivada;
    use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonaTrayectoriaPrivadasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona_trayectoria_privadas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('persona_id');
            $table->string('cargo', 1000)->nullable();
            $table->date('fecha')->nullable();
            $table->date('fecha_final')->nullable();
            $table->boolean('activo')->nullable()->default(1);
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();

            $table->foreign('persona_id')
                  ->references('id')
                  ->on('personas')
                  ->onDelete('cascade');
        });

        $this->setDataToTable();
    }

    /**
     * Set data to table.
     *
     * @return void
     */
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_persona_trayectoria_privadas.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $import_data = explode("|", $import_data[0]);

            $fecha = $import_data[3] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[3]
                );

            $fecha = $fecha
                ? $fecha->format('Y-m-d G:i')
                : null;

            $fecha_final = $import_data[4] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[4]
                );

            $fecha_final = $fecha_final
                ? $fecha_final->format('Y-m-d G:i')
                : null;

            $created_at = $import_data[8] === 'NA'
                ? null
                : date('Y-m-d G:i', strtotime($import_data[8])
                );


            $updated_at = $import_data[9] === 'NA'
                ? null
                : date('Y-m-d G:i', strtotime($import_data[9])
                );

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "persona_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "cargo"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "fecha"=>$fecha,
                "fecha_final"=>$fecha_final,
                "activo"=>$import_data[5] === 'NA' ? null : $import_data[5],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            PersonaTrayectoriaPrivada::insert($insertData);

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('persona_trayectoria_privadas');
    }
}
