<?php

use App\Models\Votacion;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVotacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votacions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->date('fecha')->nullable();
            $table->smallInteger('legislatura_id')->nullable()->unsigned();
            $table->smallInteger('cuatrienio_id')->nullable()->unsigned();
            $table->bigInteger('proyecto_de_ley_id')->nullable()->unsigned();
            $table->boolean('esPlenaria')->nullable();
            $table->boolean('esComision')->nullable();
            $table->string('urlGaceta', 350)->nullable();
            $table->string('motivo', 150)->nullable();
            $table->smallInteger('tipo_votacion_id')->nullable()->unsigned();
            $table->integer('votosFavor')->nullable()->unsigned();
            $table->integer('votosContra')->nullable()->unsigned();
            $table->string('acta', 35)->nullable();
            $table->text('observaciones')->nullable();
            $table->smallInteger('aprobada')->nullable()->unsigned();
            $table->integer('votosAbstencion')->nullable()->unsigned();
            $table->integer('numero_no_asistencias')->nullable()->unsigned();
            $table->smallInteger('clase_votacion_id')->nullable()->unsigned();
            $table->integer('numero_asistencias')->nullable()->unsigned();
            $table->boolean('voto_general')->default(1)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->text('datos_importacion')->nullable();
            $table->text('resultados_importacion')->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('votacions', function (Blueprint $table) {
            $table->foreign('legislatura_id')->references('id')->on('legislaturas')->onDelete('cascade');
            $table->foreign('cuatrienio_id')->references('id')->on('cuatrienios')->onDelete('cascade');
            $table->foreign('proyecto_de_ley_id')->references('id')->on('proyecto_leys')->onDelete('cascade');
            $table->foreign('tipo_votacion_id')->references('id')->on('tipo_votacions')->onDelete('cascade');
            $table->foreign('clase_votacion_id')->references('id')->on('clase_votacions')->onDelete('cascade');
        });

        $this->setDataToTable();
    }

    /**
     * Set data to table.
     *
     * @return void
     */
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_votacions.csv';

        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);

        // Reading file
        $file = fopen($filepath, "r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)

            $data = array_map("utf8_encode", $data); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $fecha = $import_data[1] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[1]
                );

            $fecha = $fecha
                ? $fecha->format('Y-m-d')
                : null;

            $created_at = $import_data[22] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[22]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[23] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[23]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "fecha" => $fecha,
                "legislatura_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "cuatrienio_id" => $import_data[3] === 'NA' ? null : $import_data[3],
                "proyecto_de_ley_id" => $import_data[4] === 'NA' ? null : $import_data[4],
                "urlGaceta" => $import_data[5] === 'NA' ? null : $import_data[5],
                "esPlenaria" => $import_data[6] === 'NA' ? null : $import_data[6],
                "esComision" => $import_data[7] === 'NA' ? null : $import_data[7],
                "motivo" => $import_data[8] === 'NA' ? null : $import_data[8],
                "tipo_votacion_id" => $import_data[9] === 'NA' ? null : $import_data[9],
                "votosFavor" => $import_data[10] === 'NA' ? null : $import_data[10],
                "votosContra" => $import_data[11] === 'NA' ? null : $import_data[11],
                "acta" => $import_data[12] === 'NA' ? null : $import_data[12],
                "observaciones" => $import_data[13] === 'NA' ? null : $import_data[13],
                "aprobada" => $import_data[14] === 'NA' ? null : $import_data[14],
                "votosAbstencion" => $import_data[15] === 'NA' ? null : $import_data[15],
                "numero_no_asistencias" => $import_data[16] === 'NA' ? null : $import_data[16],
                "clase_votacion_id" => $import_data[17] === 'NA' ? null : $import_data[17],
                "numero_asistencias" => $import_data[18] === 'NA' ? null : $import_data[18],
                "voto_general" => $import_data[24] === 'NA' ? 1 : $import_data[24],
                "activo" => $import_data[19] === 'NA' ? null : $import_data[19],
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];

            Votacion::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votacions');
    }
}
