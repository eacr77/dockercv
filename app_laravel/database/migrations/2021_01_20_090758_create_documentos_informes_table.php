<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentosInformesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos_informes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('titulo', 250)->nullable();
            $table->string('documento',350)->nullable();
            $table->string('nombre',200)->nullable();
            $table->integer('informes_pnud_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            // $table->string('informe', 200)->nullable();
            // $table->string('redactor', 4000)->nullable();
            $table->timestamps();
        });

        Schema::table('documentos_informes', function (Blueprint $table) {
            $table->foreign('informes_pnud_id')->references('id')->on('informes_pnuds')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documentos_informes');
    }
}
