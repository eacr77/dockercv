<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlPoliticoTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_politico_tags', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->bigInteger('control_politico_id')->nullable()->unsigned();
            $table->integer('glosario_legislativo_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('control_politico_tags', function (Blueprint $table) {
            $table->foreign('control_politico_id')->references('id')->on('control_politicos')->onDelete('cascade');
            $table->foreign('glosario_legislativo_id', 'FK_gl_legislativo_control_politico_id')->references('id')->on('glosario_legislativos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_politico_tags');
    }
}
