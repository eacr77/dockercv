<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceCuatrieniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_cuatrienios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->string('titulo', 200)->nullable();
            $table->year('yearInicio')->nullable();
            $table->year('yearFin')->nullable();
            $table->text('descripcion')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_cuatrienios');
    }
}
