<?php

use App\Models\ComisionMiembro;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComisionMiembrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comision_miembros', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->smallInteger('cuatrienio_id')->nullable()->unsigned();
            $table->integer('comision_id')->unsigned()->nullable();
            $table->integer('congresista_id')->nullable()->unsigned();
            $table->tinyInteger('cargo_legislativo_id_comision')->nullable()->unsigned();
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();

        });

        Schema::table('comision_miembros', function (Blueprint $table) {
            $table->foreign('comision_id')->references('id')->on('comisions')->onDelete('cascade');
            $table->foreign('congresista_id')->references('id')->on('congresistas')->onDelete('cascade');
            $table->foreign('cuatrienio_id')->references('id')->on('cuatrienios')->onDelete('cascade');
            $table->foreign('cargo_legislativo_id_comision')->references('id')->on('cargo_legislativos')->onDelete('cascade');
        });

        //$this->setDataToTable();
    }

    /**
     * Set data to table.
     *
     * @return void
     */
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_comision_miembros.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $fecha_inicio = $import_data[5] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[5]
                );

            $fecha_inicio = $fecha_inicio
                ? $fecha_inicio->format('Y-m-d')
                : null;

            $fecha_fin = $import_data[6] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[6]
                );

            $fecha_fin = $fecha_fin
                ? $fecha_fin->format('Y-m-d')
                : null;

            $created_at = $import_data[10] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[10]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[11] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[11]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "cuatrienio_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "comision_id"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "congresista_id"=>$import_data[3] === 'NA' ? null : $import_data[3],
                "cargo_legislativo_id_comision"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "fecha_inicio"=>$fecha_inicio,
                "fecha_fin"=>$fecha_fin,
                "activo"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            ComisionMiembro::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comision_miembros');
    }
}
