<?php
 use App\Models\AgendaLegislativa;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgendaLegislativasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agenda_legislativas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->dateTime('fecha');
            $table->date('fecha_realizada')->nullable();
            $table->string('comentarios', 2500)->nullable();
            $table->Integer('cuatrienio_id')->nullable()->unsigned();
            $table->boolean('realizado')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });
       $this->setDataToTable();
    }

    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_agenda_legislativas.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file)) !== FALSE)
        {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map(
                "utf8_encode",
                $data
            ); //added
            if ($i === 0)
            {
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data)
        {
            $fecha = $import_data[1] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y H:i',
                    $import_data[1]
                );

            $fecha = $fecha ? $fecha->format('Y-m-d H:i:s') : null;

            $fecha_realizada = $import_data[2] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[2]
                );

            $fecha_realizada = $fecha_realizada ? $fecha_realizada->format('Y-m-d H:i:s') : null;

            $created_at = $import_data[9] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[9]
                );

            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[10] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[10]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id"                        => $import_data[0] === 'NA' ? null : $import_data[0],
                "fecha"                     => $fecha,
                "fecha_realizada"           => $fecha_realizada,
                "comentarios"               => $import_data[3] === 'NA' ? null : $import_data[3],
                "cuatrienio_id"             => $import_data[4] === 'NA' ? null : $import_data[4],
                "realizado"                 => $import_data[5] === 'NA' ? 0 : $import_data[5],
                "activo"                    => 1,
                "created_at"                => $created_at,
                "updated_at"                => $updated_at,
            ];
            AgendaLegislativa::insert($insertData);
        }
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agenda_legislativas');
    }
}
