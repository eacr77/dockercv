<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePodcastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('podcasts', function (Blueprint $table) {
            $table->engine = 'InnoDB';        
            $table->increments('id');
            $table->string('titulo', 250)->nullable();
            $table->string('presentadores', 300)->nullable();
            $table->string('invitados', 500)->nullable();
            $table->date('fecha')->nullable();
            $table->text('resumen')->nullable();
            $table->string('urlAudio', 350)->nullable();
            $table->boolean('esEnlace')->default(1)->nullable();
            $table->string('urlExterno', 350)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('podcasts');
    }
}
