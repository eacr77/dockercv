<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateClaseVotacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clase_votacions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallInteger('id')->unsigned()->autoIncrement();
            $table->string('nombre', 50)->nullable();
            $table->string('descripcion', 150)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        $data = [
            ['id' => 1, 'nombre' => 'Nominal', 'descripcion' => '', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 2, 'nombre' => 'Ordinaria', 'descripcion' => '', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 3, 'nombre' => 'Secreta', 'descripcion' => '', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()]
        ];
        DB::table('clase_votacions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clase_votacions');
    }
}
