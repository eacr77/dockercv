<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistasCfJuntasAsociacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresistas_cf_juntas_asociaciones', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->integer('congresista_detalle_id')->nullable()->unsigned();
            $table->string('nombre_entidad', 50)->nullable();
            $table->integer('cf_organo_id')->nullable()->unsigned();
            $table->integer('cf_calidad_miembro_id')->nullable()->unsigned();
            $table->integer('pais_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('congresistas_cf_juntas_asociaciones', function (Blueprint $table) {
            $table->foreign('congresista_detalle_id', 'c_juntas_detalle')->references('id')->on('congresista_detalles')->onDelete('cascade');
            $table->foreign('cf_organo_id', 'organo_id_foreign')->references('id')->on('cf_organos')->onDelete('cascade');
            $table->foreign('cf_calidad_miembro_id', 'calidad_miembro_foreign')->references('id')->on('cf_calidad_miembros')->onDelete('cascade');
            $table->foreign('pais_id')->references('id')->on('pais')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresistas_cf_juntas_asociaciones');
    }
}
