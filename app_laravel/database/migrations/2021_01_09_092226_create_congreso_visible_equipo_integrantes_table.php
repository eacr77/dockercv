<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresoVisibleEquipoIntegrantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congreso_visible_equipo_integrantes', function (Blueprint $table) {
            $table->engine = 'InnoDB';            
            $table->increments('id');
            $table->string('nombre',200)->nullable();
            $table->text('descripcion')->nullable();
            $table->smallInteger('equipo_id')->nullable()->unsigned();            
            $table->tinyInteger('cargo_id')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('congreso_visible_equipo_integrantes', function (Blueprint $table) {
            $table->foreign('equipo_id')->references('id')->on('congreso_visible_equipos')->onDelete('cascade');
            // $table->foreign('cargo_id')->references('id')->on('comision_cargo_congresistas')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congreso_visible_equipo_integrantes');
    }
}
