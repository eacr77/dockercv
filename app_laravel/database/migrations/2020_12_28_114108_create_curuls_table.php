<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateCurulsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curuls', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallInteger('id')->unsigned()->autoIncrement();
            $table->smallInteger('corporacion_id')->unsigned();
            $table->string('cx', 15)->nullable();
            $table->string('cy', 15)->nullable();
            $table->string('r', 15)->nullable();
            $table->string('seccionAsiento', 7)->nullable();
            $table->string('filaAsiento', 5)->nullable();
            $table->string('asiento', 5)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable(); 
            $table->timestamps();
        });

        Schema::table('curuls', function (Blueprint $table) {
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
        });

        // Izquierda
        $data = [
            ['id' => 1, 'corporacion_id' => 1, 'cx' => '325.97', 'cy' => '577.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'K', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 2, 'corporacion_id' => 1, 'cx' => '296.97', 'cy' => '580.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'K', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 3, 'corporacion_id' => 1, 'cx' => '269.97', 'cy' => '585.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'K', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 4, 'corporacion_id' => 1, 'cx' => '243.97', 'cy' => '590.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'K', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 5, 'corporacion_id' => 1, 'cx' => '215.97', 'cy' => '597.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'K', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 6, 'corporacion_id' => 1, 'cx' => '189.38', 'cy' => '605.43', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'K', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 7, 'corporacion_id' => 1, 'cx' => '327.97', 'cy' => '526.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 8, 'corporacion_id' => 1, 'cx' => '298.97', 'cy' => '529.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 9, 'corporacion_id' => 1, 'cx' => '271.97', 'cy' => '534.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 10, 'corporacion_id' => 1, 'cx' => '245.97', 'cy' => '539.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 11, 'corporacion_id' => 1, 'cx' => '217.97', 'cy' => '546.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 12, 'corporacion_id' => 1, 'cx' => '191.38', 'cy' => '554.43', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 13, 'corporacion_id' => 1, 'cx' => '163.77', 'cy' => '563.79', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 14, 'corporacion_id' => 1, 'cx' => '138.55', 'cy' => '574.67', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 15, 'corporacion_id' => 1, 'cx' => '114.31', 'cy' => '585.32', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'J', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 16, 'corporacion_id' => 1, 'cx' => '327.97', 'cy' => '477.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 17, 'corporacion_id' => 1, 'cx' => '298.97', 'cy' => '480.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 18, 'corporacion_id' => 1, 'cx' => '271.97', 'cy' => '485.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 19, 'corporacion_id' => 1, 'cx' => '245.97', 'cy' => '490.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 20, 'corporacion_id' => 1, 'cx' => '217.97', 'cy' => '497.39', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 21, 'corporacion_id' => 1, 'cx' => '191.38', 'cy' => '505.43', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 22, 'corporacion_id' => 1, 'cx' => '163.77', 'cy' => '514.79', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 23, 'corporacion_id' => 1, 'cx' => '138.55', 'cy' => '525.67', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 24, 'corporacion_id' => 1, 'cx' => '114.31', 'cy' => '536.32', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 25, 'corporacion_id' => 1, 'cx' => '88.57', 'cy' => '549.36', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'I', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 26, 'corporacion_id' => 1, 'cx' => '327.97', 'cy' => '421.86', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 27, 'corporacion_id' => 1, 'cx' => '298.97', 'cy' => '424.86', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 28, 'corporacion_id' => 1, 'cx' => '271.97', 'cy' => '429.86', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 29, 'corporacion_id' => 1, 'cx' => '245.97', 'cy' => '434.86', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 30, 'corporacion_id' => 1, 'cx' => '217.97', 'cy' => '441.86', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 31, 'corporacion_id' => 1, 'cx' => '191.38', 'cy' => '449.9', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 32, 'corporacion_id' => 1, 'cx' => '163.77', 'cy' => '459.26', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 33, 'corporacion_id' => 1, 'cx' => '138.55', 'cy' => '470.14', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 34, 'corporacion_id' => 1, 'cx' => '114.31', 'cy' => '480.79', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 35, 'corporacion_id' => 1, 'cx' => '88.57', 'cy' => '493.83', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 36, 'corporacion_id' => 1, 'cx' => '64.57', 'cy' => '507.83', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'H', 'asiento' => '11', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 37, 'corporacion_id' => 1, 'cx' => '327.74', 'cy' => '372.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 38, 'corporacion_id' => 1, 'cx' => '298.74', 'cy' => '375.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 39, 'corporacion_id' => 1, 'cx' => '271.74', 'cy' => '380.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 40, 'corporacion_id' => 1, 'cx' => '245.74', 'cy' => '385.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 41, 'corporacion_id' => 1, 'cx' => '217.74', 'cy' => '392.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 42, 'corporacion_id' => 1, 'cx' => '191.15', 'cy' => '400.33', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 43, 'corporacion_id' => 1, 'cx' => '163.54', 'cy' => '409.69', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 44, 'corporacion_id' => 1, 'cx' => '138.32', 'cy' => '420.56', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 45, 'corporacion_id' => 1, 'cx' => '114.08', 'cy' => '431.22', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 46, 'corporacion_id' => 1, 'cx' => '88.34', 'cy' => '444.26', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 47, 'corporacion_id' => 1, 'cx' => '64.34', 'cy' => '458.26', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'G', 'asiento' => '11', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 48, 'corporacion_id' => 1, 'cx' => '326.74', 'cy' => '325.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 49, 'corporacion_id' => 1, 'cx' => '297.74', 'cy' => '328.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 50, 'corporacion_id' => 1, 'cx' => '270.74', 'cy' => '333.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 51, 'corporacion_id' => 1, 'cx' => '244.74', 'cy' => '338.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 52, 'corporacion_id' => 1, 'cx' => '216.74', 'cy' => '345.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 53, 'corporacion_id' => 1, 'cx' => '190.15', 'cy' => '353.33', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 54, 'corporacion_id' => 1, 'cx' => '162.54', 'cy' => '362.69', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 55, 'corporacion_id' => 1, 'cx' => '137.32', 'cy' => '373.56', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 56, 'corporacion_id' => 1, 'cx' => '113.08', 'cy' => '384.22', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 57, 'corporacion_id' => 1, 'cx' => '87.34', 'cy' => '397.26', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 58, 'corporacion_id' => 1, 'cx' => '327.74', 'cy' => '271.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 59, 'corporacion_id' => 1, 'cx' => '298.74', 'cy' => '274.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 60, 'corporacion_id' => 1, 'cx' => '271.74', 'cy' => '279.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 61, 'corporacion_id' => 1, 'cx' => '245.74', 'cy' => '284.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 62, 'corporacion_id' => 1, 'cx' => '217.74', 'cy' => '291.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 63, 'corporacion_id' => 1, 'cx' => '191.15', 'cy' => '299.33', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 64, 'corporacion_id' => 1, 'cx' => '163.54', 'cy' => '308.69', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 65, 'corporacion_id' => 1, 'cx' => '138.32', 'cy' => '319.56', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 66, 'corporacion_id' => 1, 'cx' => '114.08', 'cy' => '330.22', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 67, 'corporacion_id' => 1, 'cx' => '88.34', 'cy' => '343.26', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 68, 'corporacion_id' => 1, 'cx' => '327.74', 'cy' => '219.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 69, 'corporacion_id' => 1, 'cx' => '298.74', 'cy' => '222.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 70, 'corporacion_id' => 1, 'cx' => '271.74', 'cy' => '227.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 71, 'corporacion_id' => 1, 'cx' => '245.74', 'cy' => '232.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 72, 'corporacion_id' => 1, 'cx' => '217.74', 'cy' => '239.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 73, 'corporacion_id' => 1, 'cx' => '191.15', 'cy' => '247.33', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 74, 'corporacion_id' => 1, 'cx' => '163.54', 'cy' => '256.69', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 75, 'corporacion_id' => 1, 'cx' => '138.32', 'cy' => '267.56', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 76, 'corporacion_id' => 1, 'cx' => '114.08', 'cy' => '278.22', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 77, 'corporacion_id' => 1, 'cx' => '88.34', 'cy' => '291.26', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 78, 'corporacion_id' => 1, 'cx' => '328.74', 'cy' => '170.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 79, 'corporacion_id' => 1, 'cx' => '299.74', 'cy' => '173.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 80, 'corporacion_id' => 1, 'cx' => '272.74', 'cy' => '178.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 81, 'corporacion_id' => 1, 'cx' => '246.74', 'cy' => '183.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 82, 'corporacion_id' => 1, 'cx' => '218.74', 'cy' => '190.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 83, 'corporacion_id' => 1, 'cx' => '328.74', 'cy' => '123.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 84, 'corporacion_id' => 1, 'cx' => '299.74', 'cy' => '127.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 85, 'corporacion_id' => 1, 'cx' => '328.74', 'cy' => '68.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 86, 'corporacion_id' => 1, 'cx' => '299.74', 'cy' => '72.29', 'r' => '10.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            // End izquierda

            // Derecha

            ['id' => 87, 'corporacion_id' => 1, 'cx' => '407.15', 'cy' => '577.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'K', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 88, 'corporacion_id' => 1, 'cx' => '436.15', 'cy' => '580.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'K', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 89, 'corporacion_id' => 1, 'cx' => '463.15', 'cy' => '585.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'K', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 90, 'corporacion_id' => 1, 'cx' => '489.15', 'cy' => '590.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'K', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 91, 'corporacion_id' => 1, 'cx' => '517.15', 'cy' => '597.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'K', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 92, 'corporacion_id' => 1, 'cx' => '543.74', 'cy' => '605.43', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'K', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 93, 'corporacion_id' => 1, 'cx' => '405.15', 'cy' => '526.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 94, 'corporacion_id' => 1, 'cx' => '434.15', 'cy' => '529.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 95, 'corporacion_id' => 1, 'cx' => '461.15', 'cy' => '534.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 96, 'corporacion_id' => 1, 'cx' => '487.15', 'cy' => '539.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 97, 'corporacion_id' => 1, 'cx' => '515.15', 'cy' => '546.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 98, 'corporacion_id' => 1, 'cx' => '541.74', 'cy' => '554.43', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 99, 'corporacion_id' => 1, 'cx' => '569.35', 'cy' => '563.79', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 100, 'corporacion_id' => 1, 'cx' => '594.57', 'cy' => '574.67', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 101, 'corporacion_id' => 1, 'cx' => '618.81', 'cy' => '585.32', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'J', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 102, 'corporacion_id' => 1, 'cx' => '405.15', 'cy' => '477.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 103, 'corporacion_id' => 1, 'cx' => '434.15', 'cy' => '480.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 104, 'corporacion_id' => 1, 'cx' => '461.15', 'cy' => '485.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 105, 'corporacion_id' => 1, 'cx' => '487.15', 'cy' => '490.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 106, 'corporacion_id' => 1, 'cx' => '515.15', 'cy' => '497.39', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 107, 'corporacion_id' => 1, 'cx' => '541.74', 'cy' => '505.43', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 108, 'corporacion_id' => 1, 'cx' => '569.35', 'cy' => '514.79', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 109, 'corporacion_id' => 1, 'cx' => '594.57', 'cy' => '525.67', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 110, 'corporacion_id' => 1, 'cx' => '618.81', 'cy' => '536.32', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 111, 'corporacion_id' => 1, 'cx' => '644.55', 'cy' => '549.36', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'I', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 112, 'corporacion_id' => 1, 'cx' => '405.15', 'cy' => '421.86', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 113, 'corporacion_id' => 1, 'cx' => '434.15', 'cy' => '424.86', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 114, 'corporacion_id' => 1, 'cx' => '461.15', 'cy' => '429.86', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 115, 'corporacion_id' => 1, 'cx' => '487.15', 'cy' => '434.86', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 116, 'corporacion_id' => 1, 'cx' => '515.15', 'cy' => '441.86', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 117, 'corporacion_id' => 1, 'cx' => '541.74', 'cy' => '449.9', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 118, 'corporacion_id' => 1, 'cx' => '569.35', 'cy' => '459.26', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 119, 'corporacion_id' => 1, 'cx' => '594.57', 'cy' => '470.14', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 120, 'corporacion_id' => 1, 'cx' => '618.81', 'cy' => '480.79', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 121, 'corporacion_id' => 1, 'cx' => '644.55', 'cy' => '493.83', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 122, 'corporacion_id' => 1, 'cx' => '668.55', 'cy' => '507.83', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'H', 'asiento' => '11', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 123, 'corporacion_id' => 1, 'cx' => '405.38', 'cy' => '372.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 124, 'corporacion_id' => 1, 'cx' => '434.38', 'cy' => '375.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 125, 'corporacion_id' => 1, 'cx' => '461.38', 'cy' => '380.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 126, 'corporacion_id' => 1, 'cx' => '487.38', 'cy' => '385.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 127, 'corporacion_id' => 1, 'cx' => '515.38', 'cy' => '392.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 128, 'corporacion_id' => 1, 'cx' => '541.97', 'cy' => '400.33', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 129, 'corporacion_id' => 1, 'cx' => '569.58', 'cy' => '409.69', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 130, 'corporacion_id' => 1, 'cx' => '594.8', 'cy' => '420.56', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 131, 'corporacion_id' => 1, 'cx' => '619.04', 'cy' => '431.22', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 132, 'corporacion_id' => 1, 'cx' => '644.78', 'cy' => '444.26', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 133, 'corporacion_id' => 1, 'cx' => '668.78', 'cy' => '458.26', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '11', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 134, 'corporacion_id' => 1, 'cx' => '406.38', 'cy' => '325.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 135, 'corporacion_id' => 1, 'cx' => '435.38', 'cy' => '328.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 136, 'corporacion_id' => 1, 'cx' => '462.38', 'cy' => '333.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 137, 'corporacion_id' => 1, 'cx' => '488.38', 'cy' => '338.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 138, 'corporacion_id' => 1, 'cx' => '516.38', 'cy' => '345.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 139, 'corporacion_id' => 1, 'cx' => '542.97', 'cy' => '353.33', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 140, 'corporacion_id' => 1, 'cx' => '570.58', 'cy' => '362.69', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 141, 'corporacion_id' => 1, 'cx' => '595.8', 'cy' => '373.56', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 142, 'corporacion_id' => 1, 'cx' => '620.04', 'cy' => '384.22', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 143, 'corporacion_id' => 1, 'cx' => '645.78', 'cy' => '397.26', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 144, 'corporacion_id' => 1, 'cx' => '405.38', 'cy' => '271.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 145, 'corporacion_id' => 1, 'cx' => '434.38', 'cy' => '274.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 146, 'corporacion_id' => 1, 'cx' => '461.38', 'cy' => '279.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 147, 'corporacion_id' => 1, 'cx' => '487.38', 'cy' => '284.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 148, 'corporacion_id' => 1, 'cx' => '515.38', 'cy' => '291.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 149, 'corporacion_id' => 1, 'cx' => '541.97', 'cy' => '299.33', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 150, 'corporacion_id' => 1, 'cx' => '569.58', 'cy' => '308.69', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 151, 'corporacion_id' => 1, 'cx' => '594.8', 'cy' => '319.56', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 152, 'corporacion_id' => 1, 'cx' => '619.04', 'cy' => '330.22', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 153, 'corporacion_id' => 1, 'cx' => '644.78', 'cy' => '343.26', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 154, 'corporacion_id' => 1, 'cx' => '405.38', 'cy' => '219.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 155, 'corporacion_id' => 1, 'cx' => '434.38', 'cy' => '222.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 156, 'corporacion_id' => 1, 'cx' => '461.38', 'cy' => '227.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 157, 'corporacion_id' => 1, 'cx' => '487.38', 'cy' => '232.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 158, 'corporacion_id' => 1, 'cx' => '515.38', 'cy' => '239.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 159, 'corporacion_id' => 1, 'cx' => '541.97', 'cy' => '247.33', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 160, 'corporacion_id' => 1, 'cx' => '569.58', 'cy' => '256.69', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 161, 'corporacion_id' => 1, 'cx' => '594.8', 'cy' => '267.56', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 162, 'corporacion_id' => 1, 'cx' => '619.04', 'cy' => '278.22', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 163, 'corporacion_id' => 1, 'cx' => '644.78', 'cy' => '291.26', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 164, 'corporacion_id' => 1, 'cx' => '404.38', 'cy' => '170.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 165, 'corporacion_id' => 1, 'cx' => '433.38', 'cy' => '173.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 166, 'corporacion_id' => 1, 'cx' => '460.38', 'cy' => '178.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 167, 'corporacion_id' => 1, 'cx' => '486.38', 'cy' => '183.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 168, 'corporacion_id' => 1, 'cx' => '514.38', 'cy' => '190.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 169, 'corporacion_id' => 1, 'cx' => '404.38', 'cy' => '123.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 170, 'corporacion_id' => 1, 'cx' => '433.38', 'cy' => '127.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 171, 'corporacion_id' => 1, 'cx' => '406.38', 'cy' => '66.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 172, 'corporacion_id' => 1, 'cx' => '435.38', 'cy' => '70.29', 'r' => '10.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            // End derecha

            // Semitop
            ['id' => 173, 'corporacion_id' => 1, 'cx' => '333.05', 'cy' => '668.77', 'r' => '10.64', 'seccionAsiento' => 'Semitop', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 174, 'corporacion_id' => 1, 'cx' => '365.05', 'cy' => '668.77', 'r' => '10.64', 'seccionAsiento' => 'Semitop', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 175, 'corporacion_id' => 1, 'cx' => '397.05', 'cy' => '668.77', 'r' => '10.64', 'seccionAsiento' => 'Semitop', 'filaAsiento' => 'A', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            // End semitop

            // Top
            ['id' => 176, 'corporacion_id' => 1, 'cx' => '316.31', 'cy' => '713.21', 'r' => '16.21', 'seccionAsiento' => 'Top', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 177, 'corporacion_id' => 1, 'cx' => '365.05', 'cy' => '713.21', 'r' => '16.21', 'seccionAsiento' => 'Top', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 178, 'corporacion_id' => 1, 'cx' => '413.79', 'cy' => '713.21', 'r' => '16.21', 'seccionAsiento' => 'Top', 'filaAsiento' => 'A', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            // End top

            // Senado curules

            // Right senado
            ['id' => 179, 'corporacion_id' => 2, 'cx' => '458', 'cy' => '114.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 180, 'corporacion_id' => 2, 'cx' => '432', 'cy' => '110.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 181, 'corporacion_id' => 2, 'cx' => '406', 'cy' => '107.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'G', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 182, 'corporacion_id' => 2, 'cx' => '571', 'cy' => '202.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 183, 'corporacion_id' => 2, 'cx' => '543', 'cy' => '192.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 184, 'corporacion_id' => 2, 'cx' => '516', 'cy' => '184.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 185, 'corporacion_id' => 2, 'cx' => '488', 'cy' => '177.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 186, 'corporacion_id' => 2, 'cx' => '461', 'cy' => '172.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 187, 'corporacion_id' => 2, 'cx' => '435', 'cy' => '167.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 188, 'corporacion_id' => 2, 'cx' => '409', 'cy' => '164.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 189, 'corporacion_id' => 2, 'cx' => '384', 'cy' => '162.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'F', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 190, 'corporacion_id' => 2, 'cx' => '653', 'cy' => '295.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 191, 'corporacion_id' => 2, 'cx' => '623', 'cy' => '282.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 192, 'corporacion_id' => 2, 'cx' => '592', 'cy' => '270.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 193, 'corporacion_id' => 2, 'cx' => '547', 'cy' => '253.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 194, 'corporacion_id' => 2, 'cx' => '520', 'cy' => '244.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 195, 'corporacion_id' => 2, 'cx' => '491', 'cy' => '237.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 196, 'corporacion_id' => 2, 'cx' => '464', 'cy' => '232.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 197, 'corporacion_id' => 2, 'cx' => '436', 'cy' => '228.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 198, 'corporacion_id' => 2, 'cx' => '410', 'cy' => '226.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 199, 'corporacion_id' => 2, 'cx' => '385', 'cy' => '225.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'E', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 200, 'corporacion_id' => 2, 'cx' => '634', 'cy' => '348.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 201, 'corporacion_id' => 2, 'cx' => '604', 'cy' => '336.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 202, 'corporacion_id' => 2, 'cx' => '573', 'cy' => '326.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 203, 'corporacion_id' => 2, 'cx' => '529', 'cy' => '305.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 204, 'corporacion_id' => 2, 'cx' => '501', 'cy' => '297.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 205, 'corporacion_id' => 2, 'cx' => '473', 'cy' => '291.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 206, 'corporacion_id' => 2, 'cx' => '445', 'cy' => '287.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 207, 'corporacion_id' => 2, 'cx' => '418', 'cy' => '283.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 208, 'corporacion_id' => 2, 'cx' => '391', 'cy' => '282.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'D', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 209, 'corporacion_id' => 2, 'cx' => '643', 'cy' => '416.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 210, 'corporacion_id' => 2, 'cx' => '615', 'cy' => '403.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 211, 'corporacion_id' => 2, 'cx' => '585', 'cy' => '391.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 212, 'corporacion_id' => 2, 'cx' => '554', 'cy' => '381.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 213, 'corporacion_id' => 2, 'cx' => '505', 'cy' => '359.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 214, 'corporacion_id' => 2, 'cx' => '478', 'cy' => '353.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 215, 'corporacion_id' => 2, 'cx' => '449', 'cy' => '348.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 216, 'corporacion_id' => 2, 'cx' => '421', 'cy' => '345.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 217, 'corporacion_id' => 2, 'cx' => '394', 'cy' => '344.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'C', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 218, 'corporacion_id' => 2, 'cx' => '625', 'cy' => '471.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 219, 'corporacion_id' => 2, 'cx' => '597', 'cy' => '458.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 220, 'corporacion_id' => 2, 'cx' => '567', 'cy' => '446.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 221, 'corporacion_id' => 2, 'cx' => '536', 'cy' => '436.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 222, 'corporacion_id' => 2, 'cx' => '481', 'cy' => '418.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 223, 'corporacion_id' => 2, 'cx' => '454', 'cy' => '412.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 224, 'corporacion_id' => 2, 'cx' => '425', 'cy' => '408.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 225, 'corporacion_id' => 2, 'cx' => '397', 'cy' => '406.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'B', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 226, 'corporacion_id' => 2, 'cx' => '608', 'cy' => '526.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 227, 'corporacion_id' => 2, 'cx' => '580', 'cy' => '513.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 228, 'corporacion_id' => 2, 'cx' => '550', 'cy' => '501.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 229, 'corporacion_id' => 2, 'cx' => '519', 'cy' => '491.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 230, 'corporacion_id' => 2, 'cx' => '466', 'cy' => '477.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 231, 'corporacion_id' => 2, 'cx' => '436', 'cy' => '471.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 232, 'corporacion_id' => 2, 'cx' => '406', 'cy' => '467.86', 'r' => '9.64', 'seccionAsiento' => 'Right', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            // Left senado
            ['id' => 233, 'corporacion_id' => 2, 'cx' => '276', 'cy' => '115.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 234, 'corporacion_id' => 2, 'cx' => '302', 'cy' => '111.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 235, 'corporacion_id' => 2, 'cx' => '328', 'cy' => '108.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 236, 'corporacion_id' => 2, 'cx' => '165', 'cy' => '204.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 237, 'corporacion_id' => 2, 'cx' => '193', 'cy' => '195.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 238, 'corporacion_id' => 2, 'cx' => '220', 'cy' => '186.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 239, 'corporacion_id' => 2, 'cx' => '247', 'cy' => '179.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 240, 'corporacion_id' => 2, 'cx' => '273', 'cy' => '172.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 241, 'corporacion_id' => 2, 'cx' => '300', 'cy' => '167.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'F', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 242, 'corporacion_id' => 2, 'cx' => '326', 'cy' => '164.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 243, 'corporacion_id' => 2, 'cx' => '352', 'cy' => '162.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 244, 'corporacion_id' => 2, 'cx' => '80', 'cy' => '300.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '10', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 245, 'corporacion_id' => 2, 'cx' => '110', 'cy' => '287.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 246, 'corporacion_id' => 2, 'cx' => '141', 'cy' => '275.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 247, 'corporacion_id' => 2, 'cx' => '193', 'cy' => '255.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 248, 'corporacion_id' => 2, 'cx' => '219', 'cy' => '247.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 249, 'corporacion_id' => 2, 'cx' => '245', 'cy' => '240.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 250, 'corporacion_id' => 2, 'cx' => '271', 'cy' => '234.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'E', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 251, 'corporacion_id' => 2, 'cx' => '299', 'cy' => '229.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 252, 'corporacion_id' => 2, 'cx' => '325', 'cy' => '226.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 253, 'corporacion_id' => 2, 'cx' => '351', 'cy' => '225.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 254, 'corporacion_id' => 2, 'cx' => '99', 'cy' => '352.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 255, 'corporacion_id' => 2, 'cx' => '129', 'cy' => '339.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 256, 'corporacion_id' => 2, 'cx' => '160', 'cy' => '327.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 257, 'corporacion_id' => 2, 'cx' => '213', 'cy' => '307.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 258, 'corporacion_id' => 2, 'cx' => '240', 'cy' => '299.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 259, 'corporacion_id' => 2, 'cx' => '267', 'cy' => '293.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'D', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 260, 'corporacion_id' => 2, 'cx' => '294', 'cy' => '288.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 261, 'corporacion_id' => 2, 'cx' => '322', 'cy' => '284.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 262, 'corporacion_id' => 2, 'cx' => '350', 'cy' => '282.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 263, 'corporacion_id' => 2, 'cx' => '90', 'cy' => '419.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '9', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 264, 'corporacion_id' => 2, 'cx' => '118', 'cy' => '404.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '8', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 265, 'corporacion_id' => 2, 'cx' => '148', 'cy' => '392.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 266, 'corporacion_id' => 2, 'cx' => '179', 'cy' => '380.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 267, 'corporacion_id' => 2, 'cx' => '237', 'cy' => '357.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 268, 'corporacion_id' => 2, 'cx' => '266', 'cy' => '352.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'C', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 269, 'corporacion_id' => 2, 'cx' => '294', 'cy' => '348.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 270, 'corporacion_id' => 2, 'cx' => '322', 'cy' => '345.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 271, 'corporacion_id' => 2, 'cx' => '348', 'cy' => '343.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 272, 'corporacion_id' => 2, 'cx' => '139', 'cy' => '457.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '7', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 273, 'corporacion_id' => 2, 'cx' => '169', 'cy' => '445.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 274, 'corporacion_id' => 2, 'cx' => '200', 'cy' => '435.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 275, 'corporacion_id' => 2, 'cx' => '261', 'cy' => '417.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 276, 'corporacion_id' => 2, 'cx' => '290', 'cy' => '412.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 277, 'corporacion_id' => 2, 'cx' => '318', 'cy' => '408.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'B', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 278, 'corporacion_id' => 2, 'cx' => '346', 'cy' => '406.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            ['id' => 279, 'corporacion_id' => 2, 'cx' => '162', 'cy' => '513.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '6', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 280, 'corporacion_id' => 2, 'cx' => '192', 'cy' => '501.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '5', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 281, 'corporacion_id' => 2, 'cx' => '223', 'cy' => '491.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '4', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 282, 'corporacion_id' => 2, 'cx' => '273', 'cy' => '480.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 283, 'corporacion_id' => 2, 'cx' => '303', 'cy' => '473.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 284, 'corporacion_id' => 2, 'cx' => '333', 'cy' => '470.86', 'r' => '9.64', 'seccionAsiento' => 'Left', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            // semitop
            ['id' => 285, 'corporacion_id' => 2, 'cx' => '340', 'cy' => '567.86', 'r' => '9.64', 'seccionAsiento' => 'Semitop', 'filaAsiento' => 'A', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 286, 'corporacion_id' => 2, 'cx' => '373', 'cy' => '567.86', 'r' => '9.64', 'seccionAsiento' => 'Semitop', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 287, 'corporacion_id' => 2, 'cx' => '406', 'cy' => '567.86', 'r' => '9.64', 'seccionAsiento' => 'Semitop', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            // top

            ['id' => 288, 'corporacion_id' => 2, 'cx' => '340', 'cy' => '596.86', 'r' => '9.64', 'seccionAsiento' => 'Top', 'filaAsiento' => 'A', 'asiento' => '3', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 289, 'corporacion_id' => 2, 'cx' => '373', 'cy' => '596.86', 'r' => '9.64', 'seccionAsiento' => 'Top', 'filaAsiento' => 'A', 'asiento' => '2', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 290, 'corporacion_id' => 2, 'cx' => '406', 'cy' => '596.86', 'r' => '9.64', 'seccionAsiento' => 'Top', 'filaAsiento' => 'A', 'asiento' => '1', 'activo' => true, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],

            
        ];
        DB::table('curuls')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curuls');
    }
}
