<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Carbon\Carbon;

class CreateTipoVotacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipo_votacions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->smallInteger('id')->unsigned()->autoIncrement();
            $table->string('nombre', 350)->nullable();
            $table->boolean('procedimental')->default(0)->nullable();
            $table->text('descripcion')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        $data = [
            ['id' => 1, 'nombre' => 'Acto Legislativo', 'procedimental' => 0, 'descripcion' => 'Se denominan actos legislativos las normas expedidas por el Congreso que tienen por objeto modificar, reformar, adicionar o derogar los textos constitucionales', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 2, 'nombre' => 'Proyecto de Ley', 'procedimental' => 0, 'descripcion' => 'Es la propuesta de ley que se presenta ante el Congreso y que aún no ha sido aprobada ni ratificada por el mismo. Los proyectos de ley pueden ser presentados por los diferentes Congresistas de forma individual o a través de las bancadas, por el gobierno nacional a través de los ministros y  por iniciativa popular.', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 3, 'nombre' => 'Ley Estatutaria', 'procedimental' => 0, 'descripcion' => 'Se tramitarán como proyectos de ley estatutaria, de conformidad con el artículo 152 y concordantes de la Constitución Nacional, los referidos a las siguientes materias:

                1. Derechos y deberes fundamentales de las personas y los procedimientos y recursos para su protección
                2. Administración de justicia
                3. Organización y régimen de los partidos y movimientos políticos
                4. Estatuto de la oposición y funciones electorales, reglamentando la participación de las minorias
                5. Instituciones y mecanismos de participación ciudadana
                6. Estados de excepción', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 4, 'nombre' => 'Proposiciones', 'procedimental' => 0, 'descripcion' => 'Una proposición es una moción o iniciativa que se presenta por escrito ante la mesa directiva para ser considerada consideración y en una Comisión o de una de las Cámaras. Las proposiciones se clasifican, para su trámite, en: 

                1. Proposición principal: Aquella que se presenta por primera vez para su consideración
                2. Proposición Sustitutiva: Es la que tiende a reemplazar a la principal y se discute y decide primero de la que se pretende     sustituir 
                3. Proposición suspensiva: Es la que tiene por objeto suspender el debate mientras se considera otro asunto que deba decidirse con prelación
                4. Proposición modificativa: Es la que aclara a la principal
                5. Proposición especial: Es la que no admite discusión, y que puede presentarse oralmente', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 5, 'nombre' => 'Impedimentos', 'procedimental' => 0, 'descripcion' => 'Todo Senador o Representante solicitará ser declarado impedido para conocer y participar sobre determinado proyecto o decisión trascendental, al observar un conflicto de interés.', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 6, 'nombre' => 'Aprobación de Actas', 'procedimental' => 1, 'descripcion' => 'De las sesiones de las Cámaras y sus Comisiones Permanentes, especialmente, se levantarán actas que contendrán una relación sucinta de los temas debatidos, las personas que han intervenido, los mensajes leídos, las proposiciones presentadas, las comisiones designadas y las decisiones adoptadas.

            Abierta la sesión, el Presidente someterá a discusión, sin hacerla leer, el acta de la sesión anterior, puesta previamente en conocimiento de los miembros de la corporación, bien por su publicación en la Gaceta del Congreso, o bien mediante reproducción por cualquier otro medio mecánico', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 7, 'nombre' => 'Orden del Día', 'procedimental' => 1, 'descripcion' => 'Entiéndase por orden del día la serie de negocios o asuntos que se someten en cada sesión a la información, discusión y decisión de las Cámaras legislativas y sus Comisiones permanentes', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 8, 'nombre' => 'Declaración de Sesión Informal', 'procedimental' => 0, 'descripcion' => '', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 9, 'nombre' => 'Otros Asuntos', 'procedimental' => 0, 'descripcion' => '', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 10, 'nombre' => 'Sesión permanente', 'procedimental' => 1, 'descripcion' => 'Son sesiones permanente las que durante la última media hora de la sesión se decretan para continuar con el orden del día hasta finalizar el día, si fuere el caso.', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 11, 'nombre' => 'Duplicado', 'procedimental' => 0, 'descripcion' => '', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
            ['id' => 12, 'nombre' => 'Elecciones', 'procedimental' => 1, 'descripcion' => '', 'activo' => 1, 'usercreated' => 'sys@admin.com', 'created_at' => Carbon::now()],
        ];
        DB::table('tipo_votacions')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipo_votacions');
    }
}
