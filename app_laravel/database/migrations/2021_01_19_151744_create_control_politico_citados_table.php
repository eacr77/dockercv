<?php

use App\Models\ControlPoliticoCitado;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlPoliticoCitadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('control_politico_citados', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->bigInteger('control_politico_id')->nullable()->unsigned();
            $table->bigInteger('persona_id')->nullable()->unsigned();
            $table->string('representante', 250)->nullable();
            $table->integer('asistencia')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('control_politico_citados', function (Blueprint $table) {
            $table->foreign('control_politico_id')->references('id')->on('control_politicos')->onDelete('cascade');
            $table->foreign('persona_id')->references('id')->on('personas')->onDelete('cascade');
        });
        // $this->setDataToTable();
    }

    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_control_politico_citados.csv';
        // Import CSV to Database
        $filepath = public_path($location . "/" . $file_name);
        // Reading file
        $file = fopen(
            $filepath,
            "r"
        );
        $import_data_array = [];
        $i = 0;
        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map(
                "utf8_encode",
                $data
            ); //added
            if ($i === 0) {
                $i++;
                continue;
            }
            foreach ($data as $cell_value) {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);
        // Insert to MySQL database
        foreach ($import_data_array as $import_data) {
            $created_at = $import_data[8] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[8]
                );

            $created_at = $created_at ? $created_at->format('Y-m-d G:i') : null;
            $updated_at = $import_data[9] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y',
                    $import_data[9]
                );
            $updated_at = $updated_at ? $updated_at->format('Y-m-d G:i') : null;
            $insertData = [
                "id" => $import_data[0] === 'NA' ? null : $import_data[0],
                "control_politico_id" => $import_data[1] === 'NA' ? null : $import_data[1],
                "persona_id" => $import_data[2] === 'NA' ? null : $import_data[2],
                "asistencia" => $import_data[3] === 'NA' ? null : $import_data[3],
                "tipo_citacion" => $import_data[4] === 'NA' ? null : $import_data[4],
                "activo" => 1,
                "created_at" => $created_at,
                "updated_at" => $updated_at,
            ];
            ControlPoliticoCitado::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_politico_citados');
    }
}
