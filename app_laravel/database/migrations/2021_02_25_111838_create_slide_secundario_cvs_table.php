<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlideSecundarioCvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slide_secundario_cvs', function (Blueprint $table) {
            $table->engine = 'InnoDB';        
            $table->increments('id');
            $table->integer('informacion_sitio_id')->nullable()->unsigned();
            $table->string('imagen')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable(); 
            $table->timestamps();
        });
        Schema::table('slide_secundario_cvs', function (Blueprint $table) {
            $table->foreign('informacion_sitio_id')->references('id')->on('informacion_sitios')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slide_secundario_cvs');
    }
}
