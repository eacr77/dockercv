<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpinionCongresistaImagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinion_congresista_imagens', function (Blueprint $table) {
            $table->engine = 'InnoDB';        
            $table->increments('id');
            $table->integer('opinion_congresista_id')->nullable()->unsigned();
            $table->string('imagen',350)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable(); 
            $table->timestamps();
        });

        Schema::table('opinion_congresista_imagens', function (Blueprint $table) {
            $table->foreign('opinion_congresista_id')->references('id')->on('opinion_congresistas')->onDelete('cascade');
        });   
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opinion_congresista_imagens');
    }
}
