<?php

use App\Models\Persona;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('nombres', 250)->nullable();
            $table->string('apellidos', 100)->nullable();
            $table->date('fechaNacimiento')->nullable();
            $table->unsignedBigInteger('municipio_id_nacimiento')->nullable();
            $table->unsignedBigInteger('profesion_id')->nullable();
            $table->unsignedSmallInteger('genero_id')->nullable();
            $table->date('fecha_fallecimiento')->nullable();
            $table->text('perfil_educativo')->nullable();
            $table->unsignedSmallInteger('grado_estudio_id')->nullable();
            $table->boolean('activo')->nullable()->default(1);
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();

            $table->foreign('municipio_id_nacimiento')
                  ->references('id')
                  ->on('municipios')
                  ->onDelete('cascade');

            $table->foreign('profesion_id')
                  ->references('id')
                  ->on('profesions')
                  ->onDelete('cascade');

            $table->foreign('genero_id')
                  ->references('id')
                  ->on('generos')
                  ->onDelete('cascade');

            $table->foreign('grado_estudio_id')
                  ->references('id')
                  ->on('grado_estudios')
                  ->onDelete('cascade');
        });

        DB::statement('ALTER TABLE `personas` ADD FULLTEXT search_by_name (nombres, apellidos)');
        $this->setDataToTable();
        $this->setDataToTableOtros();
    }

    /**
     * Set data to table.
     *
     * @return void
     */
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_personas.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $fechaNacimiento = $import_data[3] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[3]
                );

            $fechaNacimiento = $fechaNacimiento
                ? $fechaNacimiento->format('Y-m-d')
                : null;

            $created_at = $import_data[13] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[13]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[14] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[14]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "nombres"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "apellidos"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "fechaNacimiento"=>$fechaNacimiento,
                "municipio_id_nacimiento"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "profesion_id"=>$import_data[5] === 'NA' ? null : $import_data[5],
                "genero_id"=>$import_data[6] === 'NA' ? null : $import_data[6],
                "fecha_fallecimiento"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "perfil_educativo"=>$import_data[8] === 'NA' ? null : $import_data[8],
                "grado_estudio_id"=>$import_data[9] === 'NA' ? null : $import_data[9],
                "activo"=>$import_data[10] === 'NA' ? null : $import_data[10],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            Persona::insert($insertData);
        }
    }

    public function setDataToTableOtros()
    : void
    {
        // Insertando personas otros autores


        // File upload location
        $location = 'database';
        $file_name = 'tbl_personas_otros_autores.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file)) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $fechaNacimiento = $import_data[3] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y',
                    $import_data[3]
                );

            $fechaNacimiento = $fechaNacimiento
                ? $fechaNacimiento->format('Y-m-d')
                : null;

            $created_at = $import_data[13] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[13]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[14] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd-m-Y G:i',
                    $import_data[14]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "nombres"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "apellidos"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "fechaNacimiento"=>$fechaNacimiento,
                "municipio_id_nacimiento"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "profesion_id"=>$import_data[5] === 'NA' ? null : $import_data[5],
                "genero_id"=>$import_data[6] === 'NA' ? null : $import_data[6],
                "fecha_fallecimiento"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "perfil_educativo"=>$import_data[8] === 'NA' ? null : $import_data[8],
                "grado_estudio_id"=>$import_data[9] === 'NA' ? null : $import_data[9],
                "activo"=>$import_data[10] === 'NA' ? null : $import_data[10],
                "created_at"=>$created_at,
                "updated_at"=>$updated_at,
            ];

            Persona::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
