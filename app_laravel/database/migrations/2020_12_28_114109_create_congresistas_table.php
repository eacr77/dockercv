<?php

    use App\Models\Congresista;
    use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresistas', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->bigInteger('persona_id')->nullable()->unsigned();
            $table->string('urlHojaVida', 350)->nullable();
            $table->boolean('reemplazado')->default(0)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();

        });

        Schema::table('congresistas', function (Blueprint $table) {
            $table->foreign('persona_id')->references('id')->on('personas')->onDelete('cascade');
        });

        $this->setDataToTable();
    }

    /**
     * Set data to table.
     *
     * @return void
     */
    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_congresistas_v2.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

        while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            // $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "persona_id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "reemplazado"=>$import_data[1] === 'NA' ? 0 : $import_data[1],
                "activo"=>$import_data[2] === 'NA' ? null : $import_data[2]
            ];

            Congresista::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresistas');
    }
}
