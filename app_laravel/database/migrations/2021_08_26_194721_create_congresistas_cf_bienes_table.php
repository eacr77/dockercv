<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistasCfBienesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresistas_cf_bienes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigInteger('id')->unsigned()->autoIncrement();
            $table->integer('congresista_detalle_id')->nullable()->unsigned();
            $table->string('cf_tipo_bien', 250)->nullable();
            $table->integer('pais_id')->nullable()->unsigned();
            $table->integer('cf_departamento_id')->nullable()->unsigned();
            $table->decimal('valor', 13)->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('congresistas_cf_bienes', function (Blueprint $table) {
            $table->foreign('congresista_detalle_id')->references('id')->on('congresista_detalles')->onDelete('cascade');
            $table->foreign('pais_id')->references('id')->on('pais')->onDelete('cascade');
            $table->foreign('cf_departamento_id')->references('id')->on('cf_departamentos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresistas_cf_bienes');
    }
}
