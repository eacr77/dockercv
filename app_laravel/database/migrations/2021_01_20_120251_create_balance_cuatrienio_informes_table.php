<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBalanceCuatrienioInformesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('balance_cuatrienio_informes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
             $table->increments('id');
            $table->string('titulo', 200)->nullable();
            $table->integer('balance_cuatrienio_id')->nullable()->unsigned();
            $table->smallInteger('equipo_id')->nullable()->unsigned();
            $table->smallInteger('tipo_publicacion_id')->nullable()->unsigned();
            $table->string('fuente', 100)->nullable();
            $table->date('fechaPublicacion')->nullable();
            $table->string('autores', 300)->nullable();
            $table->text('resumen')->nullable();
            $table->text('textoPublicacion')->nullable();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('balance_cuatrienio_informes', function (Blueprint $table) {
            $table->foreign('balance_cuatrienio_id')->references('id')->on('balance_cuatrienios')->onDelete('cascade');
            $table->foreign('equipo_id')->references('id')->on('congreso_visible_equipos')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('balance_cuatrienio_informes');
    }
}
