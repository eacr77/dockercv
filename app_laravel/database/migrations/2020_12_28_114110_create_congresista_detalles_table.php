<?php
use App\Models\CongresistaDetalle;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCongresistaDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('congresista_detalles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('congresista_id')->nullable()->unsigned();
            $table->smallInteger('corporacion_id')->nullable()->unsigned();
            $table->smallInteger('cuatrienio_id')->nullable()->unsigned();
            $table->integer('partido_id')->nullable()->unsigned();
            $table->smallInteger('curul_id')->nullable()->unsigned();
            $table->smallInteger('circunscripcion_id')->nullable()->unsigned();
            $table->integer('departamento_id_mayor_votacion')->nullable()->unsigned();
            $table->boolean('activo')->default(1)->nullable();
            $table->string('usercreated', 250)->nullable();
            $table->string('usermodifed', 250)->nullable();
            $table->timestamps();
        });

        Schema::table('congresista_detalles', function (Blueprint $table) {
            $table->foreign('congresista_id')->references('id')->on('congresistas')->onDelete('cascade');
            $table->foreign('corporacion_id')->references('id')->on('corporacions')->onDelete('cascade');
            $table->foreign('cuatrienio_id')->references('id')->on('cuatrienios')->onDelete('cascade');
            $table->foreign('partido_id')->references('id')->on('partidos')->onDelete('cascade');
            $table->foreign('curul_id')->references('id')->on('curuls')->onDelete('cascade');
            $table->foreign('circunscripcion_id')->references('id')->on('circunscripcions')->onDelete('cascade');
            $table->foreign('departamento_id_mayor_votacion')->references('id')->on('departamentos')->onDelete('cascade');
        });

        $this->setDataToTable();
    }

    public function setDataToTable()
    : void
    {
        // File upload location
        $location = 'database';
        $file_name = 'tbl_congresistas_detalle.csv';

        // Import CSV to Database
        $filepath = public_path($location."/".$file_name);

        // Reading file
        $file = fopen($filepath,"r");

        $import_data_array = array();
        $i = 0;

          while (($data = fgetcsv($file, 0, '|')) !== FALSE) {
            // Skip first row (Remove below comment if you want to skip the first row)
            // $data = array_map("utf8_encode", $data); //added
            if($i === 0){
                $i++;
                continue;
            }
            foreach ($data as $cell_value)
            {
                $import_data_array[$i][] = $cell_value;
            }
            $i++;
        }
        fclose($file);

        // Insert to MySQL database
        foreach($import_data_array as $import_data){
            $created_at = $import_data[7] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[7]
                );

            $created_at = $created_at
                ? $created_at->format('Y-m-d G:i')
                : null;

            $updated_at = $import_data[8] === 'NA'
                ? null
                : DateTime::createFromFormat(
                    'd/m/Y G:i',
                    $import_data[8]
                );

            $updated_at = $updated_at
                ? $updated_at->format('Y-m-d G:i')
                : null;

            $insertData = [
                "id"=>$import_data[0] === 'NA' ? null : $import_data[0],
                "congresista_id"=>$import_data[1] === 'NA' ? null : $import_data[1],
                "corporacion_id"=>$import_data[2] === 'NA' ? null : $import_data[2],
                "cuatrienio_id"=>$import_data[3] === 'NA' ? null : $import_data[3],
                "partido_id"=>$import_data[4] === 'NA' ? null : $import_data[4],
                "curul_id"=>null,
                "circunscripcion_id"=>$import_data[5] === 'NA' ? null : $import_data[5],
                "departamento_id_mayor_votacion"=>$import_data[6] === 'NA' ? null : $import_data[6],
                "activo"=>1,
                "created_at"=>$import_data[7] === 'NA' ? null : $import_data[7],
                "updated_at"=>$import_data[8] === 'NA' ? null : $import_data[8],
            ];

            CongresistaDetalle::insert($insertData);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('congresista_detalles');
    }
}
