<?php

namespace Database\Factories;

use App\Models\CongresistasCfInfoCercanias;
use Illuminate\Database\Eloquent\Factories\Factory;

class CongresistasCfInfoCercaniasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CongresistasCfInfoCercanias::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
