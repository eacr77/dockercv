<?php

namespace Database\Factories;

use App\Models\CongresistasCfIngresos;
use Illuminate\Database\Eloquent\Factories\Factory;

class CongresistasCfIngresosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CongresistasCfIngresos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
