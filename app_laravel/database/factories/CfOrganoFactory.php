<?php

namespace Database\Factories;

use App\Models\CfOrgano;
use Illuminate\Database\Eloquent\Factories\Factory;

class CfOrganoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CfOrgano::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
