<?php

namespace Database\Factories;

use App\Models\CfCalidadMiembro;
use Illuminate\Database\Eloquent\Factories\Factory;

class CfCalidadMiembroFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CfCalidadMiembro::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
