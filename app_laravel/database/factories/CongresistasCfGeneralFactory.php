<?php

namespace Database\Factories;

use App\Models\Congresistas_cf_general;
use Illuminate\Database\Eloquent\Factories\Factory;

class CongresistasCfGeneralFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Congresistas_cf_general::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
