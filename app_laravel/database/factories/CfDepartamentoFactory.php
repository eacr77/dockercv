<?php

namespace Database\Factories;

use App\Models\CfDepartamento;
use Illuminate\Database\Eloquent\Factories\Factory;

class CfDepartamentoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CfDepartamento::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
