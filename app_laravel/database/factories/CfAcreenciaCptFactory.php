<?php

namespace Database\Factories;

use App\Models\CfAcreenciaCpt;
use Illuminate\Database\Eloquent\Factories\Factory;

class CfAcreenciaCptFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CfAcreenciaCpt::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
