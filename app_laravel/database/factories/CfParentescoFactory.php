<?php

namespace Database\Factories;

use App\Models\CfParentesco;
use Illuminate\Database\Eloquent\Factories\Factory;

class CfParentescoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CfParentesco::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
