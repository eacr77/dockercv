<?php

namespace Database\Factories;

use App\Models\CongresistaDetalle;
use Illuminate\Database\Eloquent\Factories\Factory;

class CongresistaDetalleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CongresistaDetalle::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
