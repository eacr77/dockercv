<?php

namespace Database\Factories;

use App\Models\CongresistasCfBienes;
use Illuminate\Database\Eloquent\Factories\Factory;

class CongresistasCfBienesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CongresistasCfBienes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
