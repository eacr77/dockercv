#!/bin/bash
set -e
if [[ "$1" == apache2* ]] || [ "$1" == php-fpm ]; then
        echo >&2 "Validando la instalación de PHP"

if ! [ -e /var/www/html/app_laravel/public/index.php -a \( -e /var/www/html/app_laravel/config/app.php -o -e /var/www/html/app_laravel/config/app.php \) ]; then
                echo >&2 "Archivos PHP no encontrados en $(pwd) - copiando ahora ..."
                tar cf - --one-file-system -C /usr/src/laravel . | tar xf -				
		rm -f app_laravel.tar
		chown -R www-data:www-data /var/www/html/app_laravel               		
                echo >&2 "Completado los archivos PHP han sido copiados a la ruta $(pwd)"
fi
        cp /usr/src/laravel/app_laravel/.env /var/www/html/app_laravel/
        envsubst '${DBHOST},${DBNAME},${DBPORT},${DBUSER},${DBPASSWORD},${APPURL},${APIPORT}' < "/var/www/html/app_laravel/.env" | sponge "/var/www/html/app_laravel/.env"
        echo >&2 "========================================================================"
        echo >&2
        echo >&2 " Este contenedor esta corriendo PHP buster!"
        echo >&2 "========================================================================"
fi

exec "$@"