FROM php:8.0.3-fpm-buster

# Arguments defined in docker-compose.yml
ARG uid
ARG gid

# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    libzip-dev \
    unzip \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    moreutils \
    gettext
# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug 
    

# Development
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini"

RUN sed -e 's/max_execution_time = 30/max_execution_time = 300/' -i "$PHP_INI_DIR/php.ini"

VOLUME /var/www/html/app_laravel
COPY app_laravel.tar app_laravel.tar
RUN mkdir /usr/src/laravel && tar -xf app_laravel.tar -C /usr/src/laravel && chown -R www-data:www-data /usr/src/laravel
COPY docker-entrypoint.sh /entrypoint.sh


# Get latest Composer
COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

# Install NodeJs
RUN apt-get update && \
    apt-get install -y --no-install-recommends gnupg && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get update && \
    apt-get install -y --no-install-recommends nodejs

# Copy php-fpm configuration
COPY ./docker/app/conf/php-fpm/www.conf /usr/local/etc/php-fpm.d/www.conf

# PHP configuration
COPY ./docker/app/conf/php/php.ini /usr/local/etc/php/conf.d/php.ini

RUN chmod +x /entrypoint.sh
#Copiando archivos
ENTRYPOINT ["/entrypoint.sh"]
#lorem
# Replace env and start php-fpm server
CMD ["php-fpm"]